//
//  SelectedDriver.h
//  Mandobi
//
//  Created by Bassem on 9/22/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectedDriver : NSObject


@property NSInteger DriverID;
@property(nonatomic,retain)NSString*DriverName;
@property(nonatomic,retain)NSString*DriverCarName;
@property(nonatomic,retain)NSString*CareColorName;
@property(nonatomic,retain)NSString*Rate;
@property(nonatomic,retain)NSString*Price;
@property (nonatomic)NSNumber* OrderID;


@end
