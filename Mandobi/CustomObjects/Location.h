//
//  Location.h
//  Mandobi
//
//  Created by Assem Imam on 2/16/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Location : NSObject
@property(nonatomic)double latitude;
@property(nonatomic)double longitude;

-(instancetype)initWithLatitude:(double)_latitude AndLongitude:(double)_longitude;
@end
