//
//  Profession.h
//  EasyHome
//
//  Created by Assem Imam on 12/5/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Profession : NSObject
@property(nonatomic,retain)NSString*professionName;
@property(nonatomic,retain)NSString*professionEnglishName;
@property(nonatomic)int professionID;
@property(nonatomic)int professionCategoryID;
-(instancetype)initWithProfessionName:(NSString*)name AndProfessionID:(int)profession_id AndCategoryID:(int)category_id;
-(instancetype)initWithProfessionName:(NSString*)name  ProfessionEnglishName:(NSString*)name_en AndProfessionID:(int)profession_id AndCategoryID:(int)category_id;
-(instancetype)initWithProfessionName:(NSString*)name AndProfessionID:(int)profession_id;
@end
