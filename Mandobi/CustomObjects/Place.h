//
//  Place.h
//  Mandobi
//
//  Created by Assem Imam on 4/13/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Place : NSObject
@property(nonatomic,retain)NSString*placeName;
@property(nonatomic,retain)NSString*PlaceID;
@property(nonatomic,retain)NSString*PlaceReferceID;

@end
