//
//  Profession.m
//  EasyHome
//
//  Created by Assem Imam on 12/5/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#import "Profession.h"

@implementation Profession
@synthesize professionID,professionName,professionCategoryID,professionEnglishName;
- (instancetype)initWithProfessionName:(NSString *)name AndProfessionID:(int)profession_id
{
    self = [super init];
    if (self) {
        self.professionName = name;
        self.professionID = profession_id;
    }
    
    return self;
}
-(instancetype)initWithProfessionName:(NSString *)name AndProfessionID:(int)profession_id AndCategoryID:(int)category_id
{
    self = [super init];
    if (self) {
        self.professionName = name;
        self.professionID = profession_id;
        self.professionCategoryID = category_id;
    }
    
    return self;
}
-(instancetype)initWithProfessionName:(NSString*)name  ProfessionEnglishName:(NSString*)name_en AndProfessionID:(int)profession_id AndCategoryID:(int)category_id{
    self = [super init];
    if (self) {
        self.professionName = name;
        self.professionEnglishName = name_en;
        self.professionID = profession_id;
        self.professionCategoryID = category_id;
    }
    
    return self;
}
@end
