//
//  User.m
//  EasyHome
//
//  Created by Assem Imam on 12/5/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#import "User.h"

@implementation User
@synthesize fullName,photo,mobile,email,password,userName,userType,photoFile,userID,isActive,userLevel,nationality,latitude,longitude,address;
- (instancetype)initWithFullName:(NSString *)full_name UserName:(NSString *)user_name Email:(NSString *)user_email Mobile:(NSString *)user_mobile Password:(NSString *)user_password  UserType:(UserType)type Photo:(UIImage *)user_photo PhotoFile:(NSString*)file
{
    self = [super init];
    if (self) {
        self.fullName  = full_name;
        self.userName = user_name;
        self.email = user_email;
        self.mobile = user_mobile;
        self.password = user_password;
        self.photo = user_photo;
        self.userType = type;
        self.photoFile = file;
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)decoder
{
   
    return self;
}
-(instancetype)initFromUserDefaults
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"User"]) {
        id CurrentUser =[[NSUserDefaults standardUserDefaults] objectForKey:@"User"];
        self.userID =[CurrentUser[@"userID"] intValue];
        self.fullName =CurrentUser[@"fullName"];
         self.password =CurrentUser[@"password"];
        self.userName =CurrentUser[@"userName"];
        self.userLevel =[CurrentUser[@"userLevel"]intValue];
        self.photoFile =CurrentUser[@"photoFile"];
        self.mobile =CurrentUser[@"mobile"];
        self.email =CurrentUser[@"email"];
        self.address =CurrentUser[@"address"];
        self.nationality =CurrentUser[@"nationality"];
        self.isActive =[CurrentUser[@"isActive"]intValue];
        if ( CurrentUser[@"photo"]) {
            NSData *photoData =CurrentUser[@"photo"];
            if (photoData && photoData.length>0) {
                self.photo =[UIImage imageWithData: CurrentUser[@"photo"]];

            }

        }
        return  self;
    }
    else{
        return nil;
    }
}
-(void)deleteFromUserDefaults
{
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"User"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void)saveToUserDefaults
{
    NSMutableDictionary*userDictionary = [NSMutableDictionary new];
    [userDictionary setObject:self.mobile?self.mobile :@"" forKey:@"mobile"];
    [userDictionary setObject:self.password?self.password :@"" forKey:@"password"];
    [userDictionary setObject:self.fullName?self.fullName :@"" forKey:@"fullName"];
    [userDictionary setObject:self.userName?self.userName :@"" forKey:@"userName"];
    [userDictionary setObject:[NSString stringWithFormat:@"%i",self.userType]  forKey:@"userType"];
    [userDictionary setObject:self.photoFile?self.photoFile:@"" forKey:@"photoFile"];
    [userDictionary setObject:[NSString stringWithFormat:@"%i", self.userID] forKey:@"userID"];
    [userDictionary setObject:self.email?self.email:@"" forKey:@"email"];
    [userDictionary setObject:self.address?self.address:@"" forKey:@"address"];
    [userDictionary setObject:self.nationality?self.nationality:@"" forKey:@"nationality"];
    [userDictionary setObject:[NSString stringWithFormat:@"%i", self.userLevel] forKey:@"userLevel"];
    [userDictionary setObject:[NSString stringWithFormat:@"%i", self.isActive] forKey:@"isActive"];
    NSData *imageData = [CommonMethods compressImage:self.photo WithSize:CGSizeMake(80, 80)];
    [userDictionary setObject:imageData?imageData:@"" forKey:@"photo"];
    [[NSUserDefaults standardUserDefaults] setObject:userDictionary forKey:@"User"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
