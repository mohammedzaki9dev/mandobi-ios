//
//  User.h
//  EasyHome
//
//  Created by Assem Imam on 12/5/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CommonMethods.h"

typedef enum {
    CLIENT = 0,
    WORKER = 1,
    PUBLIC_USER = 2
}UserType;
@interface User : NSObject
@property(nonatomic,retain)NSString*fullName;
@property(nonatomic,retain)NSString*userName;
@property(nonatomic,retain)NSString*email;
@property(nonatomic,retain)NSString*mobile;
@property(nonatomic,retain)NSString*address;
@property(nonatomic,retain)NSString*password;
@property(nonatomic,retain)UIImage*photo;
@property(nonatomic,retain)NSString*photoFile;
@property(nonatomic) UserType userType;
@property(nonatomic) int userID;
@property(nonatomic) BOOL isActive;
@property(nonatomic) int userLevel;
@property(nonatomic,retain) NSString* nationality;
@property(nonatomic)double latitude;
@property(nonatomic)double longitude;
@property(nonatomic)NSData *imageData;
-(instancetype)initFromUserDefaults;
-(instancetype)initWithFullName:(NSString*)full_name UserName:(NSString*)user_name Email:(NSString*)user_email Mobile:(NSString*)user_mobile Password:(NSString*)user_password UserType:(UserType)type Photo:(UIImage*)user_photo PhotoFile:(NSString*)file;
-(void)saveToUserDefaults;
-(void)deleteFromUserDefaults;
@end
