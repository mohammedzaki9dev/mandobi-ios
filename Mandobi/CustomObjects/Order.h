//
//  Order.h
//  Mandobi
//
//  Created by Assem Imam on 2/16/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Location.h"
#import "Worker.h"

typedef enum {
    SEND = 0,
    RECEIVE = 1,
    OPEN = 2,
    EXECUTED = 3,
    CANCELED = 4
}OrderType;

@interface Order : NSObject

@property(nonatomic)int orderID;
@property(nonatomic)int senderID;
@property(nonatomic)int receiverID;
@property(nonatomic)float price;
@property(nonatomic,retain)CLLocation *fromLocation;
@property(nonatomic,retain)CLLocation *toLocation;
@property(nonatomic,retain)CLPlacemark *fromPlacemark;
@property(nonatomic,retain)CLPlacemark *toPlacemark;
@property(nonatomic,retain)NSString *orderDate;
@property(nonatomic,retain)NSString *orderTime;
@property(nonatomic,retain)NSString *message;
@property(nonatomic,retain)NSString *subject;
@property(nonatomic,retain)NSString *receipientName;
@property(nonatomic,retain)NSString *receipientMobile;
@property(nonatomic)BOOL isViewed;
@property(nonatomic)OrderType orderType;
@property(nonatomic,retain)Worker *worker;
@property(nonatomic)float distance;
@property(nonatomic,retain)NSString*fromAddress;
@property(nonatomic,retain)NSString*toAddress;

@property(nonatomic,retain)NSString *driverName;
@property(nonatomic,retain)NSString *driverMobile;
@property(nonatomic) double driverRate;

@end
