//
//  Location.m
//  Mandobi
//
//  Created by Assem Imam on 2/16/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "Location.h"

@implementation Location
@synthesize latitude,longitude;
-(instancetype)initWithLatitude:(double)_latitude AndLongitude:(double)_longitude{
    self = [super init];
    if (self) {
        self.latitude  = _latitude;
        self.longitude = _longitude;
    }
    return self;
}
@end
