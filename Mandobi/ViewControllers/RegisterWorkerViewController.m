//
//  RegisterWorkerViewController.m
//  Mandobi
//
//  Created by Assem Imam on 2/3/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "RegisterWorkerViewController.h"
@import IQKeyboardManager;
#import <AVFoundation/AVFoundation.h>
#import "LoginViewController.h"

#define CAMERA_BUTTON_INDEX 0
#define GALARRY_BUTTON_INDEX 1
#define CANCEL_BUTTON_INDEX 2
#define PHOTO_ACTIONSHEET_TAG 0
@interface RegisterWorkerViewController ()
{
    UIImage *userImage;
    BOOL enablePickingImage;
    VehicleType selectedVehicleType;
}
@end

@implementation RegisterWorkerViewController
@synthesize openForEdit;

- (void)viewDidLoad {
    [super viewDidLoad];
    @try {
        if (openForEdit) {
            cameraButtonHeightConstraint.constant = 0;
            [self.view layoutIfNeeded];
            [registerButton setTitle:NSLocalizedString(@"update", nil) forState:UIControlStateNormal];
            
            fullNameTexyField.text = CurrentLogedUserInfo.fullName;
            mobileTextField.text = CurrentLogedUserInfo.mobile;
            emailTextField.text = CurrentLogedUserInfo.email;
            addressTextField.text = CurrentLogedUserInfo.address;
            userNameTextField.text = CurrentLogedUserInfo.userName;

            vehicleNumberTextField.text =((Worker*) CurrentLogedUserInfo).vehicleColor;
            licenceNumberTextField.text =((Worker*) CurrentLogedUserInfo).licenceNumber;

            switch (((Worker*) CurrentLogedUserInfo).vehicleType) {
                case SMALL_VEHICLE:
                {
                    [smallCheckMarkImageView setHidden:NO];
                    [mediumCheckMarkImageView setHidden:YES];
                    [largeCheckMarkImageView setHidden:YES];
                }
                    break;
                    
                case MEDIUM_VEHICLE:
                {
                    [mediumCheckMarkImageView setHidden:NO];
                    [smallCheckMarkImageView setHidden:YES];
                    [largeCheckMarkImageView setHidden:YES];
                }
                    break;
                case LARGE_VEHICLE:
                {
                    [largeCheckMarkImageView setHidden:NO];
                    [mediumCheckMarkImageView setHidden:YES];
                    [smallCheckMarkImageView setHidden:YES];
                }
                    break;
            }

            
        }
        else{
            [registerButton setTitle:NSLocalizedString(@"register", nil) forState:UIControlStateNormal];
        }
        cameraButton.layer.cornerRadius =cameraButton.frame.size.width/2;
        [cameraButton setClipsToBounds:YES];
        cameraButton.layer.borderWidth = 2;
        cameraButton.layer.borderColor = [[UIColor whiteColor]CGColor];
        
        [CommonMethods FormatView:containerView WithShadowRadius:2 CornerRadius:5 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];
        
        
        if (self.comeFromMenu) {
            [[SlideNavigationController sharedInstance] setEnableSwipeGesture:YES];
            
        }
        else{
            [[SlideNavigationController sharedInstance] setEnableSwipeGesture:NO];
            
        }
        [SlideNavigationController sharedInstance].rightBarButtonItem = menuButton;
        
        [[IQKeyboardManager sharedManager] setEnable:YES];
        enablePickingImage = NO;
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if(status == AVAuthorizationStatusAuthorized) {
            enablePickingImage =YES;
        } else if(status == AVAuthorizationStatusDenied){
        } else if(status == AVAuthorizationStatusRestricted){
        } else if(status == AVAuthorizationStatusNotDetermined){
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                enablePickingImage = granted;
            }];
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma -mark IBActions
- (IBAction)vehicleTypeButtonAction:(UIButton *)sender {
    @try {
        selectedVehicleType = (int)sender.tag;
        switch (selectedVehicleType) {
            case SMALL_VEHICLE:
            {
                [smallCheckMarkImageView setHidden:NO];
                [mediumCheckMarkImageView setHidden:YES];
                [largeCheckMarkImageView setHidden:YES];
            }
                break;
                
            case MEDIUM_VEHICLE:
            {
                [mediumCheckMarkImageView setHidden:NO];
                [smallCheckMarkImageView setHidden:YES];
                [largeCheckMarkImageView setHidden:YES];
            }
                break;
            case LARGE_VEHICLE:
            {
                [largeCheckMarkImageView setHidden:NO];
                [mediumCheckMarkImageView setHidden:YES];
                [smallCheckMarkImageView setHidden:YES];
            }
                break;
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)registerButtonAction:(UIButton *)sender {
    @try {
        [self.view endEditing:YES];
        if ([self validateUserInputs]) {
            if (internetConnection) {
                SHOW_FULL_LOADING
                
                Worker *registerdWorker = [[Worker alloc]initWithFullName:fullNameTexyField.text UserName:userNameTextField.text Email:emailTextField.text Mobile:mobileTextField.text UserType:WORKER Password:passwordTextField.text VehicleTyepe:selectedVehicleType LicenceNumber:licenceNumberTextField.text VehicleColor:vehicleNumberTextField.text Photo:userImage PhotoFile:nil];
                
                if (openForEdit) {
                    NSString *password = CurrentLogedUserInfo.password;
                    if (passwordTextField.text.length > 0) {
                        password = passwordTextField.text;
                    }
                  registerdWorker = [[Worker alloc]initWithFullName:fullNameTexyField.text UserName:userNameTextField.text Email:emailTextField.text Mobile:mobileTextField.text UserType:WORKER Password:password VehicleTyepe:selectedVehicleType LicenceNumber:licenceNumberTextField.text VehicleColor:vehicleNumberTextField.text Photo:userImage PhotoFile:nil];
                }
                registerdWorker.address = addressTextField.text;
                registerdWorker.userID = CurrentLogedUserInfo.userID;
                registerdWorker.photo = CurrentLogedUserInfo.photo;
                registerdWorker.photoFile = CurrentLogedUserInfo.photoFile;
                
                if (openForEdit) {
                    [[ServiceLogic sharedInstance]updateProfileForUser:registerdWorker Complession:^(id response) {
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            HIDE_FULL_LOADING
                            if (response) {
                                if ([response objectForKey:@"message"]) {
                                  
                                    [TSMessage dismissActiveNotification];
                                    [TSMessage showNotificationInViewController:self title:nil subtitle:[response objectForKey:@"message"] image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                                    
                                }
                                else{
                                   if ([[response objectForKey:@"data"] isEqualToString:@"success"]) {
                                       CurrentLogedUserInfo = registerdWorker;
                                       [((Worker*)CurrentLogedUserInfo) saveToUserDefaults];
                                       
                                       [TSMessage dismissActiveNotification];
                                       [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"updateSuccess", nil) image:nil type:TSMessageNotificationTypeSuccess duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                                   }
                                   else{
                                       [TSMessage dismissActiveNotification];
                                       [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"updateFail", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                                   }
                                    
                                }
                            }
                            else{
                                [TSMessage dismissActiveNotification];
                                [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"updateFail", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                            }
 
                        });
                    }];
                }
                else{
                    [[ServiceLogic sharedInstance] registerUser:registerdWorker Complession:^(id response) {
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            HIDE_FULL_LOADING
                            BOOL registerSuccess =NO;
                            if (response) {
                                if ([response objectForKey:@"message"]) {
                                    registerSuccess =NO;
                                    [TSMessage dismissActiveNotification];
                                    [TSMessage showNotificationInViewController:self title:nil subtitle:[response objectForKey:@"message"] image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                                }
                                else{
                                    if ([[response objectForKey:@"data"] isEqualToString:@"success"]) {
                                        registerSuccess =YES;
                                        CurrentUserType = WORKER;
                                        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%i",WORKER] forKey:@"loged_user_type"];
                                        [[NSUserDefaults standardUserDefaults] synchronize];
                                        
                                        LoginViewController *vcLogin = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                        vcLogin.comeFromMenu = NO;
                                        vcLogin.currentUserType = WORKER;
                                        [self.navigationController pushViewController:vcLogin animated:YES];
                                        
                                    }
                                }
                                
                            }
                            else{
                                
                                [TSMessage dismissActiveNotification];
                                [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"register_fail", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                            }
                            
                        });
                    }];
                }
            }
            else{
                SHOW_NO_INTERNET_MESSAGE
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)cameraButtonAction:(UIButton *)sender {
    @try {
        [self.view endEditing:YES];
        
        if (enablePickingImage) {
            UIActionSheet *photoActionSheet = [[UIActionSheet alloc]initWithTitle:NSLocalizedString(@"choose_image", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"camera", nil),NSLocalizedString(@"album", nil), nil];
            photoActionSheet.tag = PHOTO_ACTIONSHEET_TAG;
            [photoActionSheet showInView:self.view];
        }
        else{
            
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enableCamera", nil) image:nil type:TSMessageNotificationTypeWarning duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
}
- (BOOL)validateUserInputs {
    @try {
        
        if ([fullNameTexyField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_name", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
            return NO;
        }
        if ([userNameTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_user_name", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
            return NO;
        }
        if ([addressTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_address", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
            return NO;
        }

        if (openForEdit == NO) {
            if ([passwordTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
                [TSMessage dismissActiveNotification];
                [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_password", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                
                return NO;
            }
            if ([passwordTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length<6) {
                [TSMessage dismissActiveNotification];
                [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"password_length", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                
                return NO;
            }
            
        }
        if ([passwordTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length>0) {
            if ([confirmPasswordTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
                [TSMessage dismissActiveNotification];
                [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"confirm_password", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                
                return NO;
            }
        }
        
        if ([passwordTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length>0) {
            if (![passwordTextField.text isEqualToString: confirmPasswordTextField.text]) {
                [TSMessage dismissActiveNotification];
                [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"missmatched_password", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                
                return NO;
            }
        }

        
        if ([mobileTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_mobile", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
            return NO;
        }
        if (![CommonMethods CheckPhoneNumber:mobileTextField.text]) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_valid_mobile", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
            return NO;
        }
        if ([emailTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_email", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
            return NO;
        }
        if (![CommonMethods ValidateMail:emailTextField.text]) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_valid_email", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
            return NO;
        }
        if ([licenceNumberTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_licence_number", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
            return NO;
        }
        if ([vehicleNumberTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_vehicle_number", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
            return NO;
        }
        
        
        return YES;
    }
    @catch (NSException *exception) {
        
    }
}

#pragma -mark uiactionsheet delegate methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    @try {
        
        if (buttonIndex!=CANCEL_BUTTON_INDEX) {
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            switch (buttonIndex) {
                case CAMERA_BUTTON_INDEX:
                {
                    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                }
                    break;
                case GALARRY_BUTTON_INDEX:
                {
                    imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                }
                    break;
            }
            
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self presentViewController:imagePicker animated:YES completion:nil];
            }];
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark imagepicker delegate methods
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    @try {
        UIImage* pickedImage = (UIImage *) [info objectForKey:
                                            UIImagePickerControllerOriginalImage];
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        userImage = [UIImage imageWithCGImage:pickedImage.CGImage scale:0.25 orientation:pickedImage.imageOrientation];
        [cameraButton setImage:userImage forState:UIControlStateNormal];
        NSData *imageData = [CommonMethods compressImage:userImage WithSize:CGSizeMake(5, 5)];
        if (imageData.length > 2097152) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"image_max_size", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            return;
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    @try {
        [picker dismissViewControllerAnimated:YES completion:nil];
        
    }
    @catch (NSException *exception) {
        
    }
}

@end
