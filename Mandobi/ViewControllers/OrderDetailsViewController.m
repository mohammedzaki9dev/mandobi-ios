//
//  OrderDetailsViewController.m
//  Mandobi
//
//  Created by Assem Imam on 2/12/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "OrderDetailsViewController.h"
#import "WorkerProfileViewController.h"
#import "RequestCancelingViewController.h"
#import "LMGeocoder.h"
#import "MandoobMapFollowViewController.h"


@interface OrderDetailsViewController ()
@property (weak, nonatomic) IBOutlet UIButton *followBtn;

@end

@implementation OrderDetailsViewController
@synthesize currentOrder,isComeFromMandoobResponse,isComeFromCancllingReason;
-(void)formatViews
{
    @try {
        for (UIView*view in containerScrollView.subviews) {
            if (view.tag == -1) {
                for (UIView*button in view.subviews) {
                    [self formatButtonViews:(UIButton*)button];
                }
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    @try {
       
        if (self.CurrentOrderType == OPEN) {
            self.statusLabel.text = NSLocalizedString(@"OPEN", nil);
        }else  if (self.CurrentOrderType == EXECUTED) {
            self.statusLabel.text = NSLocalizedString(@"EXECUTED", nil);
            [self.followBtn setHidden:YES];
        }else  if (self.CurrentOrderType == CANCELED) {
            self.statusLabel.text = NSLocalizedString(@"CANCELED", nil);
            [self.followBtn setHidden:YES];
        }
        
        
        
        [shipToActivityIndicator stopAnimating];
        [shipFromActivityIndicator stopAnimating];

        shipToLabel.text=@"";
        shipFromLabel.text=@"";
        if (isComeFromCancllingReason) {
            cancellingReasonHeightConstraint.constant = 92.0f;
            cancelReasonLabel.text = remoteNotificationInfo[@"aps"][@"alert"][@"body"];
            [acceptView setHidden:YES];
            [rejectView setHidden:YES];
        }
        else{
            cancellingReasonHeightConstraint.constant = 0.0f;
        }
        if (isComeFromMandoobResponse) {
            priceViewHeightConstraint.constant = 39.0f;
            clientNameTitleLabel.text = NSLocalizedString(@"mandoob_name_title", nil);
        }
        else{
            priceViewHeightConstraint.constant = 0.0f;
            clientNameTitleLabel.text = NSLocalizedString(@"client_name_title", nil);
        }
        [self formatViews];
        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance]getDetailsForOrder:currentOrder Complession:^(id response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        if ([response count]>0) {
                            Order *returnedOrder =(Order*)response[0];
                            self.orderDetail = returnedOrder;
                            orderTitleLabel.text = returnedOrder.message;
                            NSURL *imageUrl =[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGES_BASE_URL,returnedOrder.worker.photoFile]];
                            if (imageUrl) {
                                [userImageView setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"square"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                            }
                            clientNameLabel.text = returnedOrder.worker.fullName;
                            currentLocationLabel.text=returnedOrder.worker.place;
                            mobileLabel.text=returnedOrder.worker.mobile;
                            consigneeNameLabel.text=returnedOrder.receipientName;
                            consigneeMobileLabel.text=returnedOrder.receipientMobile;
                            priceLabel.text =[NSString stringWithFormat:@"%0.2f", returnedOrder.price];
                            shipFromLabel.text = [returnedOrder.fromAddress stringByAppendingString:@"\n\n\n"];
                            shipToLabel.text =[returnedOrder.toAddress stringByAppendingString:@"\n\n\n"];
                            _dateLabel.text = returnedOrder.orderDate;
                            _driverName.text = returnedOrder.worker.fullName;
                            _driverMobile.text = returnedOrder.worker.mobile;
//                            _driverRate.text = [NSString stringWithFormat:@"%0.1d", returnedOrder.worker.starsVoteCount];
                            
                            
                         
//                            [[ServiceLogic sharedInstance]reverseGeocodeWithLatitude:returnedOrder.fromLocation.latitude Longitude:returnedOrder.fromLocation.longitude APIKEY:@"AIzaSyDOzBuFo5ZxCjlW-4k7NaxkOE-1YdD64_o" Complession:^(id response) {
//                                dispatch_async(dispatch_get_main_queue(), ^{
//                                    [shipFromActivityIndicator stopAnimating];
//                                    if (response) {
//                                        shipFromLabel.text = [response stringByAppendingString:@"\n\n\n"];
//                                       
//                                    }
//                                });
//                            }];
                            
                            

//                            [[LMGeocoder sharedInstance] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(returnedOrder.toLocation.latitude, returnedOrder.toLocation.longitude)
//                                          service:kLMGeocoderGoogleService
//                                completionHandler:^(NSArray *results, NSError *error) {
//                                    if (results.count && !error) {
//                                        LMAddress *address = [results firstObject];
//                                        NSLog(@"Address: %@", address.formattedAddress);
//                                        shipToLabel.text =[address.formattedAddress stringByAppendingString:@"\n\n\n"];
//                                    }
//                                }];
                            
                            switch (returnedOrder.worker.vehicleType) {
                                case SMALL_VEHICLE:
                                {
                                    vehicleTypelabel.text= NSLocalizedString(@"small_vehicle", nil);
                                    vehicleTypeImageView.image = [UIImage imageNamed:@"vehicle-1"];
                                }
                                    break;
                                    
                                case MEDIUM_VEHICLE:
                                {
                                    vehicleTypelabel.text= NSLocalizedString(@"medium_vehicle", nil);
                                    vehicleTypeImageView.image = [UIImage imageNamed:@"vehicle-2"];
                                }
                                    break;
                                case LARGE_VEHICLE:
                                {
                                    vehicleTypelabel.text= NSLocalizedString(@"large_vehicle", nil);
                                    vehicleTypeImageView.image = [UIImage imageNamed:@"vehicle-3"];
                                }
                                    break;
                            }
                            
                        }
                        else{
                            
                        }
                        
                    }
                    else{
                        SHOW_NO_DATA_MESSAGE
                    }
                });
            }];
            
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
        
        [self getDriverRate];
    }
    @catch (NSException *exception) {
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)dismissPopup:(id)sender {
    [self.popup dismiss:nil];
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     @try {
         if ([segue.identifier isEqualToString:@"cancel"]) {
               ((RequestCancelingViewController*)segue.destinationViewController).currentOrder = currentOrder;
         }
         else if ([segue.identifier isEqualToString:@"follow"]){
             ((MandoobMapFollowViewController *) segue.destinationViewController).currentOrder = currentOrder;
         }
     }
     @catch (NSException *exception) {
         
     }
 }

-(void)getDriverRate{
    if (internetConnection) {
        SHOW_FULL_LOADING
        [[ServiceLogic sharedInstance]getDriverRateForDriverId:[NSNumber numberWithInt:self.currentOrder.worker.userID] Complession:^(id response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                HIDE_FULL_LOADING
                if (response) {
                    if ([response count]>0) {
                        ;
                       // self.driverRate.text = [NSString stringWithFormat:@"%0.1f",  [[[response lastObject] valueForKey:@"AVG(rate)"]doubleValue ]];
                    }
                    else{
                        
                    }
                    
                }
                else{
                    //SHOW_NO_DATA_MESSAGE
                }
            });
        }];
        
    }
    else{
        SHOW_NO_INTERNET_MESSAGE
    }
    
}


#pragma -mark IBActions
- (IBAction)rejectButtonAction:(UIButton *)sender {
    @try {
        if (isComeFromMandoobResponse) {
            [self performSegueWithIdentifier:@"cancel" sender:self];
        }
        else{
            WorkerProfileViewController*vcProfile = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"WorkerProfileViewController"];
            vcProfile.comeFromMenu = YES;
            [self.navigationController pushViewController:vcProfile animated:YES];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)acceptButtonAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:true];
    //[self performSegueWithIdentifier:@"follow" sender:sender];
    
//    @try {
//        if (isComeFromMandoobResponse) {
//            if (internetConnection) {
//                SHOW_FULL_LOADING
//                [[ServiceLogic sharedInstance]confirmRequestFromUser:CurrentLogedUserInfo ForOrder:currentOrder Complession:^(id response) {
//                    dispatch_sync(dispatch_get_main_queue(), ^{
//                        HIDE_FULL_LOADING
//                        if ([response objectForKey:@"message"]) {
//                            [TSMessage dismissActiveNotification];
//                            [TSMessage showNotificationInViewController:self title:nil subtitle:[response objectForKey:@"message"]  image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
//                        }
//                        else {
//                            [self performSegueWithIdentifier:@"follow" sender:sender];
//                        }
//                        
//                    });
//                }];
//            }
//            else{
//                SHOW_NO_INTERNET_MESSAGE
//            }
//        }
//        else{
//            UIAlertView *priceAlertView = [[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"enter_price", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil) otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
//            priceAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
//            ((UITextField*) [priceAlertView textFieldAtIndex:0]).keyboardType = UIKeyboardTypeNumberPad;
//            [priceAlertView show];
//        }
//    }
//    @catch (NSException *exception) {
//        
//    }
}

#pragma -mark uialerview delegate methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    @try {
        if (buttonIndex == 1) {
            if (internetConnection) {
                SHOW_FULL_LOADING
                float price = [[alertView textFieldAtIndex:0].text floatValue];
                [[ServiceLogic sharedInstance]acceptOrder:currentOrder ForWorker:(Worker*)CurrentLogedUserInfo WithPrice:price Complession:^(id response) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        HIDE_FULL_LOADING
                        if (response) {
                            if ([response objectForKey:@"message"]) {
                                
                                [TSMessage dismissActiveNotification];
                                [TSMessage showNotificationInViewController:self title:nil subtitle:[response objectForKey:@"message"] image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                                
                            }
                            else{
                                if ([[response objectForKey:@"data"] isEqualToString:@"success"]) {
                                    WorkerProfileViewController*vcWorkerprofile = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"WorkerProfileViewController"];
                                    vcWorkerprofile.comeFromMenu = YES;
                                    [self.navigationController pushViewController:vcWorkerprofile animated:YES];
                                }
                            }
                        }
                        else{
                            
                        }
                        
                    });
                }];
            }
            else{
                SHOW_NO_INTERNET_MESSAGE
            }
            
        }
    }
    @catch (NSException *exception) {
        
    }
}
-(BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView{
    @try {
        if (![alertView textFieldAtIndex:0].text.length == 0) {
            return YES;
        }
        else{
            return NO;
        }
    }
    @catch (NSException *exception) {
        
    }
}
@end
