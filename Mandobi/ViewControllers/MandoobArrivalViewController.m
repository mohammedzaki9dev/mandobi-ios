//
//  MandoobArrivalViewController.m
//  Mandobi
//
//  Created by Assem Imam on 3/23/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MandoobArrivalViewController.h"
#import "OpenInGoogleMapsController.h"
#import <MapKit/MapKit.h>
@interface MandoobArrivalViewController ()<CLLocationManagerDelegate,GMSMapViewDelegate>
{
    GMSMapView*mapView;
    GMSMarker*currentMarker;
    GMSMarker *mandoobMarker;
    CLLocationCoordinate2D fromPosition;
    CLLocationCoordinate2D toPosition;

}
@end

@implementation MandoobArrivalViewController
-(void)zoomToMyLocation
{
    @try {
        [mapView setCamera:[GMSCameraPosition cameraWithLatitude:CurrentLogedUserInfo.latitude
                                                       longitude:CurrentLogedUserInfo.longitude
                            
                                                            zoom:17
                                                         bearing:30
                                                    viewingAngle:40]];
        [mapView animateToLocation:CLLocationCoordinate2DMake(CurrentLogedUserInfo.latitude, CurrentLogedUserInfo.longitude)];
    }
    @catch (NSException *exception) {
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    @try {
        //adding googlemaps mapview
        mapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.view.frame.size.width, mapViewContainer.bounds.size.height) camera:nil];
        mapView.mapType=kGMSTypeNormal;
        mapView.delegate = self;
        [mapView setMyLocationEnabled:NO];
        [mapView setMinZoom:5 maxZoom:30];
        [mapView setIndoorEnabled:YES];
        mapView.settings.compassButton = YES;
        mapView.settings.myLocationButton=NO;
        [mapViewContainer addSubview:mapView];
        [mapViewContainer layoutIfNeeded];
        [mapView setFrame:CGRectMake(0, 0, self.view.frame.size.width, mapViewContainer.bounds.size.height)];
        
        // adding current location marker
         fromPosition =CLLocationCoordinate2DMake(CurrentLogedUserInfo.latitude, CurrentLogedUserInfo.longitude)  ;
       

        currentMarker = [GMSMarker markerWithPosition:fromPosition];
        currentMarker.icon = [UIImage imageNamed:@"marker٣٣"];
        currentMarker.map = mapView;
        
        [self zoomToMyLocation];
        
        
        [CommonMethods FormatView:clientView WithShadowRadius:2 CornerRadius:0 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];

        [self formatButtonViews:arrivedButton];

        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance]getInformationForUserID:self.currentOrder.receiverID Complession:^(id response) {
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [[ServiceLogic sharedInstance]getDetailsForOrder:self.currentOrder Complession:^(id response) {
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            HIDE_FULL_LOADING
                             if (response) {
                                 self.currentOrder = response[0];
                                  toPosition =CLLocationCoordinate2DMake(self.currentOrder.toLocation.coordinate.latitude, self.currentOrder.toLocation.coordinate.longitude)  ;
                             }
                        });
                    }
                ];
                    
                    if (response) {
                        User*returnUser = (User*)response;
                        clientNameLabel.text = returnUser.fullName;
                        NSURL *imageUrl =[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGES_BASE_URL,returnUser.photoFile]];
                        mandoobMarker = [GMSMarker markerWithPosition:toPosition];
                        mandoobMarker.icon = [UIImage imageNamed:@"marker٣٣"];
                        mandoobMarker.icon  = [GMSMarker markerImageWithColor:[UIColor greenColor]];
                        mandoobMarker.map = mapView;

                        if (imageUrl) {
                            [clientImageView setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"square"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                        }
                    }
                    else{
                        SHOW_NO_DATA_MESSAGE
                    }
                });
                              
            }];
            
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)arrivedButtonAction:(UIButton *)sender {
    @try {
        
        if ([[UIApplication sharedApplication] canOpenURL:
              [NSURL URLWithString:@"comgooglemaps://"]]) {
            GoogleDirectionsDefinition *definition = [[GoogleDirectionsDefinition alloc] init];
            definition.startingPoint = [GoogleDirectionsWaypoint
                                        waypointWithLocation:fromPosition];
            definition.destinationPoint = [GoogleDirectionsWaypoint
                                           waypointWithLocation:toPosition];
            
            definition.travelMode = kGoogleMapsTravelModeDriving;
            [[OpenInGoogleMapsController sharedInstance] openDirections:definition];
            
        } else {
            Class mapItemClass = [MKMapItem class];
            MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:toPosition
                                                           addressDictionary:nil];
            MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
            [mapItem setName:@"Mandoob Location"];
            NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
            MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
            [MKMapItem openMapsWithItems:@[mapItem, currentLocationMapItem]
                           launchOptions:launchOptions];
        }
    }
    @catch (NSException *exception) {
        
    }
}
@end
