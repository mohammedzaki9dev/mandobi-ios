//
//  MenuViewController.m
//  ChauffeurTaxi
//
//  Created by Assem Imam on 12/30/15.
//  Copyright © 2015 Assem Imam. All rights reserved.
//

#import "MenuViewController.h"
#import "MasterViewController.h"
#import "MenuCell.h"
#import "WelcomeViewController.h"
#import "RTLController.h"
#import "LoginViewController.h"

#define SELECTED_ITEM_VALUE 1
#define UN_SELECTED_ITEM_VALUE 0
@interface MenuViewController ()
{
    NSMutableArray *menuItemsList;

}
@end

@implementation MenuViewController
-(void)performLogOut{
    @try {
        if (internetConnection) {
            [[SlideNavigationController sharedInstance] righttMenuSelected:nil];

            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance] logOutForUser:CurrentLogedUserInfo Complession:^(id response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        [[NSUserDefaults standardUserDefaults]setObject:@"2" forKey:@"loged_user_type"];
                        [CurrentLogedUserInfo deleteFromUserDefaults];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        userViewHeightConstraint.constant =0;
                        CurrentUserType = PUBLIC_USER;
                        CurrentLogedUserInfo = nil;

                        WelcomeViewController*vcWelcome = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"WelcomeViewController"];
                        vcWelcome.comeFromMenu = YES;
                        [SlideNavigationController sharedInstance].viewControllers=@[vcWelcome];
                        [self loadMenuItems];
                        [userInfoView setHidden:YES];

                    }
                });
                
            }];
            
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
    }
    @catch (NSException *exception) {
        
    }
}
-(void)didChangeLanguage
{
    [menuItemsTableView reloadData];
}
-(void)makeRoundedImage{
    @try {
        userImageView.layer.cornerRadius =userImageView.frame.size.width/2;
        [userImageView setClipsToBounds:YES];
        userImageView.layer.borderWidth = 2;
        userImageView.layer.borderColor = [[UIColor whiteColor]CGColor];
        
        userImageView.superview.layer.cornerRadius =userImageView.superview.frame.size.width/2;
        userImageView.superview.layer.shadowColor = [UIColor blackColor].CGColor;
        userImageView.superview.layer.shadowRadius = 3.f;
        userImageView.superview.layer.shadowOffset = CGSizeMake(0.f, 0.f);
        userImageView.superview.layer.shadowOpacity = 0.4f;
    
    }
    @catch (NSException *exception) {
        
    }
}
-(void)displayCurrenUserInformation
{
    @try {
        
        NSURL *imageUrl =[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGES_BASE_URL,CurrentLogedUserInfo.photoFile]];
        if (imageUrl) {
            [userImageView setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"square"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
        }
        userFullNamelabel.text = CurrentLogedUserInfo.fullName;
    }
    @catch (NSException *exception) {
        
    }
}
-(void)didFinishLogin
{
    @try {
        [self loadMenuItems];
        [self makeRoundedImage];
        [userInfoView setHidden:NO];
        userViewHeightConstraint.constant = 70;
        [self displayCurrenUserInformation];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)loadMenuItems
{
    @try {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"menuItems" ofType:@"plist"];
        NSDictionary*menuDictionary =[[NSDictionary alloc]initWithContentsOfFile:filePath];
        [menuItemsList removeAllObjects];
        if (CurrentUserType == PUBLIC_USER) {
            for (id item in [menuDictionary objectForKey:@"default_menu"]) {
                [menuItemsList addObject:[item mutableCopy]];
            }
            
        }
       else if (CurrentUserType == WORKER) {
            for (id item in [menuDictionary objectForKey:@"worker_menu"]) {
                [menuItemsList addObject:[item mutableCopy]];
            }
            
        }
       else if (CurrentUserType == CLIENT) {
           for (id item in [menuDictionary objectForKey:@"client_menu"]) {
               [menuItemsList addObject:[item mutableCopy]];
           }
           
       }
        
        [menuItemsTableView reloadData];
    }
    @catch (NSException *exception) {
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    @try {
        menuItemsList = [NSMutableArray new];
        [self loadMenuItems];
        if (CurrentUserType != PUBLIC_USER) {
            [self makeRoundedImage];
            [userInfoView setHidden:NO];
            userViewHeightConstraint.constant = 70;
            [self displayCurrenUserInformation];
        }
        else{
            userViewHeightConstraint.constant = 0;
            [userInfoView setHidden:YES];
        }
       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFinishLogin) name:LOGIN_NOTIFICATION object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeLanguage) name:CHANGE_LANGUAGE_NOTIFICATION object:nil];
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cleMenu"];
    id currentItem =[menuItemsList objectAtIndex:indexPath.row];
    if ([[BundleLocalization sharedInstance].language isEqualToString:ARABIC_LANG]) {
        cell.menuItemLabel.text = [currentItem objectForKey:@"item_name"];
        
    }
    else{
        cell.menuItemLabel.text = [currentItem objectForKey:@"item_name_en"];
        
    }
    cell.menuItemImageView.image = [UIImage imageNamed:[currentItem objectForKey:@"image"]];
//    if ([[currentItem objectForKey:@"is_selected"]intValue]==SELECTED_ITEM_VALUE) {
//        [cell.selectionIndicatorView setHidden:NO];
//    }
//    else{
//        [cell.selectionIndicatorView setHidden:YES];
//    }
    cell.backgroundColor = [UIColor clearColor];
    return  cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [menuItemsList count];
}
#pragma -mark uitable view delegate methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        for (id item in menuItemsList) {
            [item setObject:@"0" forKey:@"is_selected"];
        }
        id currentItem =[menuItemsList objectAtIndex:indexPath.row];
        [currentItem setObject:@"1" forKey:@"is_selected"];
        [tableView reloadData];
        
        if ([[currentItem objectForKey:@"is_logout"]intValue]==1) {
            [self performLogOut];
        }
        else{
            UIViewController*vc=  [applicationStroryBoard instantiateViewControllerWithIdentifier:[currentItem objectForKey:@"view_controller_name"]];
            if ([vc isKindOfClass:[MasterViewController class]]) {
                ((MasterViewController*)vc).comeFromMenu=YES;
            }
            if ([currentItem objectForKey:@"is_worker"]) {
                if ([[currentItem objectForKey:@"is_worker"]intValue]==1) {
                    ((LoginViewController*)vc).currentUserType = WORKER;
                }
                else{
                    ((LoginViewController*)vc).currentUserType = CLIENT;
                }
            }
            if ([[BundleLocalization sharedInstance].language isEqualToString:ARABIC_LANG]) {
                [[SlideNavigationController sharedInstance] righttMenuSelected:nil];
            }
            else{
                [[SlideNavigationController sharedInstance] leftMenuSelected:nil];
            }

            [SlideNavigationController sharedInstance].viewControllers=@[vc];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}

@end
