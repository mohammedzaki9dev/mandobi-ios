//
//  MasterViewController.h
//  Amana
//
//  Created by Assem Imam on 11/27/13.
//  Copyright (c) 2013 assemimam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonMethods.h"
#import "SlideNavigationController.h"
#import "AppDelegate.h"
#import "TSMessage.h"
#import "ServiceLogic.h"
#import "global.h"
#import "UIViewController+CWPopup.h"
#import "UIViewController+MJPopupViewController.h"
#import <PopupDialog/PopupDialog-Swift.h>
@import AFNetworking;



@interface MasterViewController : UIViewController<SlideNavigationControllerDelegate>
{
     __weak IBOutlet UIButton *leftMenuButton;
   __weak IBOutlet UIButton *backButton;
   __weak IBOutlet UIButton *menuButton;
}

@property(nonatomic)BOOL comeFromMenu;
@property (nonatomic,weak)IBOutlet UIView* headerView;
@property(nonatomic)BOOL isComeFromNotification;

- (IBAction)backButtonAction:(UIButton *)sender;
-(void)formatButtonViews:(UIButton*)targetView;
@end
