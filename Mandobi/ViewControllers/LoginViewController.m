//
//  LoginViewController.m
//  Mandobi
//
//  Created by Assem Imam on 2/3/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "LoginViewController.h"
@import IQKeyboardManager;
#import "WorkerMainViewController.h"
#import "ClientMainViewController.h"
#import "RegisterClientViewController.h"
#import "RegisterWorkerViewController.h"
#import "ForgetPasswordViewController.h"

@interface LoginViewController ()
{
    NSNotification*loginNotification;

}
@end

@implementation LoginViewController



@synthesize currentUserType;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
  
    
    self.currentUserType = CLIENT;
    @try {
//        [CommonMethods FormatView:containerView WithShadowRadius:2 CornerRadius:5 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];
        [[IQKeyboardManager sharedManager] setEnable:YES];
        loginNotification = [NSNotification notificationWithName:LOGIN_NOTIFICATION object:nil];
//        if (self.currentUserType == WORKER) {
//            titleLabel.text = NSLocalizedString(@"mandoob_logoin_title", nil);
//        }
//        if (self.currentUserType == CLIENT) {
//            
//            titleLabel.text = NSLocalizedString(@"client_logoin_title", nil);
//        }
        
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    @try {
        if ([segue.identifier isEqualToString:@"register_client"]) {
            ((RegisterClientViewController*)segue.destinationViewController).comeFromMenu = NO;
        }
        else if ([segue.identifier isEqualToString:@"forget_password"]) {
            ((ForgetPasswordViewController*)segue.destinationViewController).comeFromMenu = NO;
            ((ForgetPasswordViewController*)segue.destinationViewController).currentUserType = self.currentUserType;

        }

        else{
            ((RegisterWorkerViewController*)segue.destinationViewController).comeFromMenu = NO;
        }
    }
    @catch (NSException *exception) {
        
    }
}

#pragma -mark IBActions
- (IBAction)loginButtonAction:(UIButton *)sender {
    @try {
        [self.view endEditing:YES];
        if ([self validateUserInputs]) {
            if (internetConnection) {
                SHOW_FULL_LOADING
                
                [[ServiceLogic sharedInstance]loginWithUser:[[User alloc]initWithFullName:nil UserName:userNameTextField.text Email:nil Mobile:nil Password:passwordTextField.text UserType:self.currentUserType Photo:nil PhotoFile:nil] Complession:^(id response) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        
                        HIDE_FULL_LOADING;
                        if (response) {
                            Worker*loggedWorker = (Worker*)response;
                            if (self.currentUserType == CLIENT) {
                                CurrentLogedUserInfo = (User*)loggedWorker;
                            }
                            else{
                                CurrentLogedUserInfo = loggedWorker;
                            }
                            CurrentLogedUserInfo.userType = self.currentUserType;

                            CurrentUserType = self.currentUserType;
                            CurrentLogedUserInfo.isActive = YES;
                            [[NSNotificationCenter defaultCenter] postNotification:loginNotification];
                            
                            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%i",self.currentUserType] forKey:@"loged_user_type"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            //insert device token to the server
                            
                            NSString*token = [[NSUserDefaults standardUserDefaults]objectForKey:@"token"];
                            if (token) {
                                [[ServiceLogic sharedInstance] sendNotificationToken:token ForWorker:(Worker*)CurrentLogedUserInfo Complession:^(id response) {
                                    dispatch_sync(dispatch_get_main_queue(), ^{
                                        if (response) {
                                            
                                        }
                                        else{
                                            
                                        }
                                    });
                                    
                                }];
                            }
                            else{
                                
                            }
                            

//                            if (self.currentUserType==WORKER) {
//                                [(Worker*)CurrentLogedUserInfo saveToUserDefaults];
//                                WorkerMainViewController *vcWorkerMainView = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"WorkerMainViewController"];
//                                vcWorkerMainView.comeFromMenu = YES;
//                                self.navigationController.viewControllers=@[vcWorkerMainView];
//                            }
//                            else if (self.currentUserType==CLIENT){
                                [(User*)CurrentLogedUserInfo saveToUserDefaults];
//                                ClientMainViewController *vcClientMainView = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"ClientMainViewController"];
//                                vcClientMainView.comeFromMenu = YES;
//                                self.navigationController.viewControllers=@[vcClientMainView];
//                            }
                            
                            //TODO :Localize Main Storyboard
//                                   applicationStroryBoard  = [UIStoryboard storyboardWithName:@"Main_ar" bundle:nil];
                            
                            NSString*storyBoardName=@"Main_ar";
                            //        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            
                            if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
                                storyBoardName=@"Main_en";
                            }

                            
                            [APP_DELEGATE instantiateInitialViewControllerWithstoryboard:storyBoardName];
//                            UIViewController *vc  = [applicationStroryBoard  instantiateInitialViewController];
//                            [APP_DELEGATE.window setRootViewController:vc];
//                            [APP_DELEGATE.window makeKeyAndVisible];
                            
                        }
                        else{
                            [TSMessage dismissActiveNotification];
                            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"invalid_login_data", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                        }
                        
                    });
                }];
            }
            else{
               SHOW_NO_INTERNET_MESSAGE
            }
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)registerButtonAction:(UIButton *)sender {
    @try {
        if (self.currentUserType == WORKER) {
            [self performSegueWithIdentifier:@"register_worker" sender:nil];
        }
        else if (self.currentUserType == CLIENT) {
            [self performSegueWithIdentifier:@"register_client" sender:nil];

        }
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)forgetPasswordButtonAction:(UIButton *)sender {
    @try {
        [self performSegueWithIdentifier:@"forget_password" sender:nil];

    }
    @catch (NSException *exception) {
        
    }

}

#pragma -mark Methods
- (BOOL)validateUserInputs {
    @try {
        
        if ([userNameTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_user_name", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
            return NO;
        }
        if ([passwordTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_password", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
            return NO;
        }
        return  YES;
    }
    @catch (NSException *exception) {
        
    }
}

@end
