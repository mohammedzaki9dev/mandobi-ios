//
//  BudgetCalculatorViewController.h
//  Mandobi
//
//  Created by Assem Imam on 2/8/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface BudgetCalculatorViewController : MasterViewController
{
    
    __weak IBOutlet UILabel *totalPriceLabel;
    __weak IBOutlet UILabel *totalOrdersLabel;
    __weak IBOutlet UILabel *orderDateLabel;
    __weak IBOutlet UIView *detailsView;
    __weak IBOutlet UIView *containerView;
}
- (IBAction)orderDateButtonAction:(UIButton *)sender;
@end
