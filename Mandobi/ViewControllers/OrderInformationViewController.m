//
//  OrderInformationViewController.m
//  Mandobi
//
//  Created by Assem Imam on 3/10/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "OrderInformationViewController.h"
#import "MDDirectionService.h"
#import "UserProfileViewController.h"
#import "ClientOrdersViewController.h"

@interface OrderInformationViewController ()
{
    GMSMapView*mapView;
    CLLocationManager *locationManager;
    
    MDDirectionService *routingService;
    CLLocationCoordinate2D destinationCoordinates;
    CLLocationCoordinate2D currentLocationCoordinates;
    CLLocationCoordinate2D selectedPlaceCoordinates;
    NSString *TotalDistance;
    NSMutableArray *RouteMarkersStrings;
    NSMutableArray*mapRoutingPathes;;
    NSMutableArray*addedMarkersList;
}
@end

@implementation OrderInformationViewController
@synthesize submittedOrder;

-(void)formatViews
{
    @try {
        for (UIView *view in containerScrollView.subviews) {
            if (view.tag == -1) {
                 [CommonMethods FormatView:view WithShadowRadius:2 CornerRadius:0 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];
            }
        }
        [self formatButtonViews:acceptButton];
        [self formatButtonViews:rejectButton];

    }
    @catch (NSException *exception) {
        
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    @try {
        [self formatViews];
        
        nameLabel.text = submittedOrder.receipientName;
        phoneTextView.text = submittedOrder.receipientMobile;
        
        switch (submittedOrder.worker.vehicleType) {
            case SMALL_VEHICLE:
            {
                vehicleTypeImageView.image = [UIImage imageNamed:@"smal_car"];
                vehicleTypeLabel.text = NSLocalizedString(@"small_vehicle", nil);
            }
                break;
                
            case MEDIUM_VEHICLE:
            {
                vehicleTypeImageView.image = [UIImage imageNamed:@"med_car"];
                vehicleTypeLabel.text = NSLocalizedString(@"medium_vehicle", nil);

            }
                break;

            case LARGE_VEHICLE:
            {
                vehicleTypeImageView.image = [UIImage imageNamed:@"large_car"];
                vehicleTypeLabel.text = NSLocalizedString(@"large_vehicle", nil);

            }
                break;

        }
        //adding map view
        mapView = [GMSMapView mapWithFrame:CGRectZero camera:nil];
        mapView.mapType=kGMSTypeNormal;
        [mapView setMyLocationEnabled:NO];
        [mapView setMinZoom:5 maxZoom:30];
        [mapView setIndoorEnabled:YES];
        mapView.settings.compassButton = YES;
        mapView.settings.myLocationButton=NO;
        [mapContainerView addSubview:mapView];
        [mapView setFrame:CGRectMake(0, 0,mapContainerView.bounds.size.width, mapContainerView.bounds.size.height)];
        [mapContainerView layoutIfNeeded];

        CLLocationCoordinate2D fromPosition = submittedOrder.fromLocation.coordinate;
        //////CLLocationCoordinate2DMake(submittedOrder.fromLocation.coordinate.latitude, submittedOrder.fromLocation.coordinate.longitude)  ;
        GMSMarker* currentLocationMarker = [GMSMarker markerWithPosition:fromPosition];
        currentLocationMarker.title = @"From Address";
        currentLocationMarker.icon = [UIImage imageNamed:@"marker٣٣"];
        currentLocationMarker.map = mapView;
        currentLocationMarker.draggable = YES;

        CLLocationCoordinate2D toPosition =CLLocationCoordinate2DMake(submittedOrder.toLocation.coordinate.latitude, submittedOrder.toLocation.coordinate.longitude)  ;
        GMSMarker*destimationLocationMarker = [GMSMarker markerWithPosition:toPosition];
        destimationLocationMarker.title = @"To Address";
        //                destimationLocationMarker.icon = [UIImage imageNamed:@"marker٣٣"];
        destimationLocationMarker.icon  = [GMSMarker markerImageWithColor:[UIColor greenColor]];
        destimationLocationMarker.map = mapView;
        
        RouteMarkersStrings =[[NSMutableArray alloc]init];
        [self zoomToMyLocation];
        [NSThread detachNewThreadSelector:@selector(PerformRouting) toTarget:self withObject:nil];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)PerformRouting
{
    @try {
        
        destinationCoordinates.latitude = submittedOrder.toLocation.coordinate.latitude;
        destinationCoordinates.longitude = submittedOrder.toLocation.coordinate.longitude;
        currentLocationCoordinates.latitude =submittedOrder.fromLocation.coordinate.latitude;
        currentLocationCoordinates.longitude =submittedOrder.fromLocation.coordinate.longitude;
        
        [RouteMarkersStrings removeAllObjects];
        
        
        NSString *currentLocationString = [[NSString alloc] initWithFormat:@"%f,%f",
                                           currentLocationCoordinates.latitude,currentLocationCoordinates.longitude];
        NSString *destinationString = [[NSString alloc] initWithFormat:@"%f,%f",
                                       destinationCoordinates.latitude,destinationCoordinates.longitude];
        [RouteMarkersStrings addObject:currentLocationString];
        [RouteMarkersStrings addObject:destinationString];
        
        NSString *sensor = @"false";
        NSArray *parameters = [NSArray arrayWithObjects:sensor, RouteMarkersStrings,
                               nil];
        NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
        NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                          forKeys:keys];
        routingService=[[MDDirectionService alloc] init];
        SEL selector = @selector(addDirections:);
        [routingService setDirectionsQuery:query
                              withSelector:selector
                              withDelegate:self];
        
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)clearAllAddedRoutingPathesFromMap
{
    @try {
        for (GMSPolyline *routingPath in mapRoutingPathes) {
            routingPath.map = nil;
        }
    }
    @catch (NSException *exception) {
        
    }
}
- (void)addDirections:(NSDictionary *)json{
    @try {
        
        if ([json objectForKey:@"routes"]) {
            if ([[json objectForKey:@"routes"] count]>0) {
                
                GMSMutablePath *path = [GMSMutablePath path];
                NSMutableArray *polyLinesArray = [[NSMutableArray alloc] init];
                int zero=0;
                
                TotalDistance = [[[[[[json objectForKey:@"routes"] objectAtIndex:zero] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"distance"] objectForKey:@"text"];
                
                NSLog(@"%@", TotalDistance);
                dispatch_sync(dispatch_get_main_queue(), ^{
                    distanceLabel.text = TotalDistance;
                    submittedOrder.distance = [TotalDistance floatValue];
                });
                
                NSArray *steps = [[[[[json objectForKey:@"routes"] objectAtIndex:zero] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"steps"];
                for (int i=0; i<[[[[[[json objectForKey:@"routes"] objectAtIndex:zero] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"steps"]count]; i++) {
                    
                    NSString* encodedPoints =[[[steps objectAtIndex:i]objectForKey:@"polyline"]valueForKey:@"points"];
                    polyLinesArray = [routingService decodePolyLine:encodedPoints];
                    NSUInteger numberOfCC=[polyLinesArray count];
                    for (NSUInteger index = 0; index < numberOfCC; index++) {
                        CLLocation *location = [polyLinesArray objectAtIndex:index];
                        CLLocationCoordinate2D coordinate = location.coordinate;
                        [path addLatitude:coordinate.latitude longitude:coordinate.longitude];
                        if (index==0) {
                            //[self.coordinates addObject:location];
                        }
                    }
                }
                [self clearAllAddedRoutingPathesFromMap];
                GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
                polyline.strokeWidth =5;
                polyline.geodesic = YES;
                polyline.strokeColor = [UIColor colorWithRed:243/255.0f green:189/255.0f blue:29/255.0f alpha:1];
                polyline.map = mapView;
                [mapRoutingPathes addObject:polyline];
                
                [self  performSelectorOnMainThread:@selector(HideLoading) withObject:nil waitUntilDone:NO  ];
            }
            else{
                [self performSelectorOnMainThread:@selector(ShowNoRouteAlert) withObject:nil waitUntilDone:NO];
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)HideLoading
{
    @try {
        
        HIDE_FULL_LOADING
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)ShowNoRouteAlert
{
    [[[UIAlertView alloc]initWithTitle:nil message:@"تعذر الوصول الى طريق" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil]show];
    
}

-(void)zoomToMyLocation
{
    @try {
        [mapView setCamera:[GMSCameraPosition cameraWithLatitude:submittedOrder.fromLocation.coordinate.latitude
                                                       longitude:submittedOrder.fromLocation.coordinate.longitude
                            
                                                            zoom:17
                                                         bearing:30
                                                    viewingAngle:40]];
        [mapView animateToLocation:CLLocationCoordinate2DMake(submittedOrder.fromLocation.coordinate.latitude, submittedOrder.fromLocation.coordinate.longitude)];
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma -mark IBActions
- (IBAction)acceptButtonAction:(UIButton *)sender {
    @try {
        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance] sendOrder:submittedOrder ForUser:CurrentLogedUserInfo Complession:^(id response) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        [TSMessage dismissActiveNotification];
                        [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"send_order_success", nil) image:nil type:TSMessageNotificationTypeSuccess duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                        ClientOrdersViewController*vcOrders = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"ClientOrdersViewController"];
                        vcOrders.comeFromMenu = YES;
                        self.navigationController.viewControllers=@[vcOrders];
                    }
                    else{
                        [TSMessage dismissActiveNotification];
                        [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"send_order_fail", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                    }
                });
            } ];
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
    }
    @catch (NSException *exception) {
        
    }
}
- (IBAction)rejectButtonAction:(UIButton *)sender {
    @try {
        UserProfileViewController*vcUserProfile = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
        vcUserProfile.comeFromMenu = YES;
        self.navigationController.viewControllers=@[vcUserProfile];
         
    }
    @catch (NSException *exception) {
        
    }
}
@end
