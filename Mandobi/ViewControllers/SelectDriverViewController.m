//
//  SelectDriverViewController.m
//  Mandobi
//
//  Created by Bassem on 9/22/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "ClientOrdersViewController.h"
#import "MandoobMapFollowViewController.h"
#import "MasterViewController.h"
#import "SelectDriverViewController.h"
 #import "SelectedDriver.h"
#import "UIViewController+MJPopupViewController.h"
@interface SelectDriverViewController (){
    
     NSArray*driverList;
   
}

@property (weak, nonatomic) IBOutlet UILabel *recipientNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *toAddressLabel;

@end

@implementation SelectDriverViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.useBlurForPopup = YES;
//    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"cleDriver";
    
    if ([[BundleLocalization sharedInstance].language isEqualToString:ARABIC_LANG]) {
        //                storyBoardName=@"Main_en";
        CellIdentifier=@"cleDriver_ar";
    }
    
    SelectDriverCell*cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    SelectedDriver *driver = [driverList objectAtIndex:indexPath.row];
    
    cell.DriverName.text = driver.DriverName;
    cell.OrderPrice.text = driver.Price;
//    cell.CarColor.text = driver.CareColorName;
    
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
     NSInteger numOfRows = [driverList count];
    
    return  numOfRows;
}

#pragma mark - LoadData
-(void)loadData:(NSInteger)parentId{
    NSLog(@"parentID : %ld",(long)parentId);
    @try {
        if (internetConnection) {
            SHOW_FULL_LOADING
            
            
            [[ServiceLogic sharedInstance]getOrdersResponsesFor:CurrentLogedUserInfo With:parentId Complession:^(id response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        
                        
                        NSString *toAddress = [response valueForKey:@"toAddress"];
                        NSString *contactName = [response valueForKey:@"contactName"] ;
                        NSInteger orderID = [[response valueForKey:@"orderID"] integerValue];
                        self.toAddress = toAddress;
                        self.contactName = contactName;
                        self.recipientNameLabel.text = self.contactName;
                        self.toAddressLabel.text = self.toAddress;
                        
                        self.orderId = orderID;
                         driverList = nil;
                        driverList = [[NSArray alloc] initWithArray: (NSArray*)[response objectForKey:@"drivers"] ];
//                        NSDictionary *data = @{@"toAddress":toAddress,@"contactName":contactName,@"orderID":[NSNumber numberWithInt:orderID],@"drivers":ordersList};
                        
                        _currentOrder = [[Order alloc] init];
                        _currentOrder.orderID = [[NSNumber numberWithInteger:orderID] intValue] ;
                        
                        tableView.delegate = self;
                        tableView.dataSource = self;
                        
                        bool isAcceptClientPrice = NO;
                        
                          for (SelectedDriver *driver in driverList ) {
                            
                              NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                              f.numberStyle = NSNumberFormatterDecimalStyle;
                              NSNumber *myNumber = [f numberFromString:driver.Price];
                              
                              if ([myNumber isEqual: self.clientPrice]) {
                                  isAcceptClientPrice = YES;
                                  self.selectedDriver = driver;
                                  break;
                              }
                              
                        }
                        if (isAcceptClientPrice == NO) {
                            NSLog(@"%lu",(unsigned long)driverList.count);
                            [tableView reloadData];
                        }else {
                            
                            [self selectDriverAction:self];
                        }
                        
                      
                    }
                    else{
                        
                        [APP_DELEGATE showRequestCanceledMessage];
                        [self dismiss:self];

                        
//                        [self.mainVc closePopup];
//                        [self dismissPopupViewControllerAnimated:YES completion:nil];
                        
//                        [self dismissViewControllerAnimated:YES completion:^{
//                            
//                            [APP_DELEGATE showRequestCanceledMessage];
//                        }];
                       
                        
                    }
                });
            }];
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
    }
    @catch (NSException *exception) {
        
    }
    
}


#pragma -mark uitableview delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        
         NSInteger numOfRows = [driverList count];
        
        
       
        
        if (numOfRows >= 1) {
            ((SelectDriverCell*) [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]).selectionImage.image  = [UIImage imageNamed:@"unselected"];
        }
        
        if (numOfRows >= 2) {
            ((SelectDriverCell*) [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]]).selectionImage.image  = [UIImage imageNamed:@"unselected"];
//            [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]].accessoryType= UITableViewCellAccessoryNone;
        }
        
        if (numOfRows >= 3) {
           ((SelectDriverCell*) [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]]).selectionImage.image  = [UIImage imageNamed:@"unselected"];
//            [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]].accessoryType= UITableViewCellAccessoryNone;
        }
        
        if (indexPath.row == 0) {
           
            ((SelectDriverCell*) [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]).selectionImage.image  = [UIImage imageNamed:@"selected"];
//            [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]].accessoryType= UITableViewCellAccessoryCheckmark;
            
            self.selectedDriver = (SelectedDriver*)[driverList objectAtIndex:0];
        }
        else if (indexPath.row == 1){
           
            ((SelectDriverCell*) [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]]).selectionImage.image  = [UIImage imageNamed:@"selected"];
//            [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]].accessoryType= UITableViewCellAccessoryCheckmark;
             self.selectedDriver = (SelectedDriver*)[driverList objectAtIndex:1];
            
        }
        
        else if (indexPath.row == 2){
            
            ((SelectDriverCell*) [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]]).selectionImage.image  = [UIImage imageNamed:@"selected"];
//            [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]].accessoryType= UITableViewCellAccessoryCheckmark;
             self.selectedDriver = (SelectedDriver*)[driverList objectAtIndex:2];
            
        }
        
        
     
        
        
    }
    @catch (NSException *exception) {
        
    }
}
- (IBAction)selectDriverAction:(id)sender {
    
    if (_selectedDriver != nil) {
        
        @try {
         
                if (internetConnection) {
                    SHOW_FULL_LOADING
                    NSNumber *order = self.selectedDriver.OrderID;
                    [[ServiceLogic sharedInstance] ActionForInboxMail: order ForSender: [NSNumber numberWithInt:CurrentLogedUserInfo.userID ]forReciver:[NSNumber numberWithInteger: _selectedDriver.DriverID] withTye:[NSNumber numberWithInt:2] Complession:^(id response) {
                        
                        
                    
//                    [[ServiceLogic sharedInstance]confirmRequestFromUserUpdates:CurrentLogedUserInfo.userID ForOrder: order forWorker: self.selectedDriver.DriverID Complession:^(id response) {
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            HIDE_FULL_LOADING
                            if ([[response objectForKey:@"message"]isEqualToString:@"success"])  {
                                [TSMessage dismissActiveNotification];
                                [TSMessage showNotificationInViewController:APP_DELEGATE.window.rootViewController title:nil subtitle:NSLocalizedString(@"success_Choose_Driver", nil)   image:nil type:TSMessageNotificationTypeSuccess duration:5.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
////todo Testing
//                                [self.mainVc closePopup];
                                
                                [self prepareForOrderDetail:self.selectedDriver.OrderID];

//                                 [self dismissViewControllerAnimated:YES completion:nil];
                            }
                            else {
                                [TSMessage dismissActiveNotification];
                                [TSMessage showNotificationInViewController:APP_DELEGATE.window.rootViewController title:nil subtitle:NSLocalizedString(@"fail_choose_Driver", nil)   image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
//                                [self.mainVc closePopup];
                                [self dismiss:self];

//                                [self dismissViewControllerAnimated:YES completion:nil];                             //   [self performSegueWithIdentifier:@"follow" sender:sender];
                            }
                            
                        });
                    }];
                }
                else{
                    SHOW_NO_INTERNET_MESSAGE
                }
           
        }
        @catch (NSException *exception) {
            
        }
        
        
        
        
//        [self dismissViewControllerAnimated:YES completion:nil];
        
    }else {
        
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:_mainVc title:nil subtitle:NSLocalizedString(@"select_driver_or_Cancel_order", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
        
        
        
        
                   }
    }

- (IBAction)dismiss:(id)sender {
//    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];

    [self.popup dismiss:^{
        
    }];
//    [self.mainVc closePopup];
//    [self dismissPopupViewControllerAnimated:YES completion:nil];
//    [self dismissViewControllerAnimated:true completion:nil];
}
    -(void)prepareForOrderDetail:(NSNumber*)orderID{
     
        
        self.currentOrder.orderID = [orderID intValue];
        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance]getDetailsForOrder:self.currentOrder Complession:^(id response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        if ([response count]>0) {
                            Order *returnedOrder =(Order*)response[0];
                            
                           
                            ((UITabBarController*)APP_DELEGATE.window.rootViewController).selectedIndex = 1;
                            [((UINavigationController*) ( ((UITabBarController*)APP_DELEGATE.window.rootViewController).selectedViewController)) popToRootViewControllerAnimated:NO];
                            
                            ClientOrdersViewController *MAINVC = (ClientOrdersViewController*) ((UINavigationController*) ( ((UITabBarController*)APP_DELEGATE.window.rootViewController).selectedViewController)).childViewControllers[0];
                            
                            
                            MAINVC.selectedOrder = returnedOrder;
                            MAINVC.orderType = OPEN;
                            
                            [MAINVC performSegueWithIdentifier:@"follow" sender:MAINVC];
                            
                            [self dismiss:self];
                            
                          
                        }
                        else{
                            [self dismiss:self];
                        }
                        
                    }
                    else{
                        SHOW_NO_DATA_MESSAGE
                    }
                });
            }];
            
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
        
        
//        if (internetConnection) {
//            SHOW_FULL_LOADING
//            [[ServiceLogic sharedInstance]getOrderById:orderID Complession:^(id response) {
//               
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    HIDE_FULL_LOADING
//                    if (response) {
//                        if ([response count]>0) {
//                            Order *returnedOrder =(Order*)response[0];
////  todo set tab index = 1
////                            push follow order
////
//                            
////                             MandoobMapFollowViewController *vc = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"MandoobMapFollowViewController"];
////                            
////                            vc.currentOrder = returnedOrder;
////                            vc.CurrentOrderType = OPEN;
////                            
//                            [self.mainVc closePopup];
//                            
//                            
//                            
//                           
////                            ((UITabBarController*)APP_DELEGATE.window.rootViewController).tabBarController.selectedIndex = 1;
////                            
////                            
////                            [((UINavigationController*) ( ((UITabBarController*)APP_DELEGATE.window.rootViewController).selectedViewController)).visibleViewController.navigationController pushViewController:vc animated:YES];
////
////                            
//                            
//             
//                        }
//                        else{
//                            
//                        }
//                        
//                    }
//                    else{
//                        SHOW_NO_DATA_MESSAGE
//                    }
//                });
//            }];
//            
//        }
//        else{
//            SHOW_NO_INTERNET_MESSAGE
//        }
        
    }
- (IBAction)CancelOrderAction:(id)sender {
    
    @try {
        
        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance]cancelUserRequest:CurrentLogedUserInfo ForOrder:_currentOrder WithCancelReasonID:1 Complession:^(id response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        if ([response objectForKey:@"message"]) {
                            [TSMessage showNotificationInViewController:_mainVc title:nil subtitle:[response objectForKey:@"message"]  image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                        }
//                        else {
//                            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"accept_client_request_success", nil)  image:nil type:TSMessageNotificationTypeSuccess duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
//                            UserProfileViewController*vcProfile = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
//                            vcProfile.comeFromMenu = YES;
//                            [self.navigationController pushViewController:vcProfile animated:YES];
//                        }
                        //                                                [self.mainVc closePopup];
//                        [self.mainVc closePopup];
                        [self dismiss:self];

                        [self dismissPopupViewControllerAnimated:YES completion:nil];
//                        [self dismissViewControllerAnimated:YES completion:nil];
                        
                    }
                    else{
//                        [self.mainVc closePopup];
                        [self dismiss:self];

                        [self dismissPopupViewControllerAnimated:YES completion:nil];
//                        [self dismissViewControllerAnimated:YES completion:nil];
                    }
                });
            }];
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
    }
    @catch (NSException *exception) {
        
    }
    
    
    
}

@end
