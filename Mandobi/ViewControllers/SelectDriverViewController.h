//
//  SelectDriverViewController.h
//  Mandobi
//
//  Created by Bassem on 9/22/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SelectDriverCell.h"
#import "MasterViewController.h"
#import "AppDelegate.h"
#import "SelectedDriver.h"
#import "UIViewController+CWPopup.h"
#import "ClientMainViewController.h"
#import <PopupDialog/PopupDialog-Swift.h>
@interface SelectDriverViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    
    __weak IBOutlet UITableView *tableView;
}
@property NSInteger orderId;
@property NSInteger parentId;
@property NSNumber *clientPrice;
@property NSString *toAddress ;
@property NSString *contactName ;
@property PopupDialog *popup;

@property (nonatomic) ClientMainViewController *mainVc;

@property(nonatomic,retain) Order*currentOrder;
@property(nonatomic,retain)SelectedDriver *selectedDriver;

-(void)loadData:(NSInteger)parentId;

@end
