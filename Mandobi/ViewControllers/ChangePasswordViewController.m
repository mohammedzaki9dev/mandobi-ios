//
//  ForgetPasswordViewController.m
//  Mandobi
//
//  Created by Assem Imam on 6/1/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController
@synthesize currentUserType;
- (void)viewDidLoad {
    [super viewDidLoad];
     self.currentUserType = PUBLIC_USER ;
    
    @try {
        
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)validateUserInputs {
    @try {

        if ([passwordTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_password", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
            return NO;
        }
//        if ([passwordTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length>7) {
//            [TSMessage dismissActiveNotification];
//            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_password_l", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
//            
//            return NO;
//        }
        return YES;
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)sendButtonAction:(UIButton *)sender {
    @try {
        [self.view endEditing:YES];
        if ([self validateUserInputs]) {
            if (internetConnection) {
               SHOW_FULL_LOADING
                CurrentLogedUserInfo.password = passwordTextField.text;
                
                [[ServiceLogic sharedInstance] updateProfileForUser:CurrentLogedUserInfo Complession:^(id response) {
                    
                    
                
//                [[ServiceLogic sharedInstance]resetPasswordForUserType:self.currentUserType WithEmail:emailTextField.text Complession:^(id response) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        HIDE_FULL_LOADING
                       if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
//                            if ([response objectForKey:@"message"]) {
//                             if (response) {
//                                [TSMessage dismissActiveNotification];
//                                [TSMessage showNotificationInViewController:self title:nil subtitle: response image:nil type:TSMessageNotificationTypeSuccess duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
//                                
//                            }
//                            else{
                                [TSMessage dismissActiveNotification];
                                [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"change_password_success", nil) image:nil type:TSMessageNotificationTypeSuccess duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                            [self.navigationController popViewControllerAnimated:YES];
//                            }
                            
                        }
                        else{
                            [TSMessage dismissActiveNotification];
                            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"Change_password_fail", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                        }
                    });
                }];
            }
            else{
                SHOW_NO_INTERNET_MESSAGE
            }
        }
    }
    @catch (NSException *exception) {
        
    }
}
@end
