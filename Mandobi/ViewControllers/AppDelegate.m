//
//  AppDelegate.m
//  EasyHome
//
//  Created by Assem Imam on 11/28/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#import "AppDelegate.h"
#import "MenuViewController.h"
#import "MasterViewController.h"
#import "WelcomeViewController.h"
#import "MainViewController.h"
#import "UserProfileViewController.h"
#import "WorkerMainViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "SlideNavigationController.h"
#import "RTSpinKitView.h"
#import "Reachability.h"
#import "ClientMainViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "SelectDriverViewController.h"
#import "UIViewController+CWPopup.h"
#import "UIViewController+MJPopupViewController.h"
//#import "JDStatusBarNotification.h"
#import "JDStatusBarNotification.h"
#import "WaitingViewController.h"
#import <PopupDialog/PopupDialog-Swift.h>


@import GooglePlaces;
@import Firebase;
//name="HacenBeirutLtX3" family="Hacen Beirut Lt X3"

#define APP_FONT @"GESSTwoMedium-Medium"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface AppDelegate ()<CLLocationManagerDelegate>
{
    UIView*modalView;
    RTSpinKitView *spinner;
    NSNotification*clientRemoteNotification;
    NSNotification*workerRemoteNotification;
    NSNotification*publicRemoteNotification;
    CLLocationManager*locationManager;
}
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    @try {
        // initialize Google+ Sign In API
        [GMSServices provideAPIKey:GOOGLE_MAPS_API_KEY];
        [GMSPlacesClient provideAPIKey:GOOGLE_MAPS_API_KEY];
        
        [Fabric with:@[[Crashlytics class]]];
[FIRApp configure];
        //setting reachibility
        Reachability *internetReach = [Reachability reachabilityForInternetConnection];
        [internetReach startNotifier];
        NSLog(@"connection status = %i", internetReach.isReachable);
        if (internetReach.currentReachabilityStatus == NotReachable)
        {
            internetConnection = NO;
            
        }
        else
        {
            internetConnection = YES;
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(internetReachabilityChanged:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"lang"]) {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"lang"] isEqualToString:@"ar"]) {
                [[BundleLocalization sharedInstance]setLanguage:ARABIC_LANG];
            }
            else{
                [[BundleLocalization sharedInstance]setLanguage:ENGLISH_LANG];
            }
        }
        else{
            [[BundleLocalization sharedInstance]setLanguage:ENGLISH_LANG];
        }
        
        
        
        
        publicRemoteNotification = [NSNotification notificationWithName:PUBLIC_REMOTE_NOTIFICATION object:nil];
        
        clientRemoteNotification = [NSNotification notificationWithName:CLIENT_REQUEST_REMOTE_NOTIFICATION object:nil];
        workerRemoteNotification = [NSNotification notificationWithName:WORKER_REQUEST_REMOTE_NOTIFICATION object:nil];
        
        //register to receive notifications
        if(!IS_OS_8_OR_LATER) {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes: UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
        } else {
            
            UIUserNotificationSettings* notificationSettings =
            [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge |
             UIUserNotificationTypeSound |
             UIUserNotificationTypeAlert categories:nil];
            
            [application registerUserNotificationSettings:notificationSettings];
            [application registerForRemoteNotifications];
        }
        
#pragma -mark authorize for camera access
        
//        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
//        if(authStatus == AVAuthorizationStatusAuthorized) {
//            
//        } else if(authStatus == AVAuthorizationStatusDenied){
//            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
//                if(!granted){
//                    
//                }
//            }];
//            
//        } else if(authStatus == AVAuthorizationStatusRestricted){
//            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
//                if(!granted){
//                    
//                }
//            }];
//        } else if(authStatus == AVAuthorizationStatusNotDetermined){
//            // not determined?!
//            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
//                if(!granted){
//                    
//                }
//            }];
//        } else {
//            // impossible, unknown authorization status
//        }
        
        
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
        }
        [locationManager startMonitoringSignificantLocationChanges];
        
        [locationManager startUpdatingLocation];
        
        
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleWordPress color:[UIColor colorWithRed:243/255.0f green:189/255.0f blue:29/255.0f alpha:1]];
        spinner.center = CGPointMake(CGRectGetMidX(screenBounds), CGRectGetMidY(screenBounds));
        [spinner startAnimating];
        [self.window addSubview:spinner];
        [spinner setHidden:YES];
        [self.window sendSubviewToBack:spinner];
        
        modalView  = [[UIView alloc]initWithFrame:CGRectZero];
        modalView.backgroundColor = [UIColor  blackColor];
        modalView.alpha =0.81;
        [self.window addSubview:modalView];
        [modalView setHidden:YES];
        
        application.statusBarStyle = UIStatusBarStyleLightContent;
  
        
        [self changeAppFont];
        
        
        
        
        NSString*storyBoardName=@"Main_ar";
        
        
        if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {

            
            storyBoardName=@"Main_en";
           
        }
        
        
        
        
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
//            storyBoardName=@"Main_ar";
//            if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
////                storyBoardName=@"Main_en";
//                  storyBoardName=@"Main_en";
//            }
//        }
//        else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
////            storyBoardName=@"Main_iPad";
//              storyBoardName=@"Main_ar";
//            if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
////                storyBoardName=@"Main_iPad_en";
//                  storyBoardName=@"Main_en";
//            }
//        }
//        applicationStroryBoard  = [UIStoryboard storyboardWithName:storyBoardName bundle:nil];
//        MenuViewController *vcMenu = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"MenuViewController"];
        
//        if ([[BundleLocalization sharedInstance].language isEqualToString:ARABIC_LANG]) {
//            [SlideNavigationController sharedInstance].rightMenu = vcMenu ;
//        }
//        else{
//            [SlideNavigationController sharedInstance].leftMenu = vcMenu ;
//        }
       
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"loged_user_type"]) {
//            CurrentUserType = [[[NSUserDefaults standardUserDefaults]objectForKey:@"loged_user_type"]intValue];
//            if (CurrentUserType==WORKER) {
//                CurrentLogedUserInfo= [[Worker alloc] initFromUserDefaults];
//                
//            }
//            else{
//                CurrentLogedUserInfo= [[User alloc] initFromUserDefaults];
//                
//            }
            CurrentLogedUserInfo= [[User alloc] initFromUserDefaults];

            CurrentLogedUserInfo.userType = CurrentUserType;
            
        }
        else{
            
            //Navigate to Auth Storyboard
            //TODO: Localize Auth Storyboard
            
            storyBoardName=@"Auth_ar";

            // storyBoardName=@"Main_ar";
            if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
                //                storyBoardName=@"Main_en";
                storyBoardName=@"Auth_en";
            }
            
//            applicationStroryBoard  = [UIStoryboard storyboardWithName:@"Auth_ar" bundle:nil];
            CurrentUserType = PUBLIC_USER;
        }
        
        if (CurrentUserType==WORKER) {
            
            storyBoardName = @"WorkerMain_ar";
            
            if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
                //                storyBoardName=@"Main_en";
                storyBoardName=@"WorkerMain_en";
            }
                        WorkerMainViewController *vcWorkerMainView = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"WorkerMainViewController"];
                        vcWorkerMainView.comeFromMenu = YES;
                        [SlideNavigationController sharedInstance].viewControllers =@[vcWorkerMainView];
            self.window.rootViewController = vcWorkerMainView;
            [self.window makeKeyAndVisible];
        }else {
            
              [self instantiateInitialViewControllerWithstoryboard:storyBoardName];
            
        }
        
        
      
        
//
//        else if (CurrentUserType==CLIENT) {
//            ClientMainViewController *vcClientMainScreen = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"ClientMainViewController"];
//            vcClientMainScreen.comeFromMenu = YES;
//            [SlideNavigationController sharedInstance].viewControllers =@[vcClientMainScreen];
//        }
//        else {
//            WelcomeViewController *vcMain = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"WelcomeViewController"];
//            vcMain.comeFromMenu  = YES;
//            [SlideNavigationController sharedInstance].viewControllers =@[vcMain];
//        }
        
    }
    @catch (NSException *exception) {
        
    }
    [self.window makeKeyAndVisible];
    return YES;
}


-(void)changeAppFont{
    
    @try {
        
    
        for (NSString *familyName in [UIFont familyNames]){
            NSLog(@"Family name: %@", familyName);
            for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
                NSLog(@"--Font name: %@", fontName);
            }
        }
        
    NSString *fontName = ARABIC_FONT;
    
    
    //Arabic Font :GESSTwoMedium-Medium
    //English Font :SFUIDisplay-Regular
    
    if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
        
        
               fontName = ENGLISH_FONT;
    }
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        NSFontAttributeName:[UIFont fontWithName:fontName size:13.0f]
                                                        } forState:UIControlStateNormal];
    
//    [[UINavigationBar appearance] setTitleTextAttributes: @{
//    
//                                                            NSFontAttributeName: [UIFont fontWithName:fontName size:18.0f],
//                                                            
//                                                            }];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{
                                                              NSFontAttributeName:[UIFont fontWithName:fontName size:13.0f]
                                                              } forState:UIControlStateNormal];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{
                                                              NSFontAttributeName:[UIFont fontWithName:fontName size:13.0f]
                                                              } forState:UIControlStateSelected];
        
    }@catch (NSException *exception) {
        
    }
    

}

-(void)instantiateInitialViewControllerWithstoryboard:(NSString *)name{
 
      applicationStroryBoard  = [UIStoryboard storyboardWithName:name bundle:nil];
    UIViewController *vc = [applicationStroryBoard instantiateInitialViewController];
    
    [self.window setRootViewController:vc ];
    [self.window makeKeyAndVisible];
    
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma -mark other methods
-(void)showNoDataMessage{
    @try {
        [TSMessage showNotificationInViewController:self.window.rootViewController title:nil subtitle:NSLocalizedString(@"no_data_found", nil)  image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
    }
    @catch (NSException *exception) {
        
    }
}

-(void)showRequestCanceledMessage{
    @try {
        [TSMessage showNotificationInViewController:self.window.rootViewController title:nil subtitle:NSLocalizedString(@"request_Not_Accepted", nil)  image:nil type:TSMessageNotificationTypeError duration:5.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
    }
    @catch (NSException *exception) {
        
    }
}

-(void)showModalView:(BOOL)show WithContainerViewController:(UIViewController*)controller{
    @try {
        if (show) {
            [controller.view addSubview:modalView];
            modalView.alpha =0.9f;
            modalView.frame = CGRectMake(0, ((MasterViewController*)controller).headerView.frame.size.height, ((MasterViewController*)controller).view.frame.size.width,controller.view.frame.size.height - (((MasterViewController*)controller).headerView.frame.size.height));
            [modalView setHidden:NO];
            [controller.view bringSubviewToFront:modalView];
            
        }
        else{
            [modalView setHidden:YES];
            [modalView removeFromSuperview];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}

-(void)showNoInternetMessage{
    @try {
        [self showFullLoadingView:NO WithContainerViewController:self.window.rootViewController];
        [TSMessage showNotificationInViewController:self.window.rootViewController title:nil subtitle:NSLocalizedString(@"internet_not_reached", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
    }
    @catch (NSException *exception) {
        
    }
}

-(void) prepartoAskForAsyncTask :(NSDictionary*) order{
    
    
   
                               
    NSTimer* timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(NewOrderAsyncTask:)
                                                    userInfo:order repeats:NO];
    
    
}

-(void)NewOrderAsyncTask : (NSTimer*)theTimer{
    
    NSLog (@"Got the string: %@", (NSDictionary*)[theTimer userInfo]);
    NSDictionary *order = (NSDictionary*)[theTimer userInfo];
    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:20];
    localNotification.alertBody = @"orderStatus";
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.userInfo = order;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    [JDStatusBarNotification showWithStatus:@"Requesting" dismissAfter:20 ];
    
    WaitingViewController *viewControllerForPopover = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"WaitingViewController"];
    viewControllerForPopover.view.frame = CGRectMake(0, 0, 310, 280);

    
    ClientMainViewController *MAINVC = ((UINavigationController*) ( ((UITabBarController*)self.window.rootViewController).selectedViewController)).visibleViewController;

    //
    [MAINVC presentPopupViewController:viewControllerForPopover animationType:MJPopupViewAnimationFade ];

    
    @try {
        if (internetConnection) {
//            SHOW_FULL_LOADINGg
            [[ServiceLogic sharedInstance] NewOrderAsyncTask:order Complession:^(id response) {
                dispatch_sync(dispatch_get_main_queue(), ^{
//                    HIDE_FULL_LOADING
                    if (response) {
//                        [TSMessage dismissActiveNotification];
//                        [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"send_order_success", nil) image:nil type:TSMessageNotificationTypeSuccess duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
//                        NSDictionary* track_order_data = @{@"parent_id":@"parent_id",@"lang":[NSString stringWithFormat:@"%@",[BundleLocalization sharedInstance].language],@"client_id":[NSString stringWithFormat:@"%i",CurrentLogedUserInfo.userID]};
//                        NSDictionary* order =@{
//                                               @"some_data":@"", @"track_order_endpoint": @"http://mandobiapp.com/c_m_s/track_order_for_client.php", @"track_order_data":track_order_data};
//                        
//                        [APP_DELEGATE prepartoAskForAsyncTask:order];
//                        
//                        //                        ClientOrdersViewController*vcOrders = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"ClientOrdersViewController"];
//                        //                        vcOrders.comeFromMenu = YES;
//                        //                        self.navigationController.viewControllers=@[vcOrders];
//                        [self.tabBarController setSelectedIndex:0];
                    }
                    else{
                        [TSMessage dismissActiveNotification];
                        [TSMessage showNotificationInViewController:_window.rootViewController.presentedViewController  title:nil subtitle:NSLocalizedString(@"send_order_fail", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                    }
                });
            } ];
        }
        else{
//            SHOW_NO_INTERNET_MESSAGE
        }
    }
    @catch (NSException *exception) {
        
    }

    
    
    
    
}
-(void)dismisPopUps{
    
    
}
-(void)showFullLoadingView:(BOOL)show WithContainerViewController:(UIViewController*)controller{
    @try {
        if (show) {
            
            modalView.alpha =0.75;
            modalView.frame = self.window.bounds; //CGRectMake(0, ((MasterViewController*)controller).headerView.frame.size.height, ((MasterViewController*)controller).view.frame.size.width,controller.view.frame.size.height - (((MasterViewController*)controller).headerView.frame.size.height));
            [self.window addSubview:modalView];
            [modalView setHidden:NO];
            [self.window bringSubviewToFront:modalView];
            [spinner setHidden:NO];
            [self.window bringSubviewToFront:spinner];
        }
        else{
            [spinner setHidden:YES];
            [modalView setHidden:YES];
            [self.window sendSubviewToBack:spinner];
            [modalView removeFromSuperview];
        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark remote notifications delegate methods

-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{
    
 
    [application registerForRemoteNotifications];
}



- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *) deviceToken
{
    @try {
        NSString* newToken = [deviceToken description];
        newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
        newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
//        NSLog(@"token %@",newToken);
//        [[[UIAlertView alloc]initWithTitle:nil message:newToken delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil]show];
        
        NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
        [defaults setValue:newToken forKey:@"token"];
        [defaults synchronize];
        NSLog(@"token :%@",newToken);
//        NSString*token = [[NSUserDefaults standardUserDefaults]objectForKey:@"token"];
        if (newToken) {
            [[ServiceLogic sharedInstance] sendNotificationToken:newToken ForWorker:(Worker*)CurrentLogedUserInfo Complession:^(id response) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    if (response) {
                        
                    }
                    else{
                        
                    }
                });
                
            }];
        }
        else{
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    
    
}
-(void)sendNotification
{
    @try {
        
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
            
            NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"notify" ofType:@"mp3"];
            SystemSoundID soundID;
            AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath: soundPath], &soundID);
            AudioServicesPlaySystemSound (soundID);
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self.window.rootViewController title:nil subtitle:remoteNotificationInfo[@"aps"][@"alert"][@"body"]image:nil type:TSMessageNotificationTypeSuccess duration:30.0 callback:^{
                UserType userType = [[[NSUserDefaults standardUserDefaults]objectForKey:@"loged_user_type"]intValue];
                
                if ([[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]==4 || [[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]==5) {
                    if (CurrentUserType==WORKER && [[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]==4) {
                        [[NSNotificationCenter defaultCenter] postNotification:publicRemoteNotification];
                        
                    }
                    if (CurrentUserType==CLIENT && [[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]==5) {
                        [[NSNotificationCenter defaultCenter] postNotification:publicRemoteNotification];
                        
                    }
                }
                else{
                    
                    if (userType==WORKER) {
                        [[NSNotificationCenter defaultCenter] postNotification:clientRemoteNotification];
                    }
                    else if (userType==CLIENT){
                        [[NSNotificationCenter defaultCenter] postNotification:workerRemoteNotification];
                    }
                    
                }
                
                [TSMessage dismissActiveNotification];
            } buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
        }
        else{
            UserType userType = [[[NSUserDefaults standardUserDefaults]objectForKey:@"loged_user_type"]intValue];
            if ([[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]==4 || [[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]==5) {
                if (CurrentUserType==WORKER && [[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]==4) {
                    [[NSNotificationCenter defaultCenter] postNotification:publicRemoteNotification];
                    
                }
                if (CurrentUserType==CLIENT && [[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]==5) {
                    [[NSNotificationCenter defaultCenter] postNotification:publicRemoteNotification];
                    
                }
            }
            else{
                if (userType==WORKER) {
                    [[NSNotificationCenter defaultCenter] postNotification:clientRemoteNotification];
                }
                else if (userType==CLIENT){
                    [[NSNotificationCenter defaultCenter] postNotification:workerRemoteNotification];
                }
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
//-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
//{
//    @try {
////        [[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@",userInfo] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
//        
//        
//        
////        remoteNotificationInfo = userInfo;
////        if ([[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]==4 || [[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]==5) {
////            if (CurrentUserType==WORKER && [[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]==4) {
////                [self performSelector:@selector(sendNotification) withObject:nil afterDelay:1];
////                
////            }
////            if (CurrentUserType==CLIENT && [[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]==5) {
////                [self performSelector:@selector(sendNotification) withObject:nil afterDelay:1];
////                
////            }
////
////        }
////        else{
////            [self performSelector:@selector(sendNotification) withObject:nil afterDelay:1];
////
////        }
//        
//    }
//    @catch (NSException *exception) {
//        
//    }
//    @finally {
//        
//    }
//}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    NSLog(@"%@",notification.userInfo);
    SelectDriverViewController *viewControllerForPopover = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"SelectDriverViewController"];
    viewControllerForPopover.view.frame = CGRectMake(0, 0, 310, 280);
    viewControllerForPopover.parentId = [[notification.userInfo  objectForKey:@"parent_id"]intValue];
    viewControllerForPopover.clientPrice = [notification.userInfo  objectForKey:@"price"];
//    
//    SamplePopupViewController *samplePopupViewController = [[SamplePopupViewController alloc] initWithNibName:@"SamplePopupViewController" bundle:nil];
//    [self presentPopupViewController:samplePopupViewController animated:YES completion:nil];
    
    [JDStatusBarNotification dismiss];
    ((UITabBarController*)self.window.rootViewController).selectedIndex = 0;
    [((UINavigationController*) ( ((UITabBarController*)self.window.rootViewController).selectedViewController)) popToRootViewControllerAnimated:NO];
    
    ClientMainViewController *MAINVC = ((UINavigationController*) ( ((UITabBarController*)self.window.rootViewController).selectedViewController)).visibleViewController;
    
    viewControllerForPopover.mainVc = MAINVC;
     viewControllerForPopover.parentId = [[notification.userInfo  objectForKey:@"parent_id"]intValue];
    [viewControllerForPopover loadData:[[notification.userInfo  objectForKey:@"parent_id"]integerValue]];
    viewControllerForPopover.clientPrice = [notification.userInfo  objectForKey:@"price"];

//    MAINVC.popover = [[UIPopoverController alloc] initWithContentViewController:viewControllerForPopover];
//    [ MAINVC.popover  presentPopoverFromRect:MAINVC.view.frame inView:MAINVC.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//
    
    [MAINVC dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
//    [MAINVC presentPopupViewController:viewControllerForPopover animationType:MJPopupViewAnimationFade ];


    
    PopupDialog *popup =  [[PopupDialog alloc]initWithViewController:viewControllerForPopover buttonAlignment:UILayoutConstraintAxisHorizontal transitionStyle:PopupDialogTransitionStyleBounceUp
                                                    gestureDismissal:NO completion:nil ];
    viewControllerForPopover.popup = popup;
    
    //[[PopupDialog alloc] initWithTitle:@"TEST"
//                                                    message:@"This is a test message!"
//                                                      image:nil
//                                            buttonAlignment:UILayoutConstraintAxisHorizontal
//                                            transitionStyle:PopupDialogTransitionStyleBounceUp
//                                           gestureDismissal:YES
//                                                 completion:nil];
    
    CancelButton *cancel = [[CancelButton alloc] initWithTitle:@"CANCEL" dismissOnTap:YES action:^{
        // Default action
    }];
    
    DefaultButton *ok = [[DefaultButton alloc] initWithTitle:@"OK" dismissOnTap:YES action:^{
        // Ok action
    }];
    
//    [popup addButtons: @[cancel, ok]];
    
    [MAINVC presentViewController:popup animated:YES completion:nil];
    
    
    
//    [((UINavigationController*) ( ((UITabBarController*)self.window.rootViewController).selectedViewController)).visibleViewController presentPopupViewController:vc animated:true completion:nil];

    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification: (NSDictionary *)userInfo {
    @try {
        //        [[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@",userInfo] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
        
        
        
                remoteNotificationInfo = userInfo;
//          [TSMessage showNotificationInViewController:self.window.rootViewController title:nil subtitle:[[remoteNotificationInfo [@"aps"] objectForKey:@"body"]stringValue] image:nil type:TSMessageNotificationTypeError duration:5.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
        
        
        
       //  type: [[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]
        //order_id :[[remoteNotificationInfo [@"aps"] objectForKey:@"order_id"]intValue]
        
        // "get_driver_responses_end_point" = "http://localhost/mandobi/c_m_s/get_order_responses.php";

        
                if ([remoteNotificationInfo  objectForKey:@"parent_id"]) {
                    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"notify" ofType:@"mp3"];
                    SystemSoundID soundID;
                    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath: soundPath], &soundID);
                    AudioServicesPlaySystemSound (soundID);
                    
//                    if (CurrentUserType==WORKER && [[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]==4) {
//                        [self performSelector:@selector(sendNotification) withObject:nil afterDelay:1];
//        
//                    }
//                    if (CurrentUserType==CLIENT && [[remoteNotificationInfo [@"aps"] objectForKey:@"type"]intValue]==5) {
//                        [self performSelector:@selector(sendNotification) withObject:nil afterDelay:1];
//        
//                    }
                SelectDriverViewController *vc = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"SelectDriverViewController"];
                    vc.parentId = [[remoteNotificationInfo valueForKey:@"parent_id"] integerValue]; //[[NSNumber alloc] initWithInt:[remoteNotificationInfo  objectForKey:@"parent_id"]].intValue;
                    
                 // ((UINavigationController*) ( ((UITabBarController*)self.window.rootViewController).selectedViewController)).visibleViewController;

                    //
//                    ((UINavigationController*)self.window.rootViewController.childViewControllers.firstObject).visibleViewController
                    
                    [((UINavigationController*) ( ((UITabBarController*)self.window.rootViewController).selectedViewController)).visibleViewController presentViewController:vc animated:true completion:nil];
        
                }
                else{
//                    [self performSelector:@selector(sendNotification) withObject:nil afterDelay:1];
        
                }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

#pragma mark - Reachability for internet connection
- (void)internetReachabilityChanged:(NSNotification *)note
{
    @try {
        //commit
        NetworkStatus ns = [(Reachability *)[note object] currentReachabilityStatus];
        if (ns == NotReachable)
        {
            
            if (internetConnection)
            {
                internetConnection = NO;
                [TSMessage showNotificationInViewController:self.window.rootViewController title:nil subtitle:NSLocalizedString(@"internet_not_reached", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            }
        }
        else
        {
            //check for both with/out internet connection
            if (!internetConnection)
            {
                internetConnection = YES;
                
            }
        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark Location manager delegate methods
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    @try {
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    @try {
        [locationManager startUpdatingLocation];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    @try {
        
        CLLocation *currentLocation = newLocation;
        NSString *_lat,*_long;
        if (currentLocation != nil) {
            _long= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
            _lat= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
            
            [[NSUserDefaults standardUserDefaults] setObject:@"latitude" forKey:_lat];
            [[NSUserDefaults standardUserDefaults] setObject:@"longitude" forKey:_long];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            
            CurrentLogedUserInfo.longitude =currentLocation.coordinate.longitude;
            CurrentLogedUserInfo.latitude =currentLocation.coordinate.latitude;
            
            
            
        }
        
        [locationManager stopUpdatingLocation];
        
    }
    @catch (NSException *exception) {
        
    }
}

@end
