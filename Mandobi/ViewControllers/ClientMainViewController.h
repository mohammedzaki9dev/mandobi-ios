//
//  ClientMainViewController.h
//  Mandobi
//
//  Created by Assem Imam on 3/8/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"
#import "HMSegmentedControl.h"

@interface ClientMainViewController : MasterViewController
{
    __weak IBOutlet UIView *autoCompleteView;
    __weak IBOutlet UITableView *autoCompleteTableView;
    __weak IBOutlet UIView *mapViewContainer;
        __weak IBOutlet UIView *markerHolder;
    __weak IBOutlet UITextField *toAddressTextField;
    __weak IBOutlet UITextField *fromAddressTextField;
    __weak IBOutlet UIView *fromAddressView;
    __weak IBOutlet UIView *toAddressView;
    
    
    
}
@property (nonatomic) UIPopoverController *popover;

- (IBAction)toAddressSearchButtonAction:(UIButton *)sender;
- (IBAction)fromAddressSearchButtonAction:(UIButton *)sender;
- (void)closePopup;
@end
