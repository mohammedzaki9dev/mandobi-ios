//
//  ClientMainViewController.m
//  Mandobi
//
//  Created by Assem Imam on 3/8/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "ClientMainViewController.h"
#import "SendOrderView.h"
#import "LMGeocoder.h" 
#import "OrderInformationViewController.h"
#import "AutoCompleteCell.h"
#import "NewOrderViewController.h";
@import GooglePlaces;

@interface ClientMainViewController ()<CLLocationManagerDelegate,GMSMapViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    

    __weak IBOutlet UILabel *timerLabel;
    __weak IBOutlet UIButton *toSearchBTN;
    
    __weak IBOutlet UIButton *fromSearchBtm;
    
    __weak IBOutlet GMSMapView *mapView;
   // GMSMapView*mapView;
    CLLocationManager *locationManager;
    CLLocation*selectedLocation;
    BOOL locationSubmitted;
    NSTimer *locationTimer;
    BOOL currentLocationMarkerAdded;
//    SendOrderView *vSendOrder;
    VehicleType selectedVehicleType;
    GMSMarker *currentLocationMarker ;
    GMSMarker *destimationLocationMarker ;
    GMSCircle *circ;
    CLLocation *fromLocation;
    CLPlacemark* fromplacemark;
    CLLocation *toLocation;
    CLPlacemark* toplacemark;
    NSArray *placesList;
    CLLocation *currentMapLocation;
}


@property (weak, nonatomic) IBOutlet UILabel *soonLabel;
@property(weak,nonatomic) NSTimer *updateDriverTimer;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong, nonatomic)  NSMutableArray *markersArray;

@end

@implementation ClientMainViewController{
    
 GMSPlacesClient *_placesClient;
}




- (void)closePopup
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}


-(void)zoomToMyLocation
{
    NSLog(@" zoom to location ");
    // mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
    @try {
        dispatch_async(dispatch_get_main_queue(), ^{
        [mapView setCamera:[GMSCameraPosition cameraWithLatitude:fromLocation.coordinate.latitude
                                                       longitude:fromLocation.coordinate.longitude
                            
                                                            zoom:17
                                                         bearing:0
                                                    viewingAngle:0]];
        [mapView animateToLocation:CLLocationCoordinate2DMake(fromLocation.coordinate.latitude, fromLocation.coordinate.longitude)];
        });
        }
    @catch (NSException *exception) {
        
    }
}
- (void)initGPS {
    @try {
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
        }
        if([CLLocationManager locationServicesEnabled]){
            
        }
        else{
            
        }
        [locationManager startMonitoringSignificantLocationChanges];
        [locationManager requestLocation];
    
        [locationManager startUpdatingLocation];
    }
    @catch (NSException *exception) {
        
    }
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    
    return UIStatusBarStyleDefault;
}

-(void)locationTimerSelector
{
    locationSubmitted=NO;
    [locationManager startMonitoringSignificantLocationChanges];
    [locationManager startUpdatingLocation];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    //[self.navigationController setNavigationBarHidden:YES];
   // [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.view setNeedsLayout];
    
//    
//    NSString *fontName = ARABIC_FONT;
//    
//    
//    //Arabic Font :GESSTwoMedium-Medium
//    //English Font :SFUIDisplay-Regular
//    
//    if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
//        
//        
//        fontName = ENGLISH_FONT;
//    }
//    UIFont *font = [UIFont fontWithName:fontName size:17.0f];
//    
//
//    
//
//    UILabel* lbNavTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,40,320,40)];
//    lbNavTitle.textAlignment = UITextAlignmentCenter;
//    lbNavTitle.text = @"MANDOBI";
//    lbNavTitle.textColor = [UIColor blackColor];
//    lbNavTitle.font = font;
//    self.navigationItem.titleView = lbNavTitle;
    
//    self.soonLabel.text = NSLocalizedString(@"soon", @"soon");
    selectedVehicleType = 1;
     _placesClient = [GMSPlacesClient sharedClient];
    
      HMSegmentedControl *segmentedControl2 = [[HMSegmentedControl alloc] initWithSectionImages:@[[UIImage imageNamed:@"smallVehicle"],[UIImage imageNamed:@"mediumVehicle"]] sectionSelectedImages:@[[UIImage imageNamed:@"smallVehicle_filled"],[UIImage imageNamed:@"mediumVehicle"]]];
    segmentedControl2.frame = CGRectMake(0, 5, self.view.bounds.size.width, 40);
    segmentedControl2.selectionIndicatorHeight = 4.0f;
    segmentedControl2.backgroundColor = [UIColor clearColor];
    segmentedControl2.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl2.touchEnabled = NO;
    segmentedControl2.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
   // [segmentedControl2 addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    //[self.view addSubview:segmentedControl2];
    
    @try {
        
//        [CommonMethods FormatView:fromAddressView WithShadowRadius:2 CornerRadius:0 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];
//        [CommonMethods FormatView:toAddressView WithShadowRadius:2 CornerRadius:0 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];

        //adding googlemaps mapview
        
//        mapView = [GMSMapView mapWithFrame:CGRectZero camera:nil];
        mapView.mapType=kGMSTypeNormal;
        mapView.delegate = self;
        [mapView setMyLocationEnabled:YES];
        [mapView setMinZoom:0 maxZoom:30];
        [mapView setIndoorEnabled:YES];
        mapView.settings.compassButton = YES;
        mapView.settings.myLocationButton=YES;
//        [mapViewContainer addSubview:mapView];
//        [mapViewContainer layoutIfNeeded];
//        [mapView setFrame:CGRectMake(0, 0, self.view.frame.size.width, mapViewContainer.bounds.size.height)];
//          [mapViewContainer bringSubviewToFront:markerHolder];
        
        //GPS Code
        currentLocationMarkerAdded = NO;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        [self initGPS];
        
        
        //add send order view
//        vSendOrder  =(SendOrderView*) [[[NSBundle mainBundle]loadNibNamed:NSLocalizedString(@"SendOrderView", nil) owner:nil options:nil]objectAtIndex:0];
//        [vSendOrder setFrame:CGRectMake(0, self.view.frame.size.height - vSendOrder.menuButton.frame.size.height, self.view.frame.size.width, vSendOrder.frame.size.height)];
//        [self.view addSubview:vSendOrder];
//        [self.view bringSubviewToFront:vSendOrder];
//        [vSendOrder.menuButton addTarget:self action:@selector(sendOrderMenuButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//        [vSendOrder layoutIfNeeded];
//        [CommonMethods FormatView:vSendOrder WithShadowRadius:2 CornerRadius:0 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];
//        [self formatButtonViews:vSendOrder.sendButton];
//        [vSendOrder.smallVehicleButton addTarget:self action:@selector(vehicleTypeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//        [vSendOrder.largeVehicleButton addTarget:self action:@selector(vehicleTypeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//        [vSendOrder.mediumVehicleButton addTarget:self action:@selector(vehicleTypeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//        [vSendOrder.sendButton addTarget:self action:@selector(sendOrderButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
          locationTimer = [NSTimer scheduledTimerWithTimeInterval:60*60*5 target:self selector:@selector(locationTimerSelector) userInfo:nil repeats:YES];
    }
    @catch (NSException *exception) {
        
    }
    
    self.updateDriverTimer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(updateDriverLocations) userInfo:nil repeats:YES];

    
}

-(void)updateDriverLocations{
    
    @try {
        if (CurrentLogedUserInfo != nil && currentMapLocation != nil) {
            
        
        [[ServiceLogic sharedInstance] getAvalibleDriverinRangeForUserID:[NSNumber numberWithInt: CurrentLogedUserInfo.userID ]ForLocation:currentMapLocation forCarType:[NSNumber numberWithInteger:selectedVehicleType  ]  Complession:^(id response ) {
            
            [mapView clear];
            [_markersArray removeAllObjects];
            _markersArray =  [[NSMutableArray alloc] init];
            self.timerLabel.text = @"";
            if (response != nil) {
                
               
            
//            NSMutableArray *newplacesList = [[NSMutableArray alloc]init];
        
            for (NSDictionary *driver in response) {

                CLLocation *location = [[CLLocation alloc] initWithLatitude:[[driver objectForKey:@"latitude"]doubleValue ] longitude:[[driver objectForKey:@"longitude"]doubleValue ]];
                [self addMarkerWith:location.coordinate];
//                [self addMarkerWith:location];
           
            }
                dispatch_async(dispatch_get_main_queue(), ^{
                   self.timerLabel.text = @"5 min";
                    
                });
                
                
            }
            
        }];
    }
//
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    //[self.navigationController setNavigationBarHidden:YES animated:YES];

}

-(void)viewDidDisappear:(BOOL)animated{
    
    [[LMGeocoder sharedInstance] cancelGeocode];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) addMarkerWith:(CLLocationCoordinate2D) location{
    
    
    //    [mapView clear];
    dispatch_async(dispatch_get_main_queue(), ^{
   GMSMarker *carMarker  = [GMSMarker markerWithPosition:location];
        [_markersArray addObject:carMarker];
    carMarker.title = @"";
    carMarker.icon = [UIImage imageNamed:@"smallVehicle_Pin"];
    carMarker.map = mapView;
    });

    
}
- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
    selectedVehicleType = segmentedControl.selectedSegmentIndex + 1;
    
}



// Add a UIButton in Interface Builder to call this function
- (void)getCurrentPlace {
    
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(37.788204, -122.411937);
    CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(center.latitude + 0.001,
                                                                  center.longitude + 0.001);
    CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(center.latitude - 0.001,
                                                                  center.longitude - 0.001);
    GMSCoordinateBounds *viewport = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast
                                                                         coordinate:southWest];
//    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:viewport];
  //  _placePicker = [[GMSPlacePicker alloc] initWithConfig:config];
    
    [_placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *placeLikelihoodList, NSError *error){
        
        
        if (error != nil) {
            NSLog(@"Pick Place error %@", [error localizedDescription]);
            return;
        }
        
        //self.nameLabel.text = @"No current place";
        //self.addressLabel.text = @"";
        
        if (placeLikelihoodList != nil) {
            GMSPlace *place = [[[placeLikelihoodList likelihoods] firstObject] place];
            if (place != nil) {
             NSLog(@"%@", place.name);
                NSLog(@"%@", [[place.formattedAddress componentsSeparatedByString:@", "]
                                          componentsJoinedByString:@"\n"]);
            }
        }
    }];
}


#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    @try {
//        OrderInformationViewController *vcOrderInformation  = (OrderInformationViewController*)segue.destinationViewController;
        NewOrderViewController *vcOrderInformation  = (NewOrderViewController*)segue.destinationViewController;

        Order *submittedOder = [[Order alloc]init];
        submittedOder.fromLocation  = fromLocation ;
        submittedOder.fromPlacemark = fromplacemark;
        submittedOder.fromAddress = fromAddressTextField.text;
        
        //[[Location alloc]initWithLatitude:currentLocationMarker.position.latitude AndLongitude:currentLocationMarker.position.longitude];

        submittedOder.toLocation  =  toLocation;
        submittedOder.toPlacemark = toplacemark;
        submittedOder.toAddress = toAddressTextField.text;
        //
        //[[Location alloc]initWithLatitude:destimationLocationMarker.position.latitude AndLongitude:destimationLocationMarker.position.longitude];
        
//        submittedOder.receipientName = vSendOrder.nameTextField.text;
//        submittedOder.receipientMobile = vSendOrder.phoneTextField.text;
//        submittedOder.message = vSendOrder.shipmentDetailsTextField.text;
//        submittedOder.subject = vSendOrder.shipmentDetailsTextField.text;
        Worker*orderWorker = [[Worker alloc]init];
        orderWorker.vehicleType = selectedVehicleType;
        submittedOder.worker = orderWorker;
        vcOrderInformation.submittedOrder = submittedOder;
       


    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark methods
-(BOOL)validateUserInputs{
    @try {
       
        if ([fromAddressTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"from_address", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            

            return NO;
        }
        if ([toAddressTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"to_address", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            return NO;
        }
        if (selectedVehicleType == 0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"vehicle_type", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            return  NO;
        }
//        if ([vSendOrder.shipmentDetailsTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
//            [TSMessage dismissActiveNotification];
//            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"shipment_details", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
//            
//
//            return NO;
//        }
//        if ([vSendOrder.nameTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
//            [TSMessage dismissActiveNotification];
//            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_name", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
//            
//
//            return NO;
//        }
        
//        if ([vSendOrder.phoneTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length==0) {
//            [TSMessage dismissActiveNotification];
//            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_mobile", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
//            
//            return NO;
//        }
//        if (![CommonMethods CheckPhoneNumber:vSendOrder.phoneTextField.text]) {
//            [TSMessage dismissActiveNotification];
//            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enter_valid_mobile", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
//            
//            return NO;
//        }

        return YES;
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark IBActions
-(IBAction)sendOrderButtonAction:(UIButton*)sender{
    @try {
        if ([self validateUserInputs]) {
            HIDE_MODAL_VIEW
//            [self sendOrderMenuButtonAction:vSendOrder.menuButton];
            [self performSegueWithIdentifier:@"orderInformation" sender:nil];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
- (IBAction)toAddressSearchButtonAction:(UIButton *)sender {
    @try {
//        [self.view endEditing:YES];
        if ([toAddressTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length >0) {
            [[LMGeocoder sharedInstance] geocodeAddressString:toAddressTextField.text
                                                      service:kLMGeocoderGoogleService
                                            completionHandler:^(NSArray *results, NSError *error) {
                    if (results.count && !error) {
                        LMAddress *address = [results firstObject];
                        NSLog(@"Coordinate: (%f, %f)", address.coordinate.latitude, address.coordinate.longitude);
                          destimationLocationMarker.position = address.coordinate;
                         toLocation = [[CLLocation alloc]initWithLatitude:address.coordinate.latitude longitude:address.coordinate.longitude];
//                        [mapView setCamera:[GMSCameraPosition cameraWithLatitude:address.coordinate.latitude
//                                                                       longitude:address.coordinate.longitude
//                                            
//                                                                            zoom:17
//                                                                         bearing:30
//                                                                    viewingAngle:40]];
//                        [mapView animateToLocation:CLLocationCoordinate2DMake(address.coordinate.latitude, address.coordinate.longitude)];
                    }
            }];
            
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)fromAddressSearchButtonAction:(UIButton *)sender {
    @try {
//        [self.view endEditing:YES];
        if ([fromAddressTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length >0) {
            [[LMGeocoder sharedInstance] geocodeAddressString:fromAddressTextField.text
                                                      service:kLMGeocoderGoogleService
                                            completionHandler:^(NSArray *results, NSError *error) {
                if (results.count && !error) {
                    LMAddress *address = [results firstObject];
                    dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"Coordinate: (%f, %f)", address.coordinate.latitude, address.coordinate.longitude);
                    currentLocationMarker.position = address.coordinate;
                    fromLocation = [[CLLocation alloc]initWithLatitude:address.coordinate.latitude longitude:address.coordinate.longitude];
                    [mapView setCamera:[GMSCameraPosition cameraWithLatitude:address.coordinate.latitude
                                                                   longitude:address.coordinate.longitude
                                        
                                                                        zoom:17
                                                                     bearing:30
                                                                viewingAngle:40]];
                    [mapView animateToLocation:CLLocationCoordinate2DMake(address.coordinate.latitude, address.coordinate.longitude)];

                    });
            
                }
           }];

        }
    }
    @catch (NSException *exception) {
        
    }
}
//-(IBAction)vehicleTypeButtonAction:(UIButton*)sender
//{
//    @try {
//        selectedVehicleType = sender.tag;
//        switch (sender.tag) {
//            case SMALL_VEHICLE:
//            {
//                [vSendOrder.smallRadioButton setImage:[UIImage imageNamed:@"radio_select"] forState:UIControlStateNormal];
//                 [vSendOrder.mediumRadioButton setImage:[UIImage imageNamed:@"radio_unselect"] forState:UIControlStateNormal];
//                 [vSendOrder.largeRadioButton setImage:[UIImage imageNamed:@"radio_unselect"] forState:UIControlStateNormal];
//
//            }
//                break;
//                
//            case MEDIUM_VEHICLE:
//            {
//                [vSendOrder.mediumRadioButton setImage:[UIImage imageNamed:@"radio_select"] forState:UIControlStateNormal];
//                [vSendOrder.smallRadioButton setImage:[UIImage imageNamed:@"radio_unselect"] forState:UIControlStateNormal];
//                [vSendOrder.largeRadioButton setImage:[UIImage imageNamed:@"radio_unselect"] forState:UIControlStateNormal];
//
//            }
//                break;
//            case LARGE_VEHICLE:
//            {
//                [vSendOrder.largeRadioButton setImage:[UIImage imageNamed:@"radio_select"] forState:UIControlStateNormal];
//                [vSendOrder.mediumRadioButton setImage:[UIImage imageNamed:@"radio_unselect"] forState:UIControlStateNormal];
//                [vSendOrder.smallRadioButton setImage:[UIImage imageNamed:@"radio_unselect"] forState:UIControlStateNormal];
//
//            }
//                break;
//        }
//    }
//    @catch (NSException *exception) {
//        
//    }
//   
//}
//-(IBAction)sendOrderMenuButtonAction:(UIButton*)sender{
//    @try {
//        if (sender.tag == 0) {
//            SHOW_MODAL_VIEW
//            [self.view bringSubviewToFront:vSendOrder];
//            [UIView animateWithDuration:0.5 animations:^{
//                [vSendOrder setFrame:CGRectMake(0, self.view.frame.size.height - vSendOrder.frame.size.height, vSendOrder.frame.size.width, vSendOrder.frame.size.height)];
//                [vSendOrder layoutIfNeeded];
//                
//            }];
//            
//            sender.tag = 1;
//        }
//        else{
//            
//            [UIView animateWithDuration:0.5 animations:^{
//                 [vSendOrder setFrame:CGRectMake(0, self.view.frame.size.height - vSendOrder.menuButton.frame.size.height, vSendOrder.frame.size.width, vSendOrder.frame.size.height)];
//                [vSendOrder layoutIfNeeded];
//                HIDE_MODAL_VIEW
//
//            }];
//            sender.tag = 0;
//        }
//    }
//    @catch (NSException *exception) {
//        
//    }
//}
#pragma -mark Location manager delegate methods
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    @try {
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    @try {
        [locationManager startUpdatingLocation];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    @try {
        
        CLLocation *newcurrentLocation = newLocation;
        NSString *_lat,*_long;
        if (newcurrentLocation != nil) {
            _long= [NSString stringWithFormat:@"%.8f", newcurrentLocation.coordinate.longitude];
            _lat= [NSString stringWithFormat:@"%.8f", newcurrentLocation.coordinate.latitude];
            
            selectedLocation =
            [[CLLocation alloc] initWithLatitude:newcurrentLocation.coordinate.latitude
                                       longitude:newcurrentLocation.coordinate.longitude];
            
            CurrentLogedUserInfo.longitude =newcurrentLocation.coordinate.longitude;
            CurrentLogedUserInfo.latitude =newcurrentLocation.coordinate.latitude;
            fromLocation = newcurrentLocation;
            currentMapLocation = newLocation;
            //toLocation  = currentLocation;

            
         //   CLLocationCoordinate2D circleCenter = CLLocationCoordinate2DMake(latitude, longitude);
            
//           circ = [GMSCircle circleWithPosition:currentLocation.coordinate radius:10000];
//            
//            circ.fillColor = [UIColor colorWithRed:50.0f/256.0f green:100.0f/256.0f blue:170.0f/256.0f alpha:0.1f];
//            circ.strokeColor = [UIColor redColor];
//            circ.strokeWidth = 1;
//            circ.map = mapView;
            
            [self zoomToMyLocation];
//            if (currentLocationMarkerAdded == NO) {
//                CLLocationCoordinate2D position = currentLocation.coordinate;
//                currentLocationMarker = [GMSMarker markerWithPosition:position];
//                currentLocationMarker.title = @"From Address";
//                currentLocationMarker.icon = [UIImage imageNamed:@"marker٣٣"];
//                currentLocationMarker.map = mapView;
//                currentLocationMarker.draggable = YES;
//                currentLocationMarker.accessibilityLabel =@"0";
//                currentLocationMarkerAdded = YES;
//
//                destimationLocationMarker = [GMSMarker markerWithPosition:position];
//                destimationLocationMarker.title = @"To Address";
////              destimationLocationMarker.icon = [UIImage imageNamed:@"marker٣٣"];
//                destimationLocationMarker.icon  = [GMSMarker markerImageWithColor:[UIColor greenColor]];
//                destimationLocationMarker.map = mapView;
//                currentLocationMarker.accessibilityLabel =@"1";
//
//                destimationLocationMarker.draggable = YES;
//               
//
//            }
          if (locationSubmitted== NO) {
                if (internetConnection) {
                    [[ServiceLogic sharedInstance]  updateLocationForUser:CurrentLogedUserInfo Complession:^(id response) {
//                        [[LMGeocoder sharedInstance] reverseGeocodeCoordinate:currentLocation.coordinate
//                                                                      service:kLMGeocoderGoogleService
//                                                            completionHandler:^(NSArray *results, NSError *error) {
//                                                                if (results.count && !error) {
//                                                                    LMAddress *address = [results firstObject];
//                                                                    NSLog(@"Address: 33/ %@", address.formattedAddress);
//                                                                    fromAddressTextField.text =address.formattedAddress;
//                                                                }
//                                                            }];
                      

                        locationSubmitted = YES;
                    }];
                }
                else{
                    SHOW_NO_INTERNET_MESSAGE
                }
            }
            
        }
        
        [locationManager stopUpdatingLocation];
        
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark mapview delegate methods
- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker
{
    @try {
        if ([marker.accessibilityLabel intValue] == 0) {
            [[LMGeocoder sharedInstance] reverseGeocodeCoordinate:marker.position
                                                          service:kLMGeocoderGoogleService
            completionHandler:^(NSArray *results, NSError *error) {
                if (results.count && !error) {
                    LMAddress *address = [results firstObject];
                    NSLog(@"Address: %@", address.formattedAddress);
                    toAddressTextField.text =address.formattedAddress;
                    toLocation = [[CLLocation alloc]initWithLatitude:marker.position.latitude longitude:marker.position.longitude];
                }
            }];
        }
        else{
            [[LMGeocoder sharedInstance] reverseGeocodeCoordinate:marker.position
                                                          service:kLMGeocoderGoogleService
                completionHandler:^(NSArray *results, NSError *error) {
                    if (results.count && !error) {
                        LMAddress *address = [results firstObject];
                        NSLog(@"Address: %@", address.formattedAddress);
                        fromAddressTextField.text =address.formattedAddress;
                        fromLocation = [[CLLocation alloc]initWithLatitude:marker.position.latitude longitude:marker.position.longitude];
                    }
                }];
        }
    }
    @catch (NSException *exception) {
        
    }
}


-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    @try {
        
    
    CLLocationCoordinate2D newCoords = position.target;
    currentMapLocation =  [[CLLocation alloc ] initWithLatitude:position.target.latitude longitude:position.target.longitude];
    //[self updateDriverLocations];
    NSLog(@" %f  %f",newCoords.latitude  , newCoords.longitude);
    
    CLLocation* eventLocation = [[CLLocation alloc] initWithLatitude:newCoords.latitude  longitude:newCoords.longitude];
    
    
    [[LMGeocoder sharedInstance] reverseGeocodeCoordinate:eventLocation.coordinate
                                                  service:kLMGeocoderGoogleService
                                        completionHandler:^(NSArray *results, NSError *error) {
                                            if (results.count && !error) {
                                                
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                LMAddress *address = [results firstObject];
                                                NSLog(@"Address: 55/ %@", address.formattedAddress);
                                                
                                                fromAddressTextField.text =address.formattedAddress;
                                                 });
                                                
                                                
                                                
                                                
                                            }
                                        }];
    
    
    [self getAddressFromLocation:eventLocation complationBlock:^(NSString * address) {
//        if(address) {
//        NSLog(@" address 44 %@", address);
//            
//            fromAddressTextField.text = address;
//        }
    }];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
   // [self getCurrentPlace];
}

typedef void(^addressCompletion)(NSString *);

-(void)getAddressFromLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock
{
    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             
             /*
              
              @property (nonatomic, readonly, copy, nullable) NSString *name; // eg. Apple Inc.
              @property (nonatomic, readonly, copy, nullable) NSString *thoroughfare; // street name, eg. Infinite Loop
              @property (nonatomic, readonly, copy, nullable) NSString *subThoroughfare; // eg. 1
              @property (nonatomic, readonly, copy, nullable) NSString *locality; // city, eg. Cupertino
              @property (nonatomic, readonly, copy, nullable) NSString *subLocality; // neighborhood, common name, eg. Mission District
              @property (nonatomic, readonly, copy, nullable) NSString *administrativeArea; // state, eg. CA
              @property (nonatomic, readonly, copy, nullable) NSString *subAdministrativeArea; // county, eg. Santa Clara
              @property (nonatomic, readonly, copy, nullable) NSString *postalCode; // zip code, eg. 95014
              @property (nonatomic, readonly, copy, nullable) NSString *ISOcountryCode; // eg. US
              @property (nonatomic, readonly, copy, nullable) NSString *country; // eg. United States
              @property (nonatomic, readonly, copy, nullable) NSString *inlandWater; // eg. Lake Tahoe
              @property (nonatomic, readonly, copy, nullable) NSString *ocean; /
              */
             placemark = [placemarks lastObject];
             address = [[NSString alloc] init];
             if (placemark.subThoroughfare != nil){
                 [address stringByAppendingString: [NSString stringWithFormat:@" %@",placemark.subThoroughfare]];
             }
             if (placemark.name != nil){
                 [address stringByAppendingString: [NSString stringWithFormat:@" %@",placemark.name]];
             }
             
             if (placemark.subLocality != nil){
                 [address stringByAppendingString: [NSString stringWithFormat:@" %@",placemark.subLocality]];
             }
             
             if (placemark.locality != nil){
                 [address stringByAppendingString: [NSString stringWithFormat:@" %@",placemark.locality]];
             }
             
             if (placemark.subAdministrativeArea != nil){
                 [address stringByAppendingString: [NSString stringWithFormat:@" %@",placemark.subAdministrativeArea]];
             }
             
             if (placemark.administrativeArea != nil){
                 [address stringByAppendingString: [NSString stringWithFormat:@" %@",placemark.administrativeArea]];
             }
             
             if (placemark.country != nil){
                 [address stringByAppendingString: [NSString stringWithFormat:@" %@",placemark.country]];
             }
             
             fromplacemark = placemark;
             
             
//             address = [NSString stringWithFormat:@"%@, %@  %@ %@, %@ %@  %@", placemark.subThoroughfare,placemark.name, placemark.subLocality,  placemark.locality,placemark.subAdministrativeArea , placemark.administrativeArea , placemark.country];
             completionBlock(address);
             
             
         }
     }];
}

-(void)getAddressToLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock
{
    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             
             /*
              
              @property (nonatomic, readonly, copy, nullable) NSString *name; // eg. Apple Inc.
              @property (nonatomic, readonly, copy, nullable) NSString *thoroughfare; // street name, eg. Infinite Loop
              @property (nonatomic, readonly, copy, nullable) NSString *subThoroughfare; // eg. 1
              @property (nonatomic, readonly, copy, nullable) NSString *locality; // city, eg. Cupertino
              @property (nonatomic, readonly, copy, nullable) NSString *subLocality; // neighborhood, common name, eg. Mission District
              @property (nonatomic, readonly, copy, nullable) NSString *administrativeArea; // state, eg. CA
              @property (nonatomic, readonly, copy, nullable) NSString *subAdministrativeArea; // county, eg. Santa Clara
              @property (nonatomic, readonly, copy, nullable) NSString *postalCode; // zip code, eg. 95014
              @property (nonatomic, readonly, copy, nullable) NSString *ISOcountryCode; // eg. US
              @property (nonatomic, readonly, copy, nullable) NSString *country; // eg. United States
              @property (nonatomic, readonly, copy, nullable) NSString *inlandWater; // eg. Lake Tahoe
              @property (nonatomic, readonly, copy, nullable) NSString *ocean; /
              */
             placemark = [placemarks lastObject];
             address = [[NSString alloc] init];
             if (placemark.subThoroughfare != nil){
                 [address stringByAppendingString: [NSString stringWithFormat:@" %@",placemark.subThoroughfare]];
             }
             if (placemark.name != nil){
                 [address stringByAppendingString: [NSString stringWithFormat:@" %@",placemark.name]];
             }
             
             if (placemark.subLocality != nil){
                 [address stringByAppendingString: [NSString stringWithFormat:@" %@",placemark.subLocality]];
             }
             
             if (placemark.locality != nil){
                 [address stringByAppendingString: [NSString stringWithFormat:@" %@",placemark.locality]];
             }
             
             if (placemark.subAdministrativeArea != nil){
                 [address stringByAppendingString: [NSString stringWithFormat:@" %@",placemark.subAdministrativeArea]];
             }
             
             if (placemark.administrativeArea != nil){
                 [address stringByAppendingString: [NSString stringWithFormat:@" %@",placemark.administrativeArea]];
             }
             
             if (placemark.country != nil){
                 [address stringByAppendingString: [NSString stringWithFormat:@" %@",placemark.country]];
             }
             
             toplacemark = placemark;
             
             
             //             address = [NSString stringWithFormat:@"%@, %@  %@ %@, %@ %@  %@", placemark.subThoroughfare,placemark.name, placemark.subLocality,  placemark.locality,placemark.subAdministrativeArea , placemark.administrativeArea , placemark.country];
             completionBlock(address);
             
             
         }
     }];
}


- (void)placeAutocompletewithString:(NSString*)query{
    
    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
    filter.type = kGMSPlacesAutocompleteTypeFilterNoFilter;
    
    [_placesClient autocompleteQuery:query
                              bounds:nil
                              filter:filter
                            callback:^(NSArray *results, NSError *error) {
                                if (error != nil) {
                                    NSLog(@"Autocomplete error %@", [error localizedDescription]);
                                    return;
                                }
                                
                                
                                NSMutableArray *newplacesList = [[NSMutableArray alloc]init];
                                
                               for (GMSAutocompletePrediction* result in results) {
                                    Place *addedPlace = [[Place alloc]init];
                                   addedPlace.placeName = result.attributedFullText.string;
                                   
                                   addedPlace.PlaceID = result.placeID;
                                   
                                   
                                    [newplacesList addObject:addedPlace];
                                }

                                
                                placesList = newplacesList;
                                
//                                dispatch_sync(dispatch_get_main_queue(), ^{
                                
                                    if ([placesList count]>0) {
                                        [autoCompleteView setHidden:NO];
                                    }
                                    else{
                                        [autoCompleteView setHidden:YES];
                                    }
                                    [autoCompleteTableView reloadData];
//                                });
                                
                                
//                                for (GMSAutocompletePrediction* result in results) {
//                                    NSLog(@"Result '%@' with placeID %@", result.attributedFullText.string, result.placeID);
//                                }
                                
                                
                            }];
}


-(void)getPlaceDetailsFromPlaceID:(NSString*)placeId{
    // A hotel in Saigon with an attribution.
   // NSString *placeID = @"ChIJV4k8_9UodTERU5KXbkYpSYs";
    
    [_placesClient lookUpPlaceID:placeId callback:^(GMSPlace *place, NSError *error) {
        if (error != nil) {
            NSLog(@"Place Details error %@", [error localizedDescription]);
            return;
        }
        
        if (place != nil) {
            
            toLocation = [[CLLocation alloc] initWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude];
            [self getAddressToLocation:toLocation complationBlock:^(NSString * address) {
                
                
            }];
//            toAddressTextField.text = place.name;
            NSLog(@"Place name %@", place.name);
            NSLog(@"Place address %@", place.formattedAddress);
            NSLog(@"Place placeID %@", place.placeID);
            NSLog(@"Place attributions %@", place.attributions);
        } else {
            NSLog(@"No place details for %@", placeId);
        }
    }];
    
}
#pragma -mark uitextfield delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Place*currentPlace = [placesList objectAtIndex:indexPath.row];
    toAddressTextField.text = currentPlace.placeName;
    dispatch_async(dispatch_get_main_queue(), ^{
           [toAddressTextField endEditing:YES];
        [toAddressTextField resignFirstResponder];
        //Code that presents or dismisses a view controller here
    });
    

 
     
    [self getPlaceDetailsFromPlaceID:currentPlace.PlaceID];
    [autoCompleteView setHidden:YES];
    //    if (textField== fromAddressTextField) {
    //        [self fromAddressSearchButtonAction:nil];
    //    }
    //    else if (textField== toAddressTextField) {
            [self toAddressSearchButtonAction:nil];
    //    }
    
    [self.view sendSubviewToBack:autoCompleteView];
//    [self.view endEditing:YES];
}
#pragma -mark uitable view datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AutoCompleteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"clePlace"];
    Place*currentPlace = [placesList objectAtIndex:indexPath.row];
    cell.placeNameLabel.text = currentPlace.placeName;
    return  cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [placesList count];
}
#pragma -mark uitableview datasource methods
-(BOOL)textFieldShouldClear:(UITextField *)textField{
    [autoCompleteView setHidden:YES];
    [self.view sendSubviewToBack:autoCompleteView];

    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField ==  toAddressTextField) {
    NSString *newString = textField.text;
    if ([newString length]==0) {
        [autoCompleteView setHidden:YES];
        [self.view sendSubviewToBack:autoCompleteView];
        //        return YES;
    }
    
    @try {
        [autoCompleteView setHidden:NO];
        [self.view bringSubviewToFront:autoCompleteView];
        [self placeAutocompletewithString:newString];
        //        [[ServiceLogic sharedInstance]autoCompleteForPlace:newString Complession:^(id response) {
        //            dispatch_sync(dispatch_get_main_queue(), ^{
        //                placesList = response;
        //                if ([placesList count]>0) {
        //                    [autoCompleteView setHidden:NO];
        //                }
        //                else{
        //                    [autoCompleteView setHidden:YES];
        //                }
        //                [autoCompleteTableView reloadData];
        //            });
        //        }];
    }
    @catch (NSException *exception) {
        
    }
    
    
        // from textfield
      [self toAddressSearchButtonAction: toSearchBTN];
   
    NSLog(@"%@",newString);
    }
        return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
//    if (textField== fromAddressTextField) {
//        [self fromAddressSearchButtonAction:nil];
//    }
//    else if (textField== toAddressTextField) {
//        [self toAddressSearchButtonAction:nil];
//    }
 
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
//    NSString *newString = textField.text;
//    if ([newString length]==0) {
//        [autoCompleteView setHidden:YES];
//        [self.view sendSubviewToBack:autoCompleteView];
////        return YES;
//    }
//    
//    @try {
//        [autoCompleteView setHidden:NO];
//        [self.view bringSubviewToFront:autoCompleteView];
//        [self placeAutocompletewithString:newString];
//        //        [[ServiceLogic sharedInstance]autoCompleteForPlace:newString Complession:^(id response) {
//        //            dispatch_sync(dispatch_get_main_queue(), ^{
//        //                placesList = response;
//        //                if ([placesList count]>0) {
//        //                    [autoCompleteView setHidden:NO];
//        //                }
//        //                else{
//        //                    [autoCompleteView setHidden:YES];
//        //                }
//        //                [autoCompleteTableView reloadData];
//        //            });
//        //        }];
//    }
//    @catch (NSException *exception) {
//        
//    }
//    
//    if (textField.tag == 4) {
//    // from textfield
//        [self fromAddressSearchButtonAction:fromSearchBtm];
//        
//    }else {
//        [self toAddressSearchButtonAction: toSearchBTN];
//    }
//    NSLog(@"%@",newString);
////    return YES;

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField== fromAddressTextField) {
        [self fromAddressSearchButtonAction:nil];
    }
    else if (textField== toAddressTextField) {
        [self toAddressSearchButtonAction:nil];
    }
    return YES;
}
@end
