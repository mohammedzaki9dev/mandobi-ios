//
//  LoginViewController.h
//  Mandobi
//
//  Created by Assem Imam on 2/3/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
@interface LoginViewController :MasterViewController
{
    __weak IBOutlet UIButton *loginBtn;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UITextField *passwordTextField;
    __weak IBOutlet UITextField *userNameTextField;
    __weak IBOutlet UIView *containerView;
}
@property(nonatomic)UserType currentUserType;
- (IBAction)loginButtonAction:(UIButton *)sender;
- (IBAction)registerButtonAction:(UIButton *)sender;
- (IBAction)forgetPasswordButtonAction:(UIButton *)sender;

@end
