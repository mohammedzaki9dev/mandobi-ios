//
//  BudgetCalculatorViewController.m
//  Mandobi
//
//  Created by Assem Imam on 2/8/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "BudgetCalculatorViewController.h"
#import "ProfitPickerView.h"

@interface BudgetCalculatorViewController ()
{
    ProfitPickerView *vProfitPicker;
}
@end

@implementation BudgetCalculatorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    @try {
        [CommonMethods FormatView:containerView WithShadowRadius:5 CornerRadius:10 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];
        

        for (UIView*view in containerView.subviews) {
            if (view.tag == -1) {
                view.layer.borderWidth = 1.5;
                view.layer.borderColor = [[UIColor colorWithRed:224/255.0f green:224/255.0f blue:224/255.0f alpha:1]CGColor];
                
            }
        }
        for (UIView*view in detailsView.subviews) {
            if (view.tag == -1) {
                view.layer.borderWidth = 1.5;
                view.layer.borderColor = [[UIColor colorWithRed:224/255.0f green:224/255.0f blue:224/255.0f alpha:1]CGColor];

            }
        }
        vProfitPicker = [[[NSBundle mainBundle]loadNibNamed:@"ProfitPickerView" owner:nil options:nil]objectAtIndex:0];
        vProfitPicker.center = self.view.center;
        [vProfitPicker setFrame:CGRectMake(vProfitPicker.frame.origin.x, self.view.frame.size.height, vProfitPicker.frame.size.width, vProfitPicker.frame.size.height)];
        
        [vProfitPicker.cancelButton addTarget:self action:@selector(cancelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
         [vProfitPicker.okButton addTarget:self action:@selector(okButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:vProfitPicker];
          [CommonMethods FormatView:vProfitPicker WithShadowRadius:5 CornerRadius:10 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];
        
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)getProfitData
{
    @try {
        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance] getProfitForUser:CurrentLogedUserInfo ForDate:vProfitPicker.datePickerView.date Complession:^(id response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        
                        totalOrdersLabel.text = [NSNull null]!= [response objectForKey:@"count"] && [response objectForKey:@"count"]?[NSString stringWithFormat:@"%i",[[response objectForKey:@"count"]intValue]]:@"";
                        totalPriceLabel.text = [NSNull null]!= [response objectForKey:@"profits"] && [response objectForKey:@"profits"]?[NSString stringWithFormat:@"%i",[[response objectForKey:@"profits"]intValue]]:@"";
                    }
                    else{
                        SHOW_NO_DATA_MESSAGE
                    }
                });
            }];
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }

    }
    @catch (NSException *exception) {
        
    }
}
-(IBAction)okButtonAction:(UIButton*)sender
{
    @try {
        [UIView animateWithDuration:0.4 animations:^{
            [vProfitPicker setFrame:CGRectMake(vProfitPicker.frame.origin.x, self.view.frame.size.height  , vProfitPicker.frame.size.width, vProfitPicker.frame.size.height)];
            HIDE_MODAL_VIEW
        }];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
        dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        orderDateLabel.text =    [dateFormatter stringFromDate:vProfitPicker.datePickerView.date];
        
        [self performSelector:@selector(getProfitData) withObject:nil afterDelay:0.4];
    }
    @catch (NSException *exception) {
        
    }
}

-(IBAction)cancelButtonAction:(UIButton*)sender
{
    @try {
        [UIView animateWithDuration:0.4 animations:^{
            [vProfitPicker setFrame:CGRectMake(vProfitPicker.frame.origin.x, self.view.frame.size.height  , vProfitPicker.frame.size.width, vProfitPicker.frame.size.height)];
            HIDE_MODAL_VIEW
        }];
 
    }
    @catch (NSException *exception) {
        
    }
}
- (IBAction)orderDateButtonAction:(UIButton *)sender {
    @try {
    SHOW_MODAL_VIEW
        [self.view bringSubviewToFront:vProfitPicker];
       [UIView animateWithDuration:0.4 animations:^{
             [vProfitPicker setFrame:CGRectMake(vProfitPicker.frame.origin.x, self.view.frame.size.height -  vProfitPicker.frame.size.height , vProfitPicker.frame.size.width, vProfitPicker.frame.size.height)];
           
       }];

    }
    @catch (NSException *exception) {
        
    }
    
}
@end
