//
//  WelcomeViewController.m
//  Mandobi
//
//  Created by Assem Imam on 2/3/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "WelcomeViewController.h"
#import "LoginViewController.h"
@interface WelcomeViewController ()

@end

@implementation WelcomeViewController
-(void)formatViews
{
    @try {
        for (UIView*view in containerView.subviews) {
            if (view.tag == -1) {
                [CommonMethods FormatView:view WithShadowRadius:2 CornerRadius:0 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];
            }
        }
    }
    @catch (NSException *exception) {
        
    }
}
- (void)viewDidLoad {
    self.comeFromMenu =YES;
    [super viewDidLoad];
    @try {
        [self formatViews];
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    @try {
        LoginViewController*vcLogin = (LoginViewController*)segue.destinationViewController;
        vcLogin.comeFromMenu = NO;
        if ([segue.identifier isEqualToString:@"client_login"]) {
            vcLogin.currentUserType =  CLIENT;
        }
        else if ([segue.identifier isEqualToString:@"delegate_login"]) {
            vcLogin.currentUserType =  WORKER;

        }
    }
    @catch (NSException *exception) {
        
    }
}


@end
