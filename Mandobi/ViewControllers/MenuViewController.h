//
//  MenuViewController.h
//  ChauffeurTaxi
//
//  Created by Assem Imam on 12/30/15.
//  Copyright © 2015 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet NSLayoutConstraint *userViewHeightConstraint;
    __weak IBOutlet UITableView *menuItemsTableView;
    
    __weak IBOutlet UIView *userInfoView;
    __weak IBOutlet UILabel *userFullNamelabel;
    __weak IBOutlet UIImageView *userImageView;
}
@end
