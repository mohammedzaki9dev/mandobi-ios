//
//  RequestCancelingViewController.m
//  ChauffeurTaxi
//
//  Created by Assem Imam on 12/30/15.
//  Copyright © 2015 Assem Imam. All rights reserved.
//

#import "RequestCancelingViewController.h"
#import "UserProfileViewController.h"
#import "CancelCell.h"

#define NO_OPENION_OPTION 1
#define NO_WANT_TAXI_OPTION 2
#define TAXI_COME_LATE_OPTION 3
#define TEXT_APP_OPTION 4
#define AVERAGE_PRICE_OPTION 5
#define DISSLIKE_DRIVER_OPTION 6

@interface RequestCancelingViewController ()
{
    int cancelReasonID;
    NSMutableArray *cancellingResonsList;
}
@end

@implementation RequestCancelingViewController
@synthesize currentOrder;
-(void)formateViews
{
    @try {
      [CommonMethods FormatView:allContainerView WithShadowRadius:2 CornerRadius:15 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];
        
        for (UIView*view in optionsContainerView.subviews) {
            if (view.tag == -1) {
                for (UIView*button in view.subviews) {
                    [self formatButtonViews:(UIButton*)button];
                }
            }
        }
    }
    @catch (NSException *exception) {
        
    }
}
-(void)getCancellingReasons
{
    @try {
        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance]getCancellingReasonsWithComplession:^(id response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    [allContainerView setHidden:NO];
                    
                    if (response) {
                        for (id reason in response) {
                            NSMutableDictionary*reasonDic = [[NSMutableDictionary alloc]init];
                            [reasonDic setObject:[reason objectForKey:@"massage_id"] forKey:@"massage_id"];
                            [reasonDic setObject:[reason objectForKey:@"msg_txt"] forKey:@"msg_txt"];
                            [reasonDic setObject:[reason objectForKey:@"msg_en"] forKey:@"msg_en"];
                            [reasonDic setObject:@"0" forKey:@"selected"];
                            [cancellingResonsList addObject:reasonDic];
                        }
                        [cancelReasonsTableView reloadData];
                    }
                    else{
                        [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"no_data_found", nil)  image:nil type:TSMessageNotificationTypeWarning duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                       
                    }
                });
            }];
            
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }

    }
    @catch (NSException *exception) {
        
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    @try {
        cancellingResonsList = [[NSMutableArray alloc]init];
        cancelReasonID = -1;
        [self formateViews];
        [allContainerView setHidden:YES];
        
        [self performSelector:@selector(getCancellingReasons) withObject:nil afterDelay:0.2];
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)desellectRadioButtons
{
    @try {
        for (id reason in cancellingResonsList) {
            [reason setObject:@"0" forKey:@"selected"];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark IBActions
- (IBAction)radioButtonButtonAction:(UIButton *)sender {
    @try {
        [self desellectRadioButtons];
        [sender setImage:[UIImage imageNamed:@"radio_select"] forState:UIControlStateNormal];
    
        id reason = [cancellingResonsList objectAtIndex:sender.tag];
        [[cancellingResonsList objectAtIndex:sender.tag] setObject:@"1" forKey:@"selected"];
        cancelReasonID = [[reason objectForKey:@"massage_id"]intValue];
        [cancelReasonsTableView reloadData];
    }
    @catch (NSException *exception) {
        
    }
}
- (IBAction)sendButtonAction:(UIButton *)sender {
    @try {
        if (cancelReasonID==-1) {
            return;
        }
        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance]cancelUserRequest:CurrentLogedUserInfo ForOrder:currentOrder WithCancelReasonID:cancelReasonID Complession:^(id response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        if ([response objectForKey:@"message"]) {
                            [TSMessage showNotificationInViewController:self title:nil subtitle:[response objectForKey:@"message"]  image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                        }
                        else {
                            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"accept_client_request_success", nil)  image:nil type:TSMessageNotificationTypeSuccess duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                            UserProfileViewController*vcProfile = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
                            vcProfile.comeFromMenu = YES;
                            [self.navigationController pushViewController:vcProfile animated:YES];
                        }
                    }
                    else{
                        
                    }
                });
            }];
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark uitableview datasource 
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [cancellingResonsList count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CancelCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cleCancel"];
    cell.cancelOptionButton.tag = indexPath.row;
    id reason = [cancellingResonsList objectAtIndex:indexPath.row];
    if ([[reason objectForKey:@"selected"]intValue]==0) {
        [cell.cancelOptionButton setImage:[UIImage imageNamed:@"radio_unselect"] forState:UIControlStateNormal];
    }
    else{
       [cell.cancelOptionButton setImage:[UIImage imageNamed:@"radio_select"] forState:UIControlStateNormal];
    }
    if ([[BundleLocalization sharedInstance].language isEqualToString:ARABIC_LANG]) {
       cell.cancelReasonLabel.text =[NSNull null]!= [reason objectForKey:@"msg_txt"]&&[reason objectForKey:@"msg_txt"] ?[reason objectForKey:@"msg_txt"]:@"";
    }
    else{
      cell.cancelReasonLabel.text =[NSNull null]!= [reason objectForKey:@"msg_txt"]&&[reason objectForKey:@"msg_en"] ?[reason objectForKey:@"msg_en"]:@"";
    }

    
    return  cell;
}
@end
