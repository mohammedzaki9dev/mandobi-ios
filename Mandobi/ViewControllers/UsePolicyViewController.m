//
//  UsePolicyViewController.m
//  Mandobi
//
//  Created by Assem Imam on 3/12/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "UsePolicyViewController.h"

@interface UsePolicyViewController ()

@end

@implementation UsePolicyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    @try {
        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance]getUsePolicyWithComplession:^(id response) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        if ([[BundleLocalization sharedInstance].language isEqualToString:ARABIC_LANG]) {
                            detailTextView.text = [NSNull null]!= [response objectForKey:@"detail_text"]?[response objectForKey:@"detail_text"]:@"";
                        }
                        else{
                             detailTextView.text = [NSNull null]!= [response objectForKey:@"detail_text_en"]?[response objectForKey:@"detail_text_en"]:@"";
                        }
                    }
                    else{
                        SHOW_NO_DATA_MESSAGE
                    }
                });
            }];
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
