//
//  MandoobMapFollowViewController.h
//  Mandobi
//
//  Created by Assem Imam on 3/23/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface MandoobMapFollowViewController : MasterViewController
{
    __weak IBOutlet UIButton *doneButton;
    __weak IBOutlet UIView *mapViewContainer;
}
@property(nonatomic,retain)Order*currentOrder;
    @property (nonatomic) OrderType  CurrentOrderType;

- (IBAction)doneButtonAction:(UIButton *)sender;
@end
