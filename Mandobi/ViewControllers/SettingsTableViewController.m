//
//  SettingsTableViewController.m
//  Mandobi
//
//  Created by Bassem on 9/8/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//
#import "MasterViewController.h"
#import "SettingsTableViewController.h"
#import "SettingsTableViewCell.h"

@interface SettingsTableViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UIButton *logoutBtn;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *languageSegment;
@property (weak, nonatomic) IBOutlet UIButton *editProfileBtn;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;

@end

@implementation SettingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

//    self.tabBarController?.tabBar.isHidden = true;
//    self.tabBarController?.tabBar.isOpaque = true;
//   [self.tabBarController.tabBar setHidden:YES];
//    [self.tabBarController.tabBar setOpaque:YES];
    
    [self.view layoutSubviews];
//    self.edgesForExtendedLayout = UIRectEdgeAll;
    
    
//    NSString *fontName = ARABIC_FONT;
//    
//    
//    //Arabic Font :GESSTwoMedium-Medium
//    //English Font :SFUIDisplay-Regular
//    
//    if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
//        
//        
//        fontName = ENGLISH_FONT;
//    }
//    UIFont *font = [UIFont fontWithName:fontName size:17.0f];
//    
//
//
//    
//    UILabel* lbNavTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,40,320,40)];
//    lbNavTitle.textAlignment = UITextAlignmentLeft;
//    lbNavTitle.text = NSLocalizedString(@"SETTINGS",@"");
//    lbNavTitle.textColor = [UIColor blackColor];
//    lbNavTitle.font = font;
//    self.navigationItem.titleView = lbNavTitle;
    
    self.userImageView.layer.cornerRadius = 45;
    self.userImageView.layer.borderWidth = 2;
    self.userImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.userImageView.layer.masksToBounds = YES;
    self.userImageView.clipsToBounds= YES;
    
    if (CurrentLogedUserInfo.photo != nil) {
        
    
    [self.userImageView setImage:  CurrentLogedUserInfo.photo];
    }
    
    if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
        //                storyBoardName=@"Main_en";
        self.languageSegment.selectedSegmentIndex = 0;
    }else{
        self.languageSegment.selectedSegmentIndex = 1;
    }

}
- (IBAction)backtoHome:(id)sender {
//      [self.tabBarController setSelectedIndex:0];
//      [self.tabBarController.tabBar setHidden:NO];
//    [self.tabBarController.tabBar setOpaque:NO];
    
    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleDefault ];
    
}
-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    self.nameLabel.text = CurrentLogedUserInfo.fullName;
    self.emailLabel.text = CurrentLogedUserInfo.email;
    self.mobileLabel.text = CurrentLogedUserInfo.mobile;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didChaneSegmentLanguage:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        //English
         [[BundleLocalization sharedInstance]setLanguage:ENGLISH_LANG];
        [[NSUserDefaults standardUserDefaults] setObject:ENGLISH_LANG forKey:@"lang"];
        [[NSUserDefaults standardUserDefaults] synchronize];
       
    }else {
        [[BundleLocalization sharedInstance] setLanguage:ARABIC_LANG];
        [[NSUserDefaults standardUserDefaults] setObject:ARABIC_LANG forKey:@"lang"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        

        
    }
    
    
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSString*storyBoardName=@"Main_ar";
    
    if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
        storyBoardName=@"Main_en";
    }
    
    
    
    [APP_DELEGATE instantiateInitialViewControllerWithstoryboard:storyBoardName];
    
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    
    return UIStatusBarStyleLightContent;
}

//#pragma mark - Table view data source
//
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    return 1;
//    
//}
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return 4;
//    
//}
//
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//
//{
//    
//    NSString *cellIdentfire = @"cell";
//    
//    if (indexPath.row == 1 || indexPath.row == 2) {
//        cellIdentfire = @"cell3";
//    }
//  else  if (indexPath.row == 2) {
//        cellIdentfire = @"cell3";
//    }
//    SettingsTableViewCell *cell = (SettingsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentfire forIndexPath:indexPath];
//    
//    
//    if (indexPath.row == 0) {
//        
//        //Email Cell
//        
//        cell.TitleLabel.text = NSLocalizedString(@"email", nil);
//        cell.ValueLabel.text = CurrentLogedUserInfo.email;
//        
//    }else if (indexPath.row == 1){
//        cell.TitleLabel.text = NSLocalizedString(@"EDIT_PROFILE", nil);
//        cell.ValueLabel.text = @"";
//
//        
//            }else if (indexPath.row == 2){
//                //Password Cell
//                
//                cell.TitleLabel.text = NSLocalizedString(@"change_password", nil);
//                cell.ValueLabel.text = @"";
//                
//
//    }else if (indexPath.row == 3){
//        //Language Cell
//        cell.TitleLabel.text = NSLocalizedString(@"language", nil);
//        cell.ValueLabel.text = NSLocalizedString([[BundleLocalization sharedInstance] language], nil);
//    }
//
//    
//    
//    
//    
//    return cell;
//    
//    
//    
//}
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    if (indexPath.row == 1) {
//        //change Password
//        
//         [self performSegueWithIdentifier:@"editProfile" sender:self];
//        
//    }else if (indexPath.row == 2){
//        //change language
//        [self performSegueWithIdentifier:@"changePassword" sender:self];
//    }else if (indexPath.row == 3){
//        //change language
//        [self performSegueWithIdentifier:@"changeLang" sender:self];
//    }
//}
//
//-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    
//    return  [[UIView alloc] initWithFrame:CGRectZero];
//    
//}
-(IBAction)performLogOut{
    @try {
        if (internetConnection) {
            [[SlideNavigationController sharedInstance] righttMenuSelected:nil];
            
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance] logOutForUser:CurrentLogedUserInfo Complession:^(id response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"loged_user_type"];
                        [CurrentLogedUserInfo deleteFromUserDefaults];
                        [[NSUserDefaults standardUserDefaults]synchronize];
                      
                        CurrentUserType = PUBLIC_USER;
                        CurrentLogedUserInfo = nil;
                        
                        
//                          applicationStroryBoard  = [UIStoryboard storyboardWithName:@"Auth_ar" bundle:nil];
                        
                              [APP_DELEGATE instantiateInitialViewControllerWithstoryboard:@"Auth_ar"];
                        
//                        UIViewController *vc = [applicationStroryBoard instantiateInitialViewController];
//                        
//                        [APP_DELEGATE.window setRootViewController:vc];
//                        [APP_DELEGATE.window makeKeyAndVisible ];
                       
                        
                        
                    }
                });
                
            }];
            
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
    }
    @catch (NSException *exception) {
        
    }
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
