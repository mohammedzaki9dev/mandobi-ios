//
//  MandoobResponsesViewController.m
//  Mandobi
//
//  Created by Assem Imam on 3/19/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MandoobResponsesViewController.h"
#import "WorkerOrderCell.h"
#import "OrderDetailsViewController.h"

@interface MandoobResponsesViewController ()
{
    NSArray *ordersList;
    Order*selectedOrder;
}
@end

@implementation MandoobResponsesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    @try {
        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance]getMandoobResponsesForUser:(User*)CurrentLogedUserInfo Complession:^(id response){
                dispatch_sync(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        ordersList = response;
                        [ordersTableView reloadData];
                    }
                    else{
                        SHOW_NO_DATA_MESSAGE
                    }
                });
            }];
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     @try {
         
            ((OrderDetailsViewController*)segue.destinationViewController).currentOrder = selectedOrder;
            ((OrderDetailsViewController*)segue.destinationViewController).isComeFromMandoobResponse = YES;
     }
     @catch (NSException *exception) {
     
     }
 }

#pragma -mark uitableview datasource methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ordersList count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WorkerOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cleOrder"];
    @try {
        
        Order *currentOrder = (Order*)[ordersList objectAtIndex:indexPath.row];
        cell.orderTitleLabel.text = currentOrder.message;
        cell.orderTimeLabel.text = currentOrder.orderDate;
        cell.orderStatusImageView.hidden = YES;
        
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
#pragma -mark uitableview delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        selectedOrder  =(Order*) [ordersList objectAtIndex:indexPath.row];
        ((Order*) [ordersList objectAtIndex:indexPath.row]).isViewed=YES;
        [self performSegueWithIdentifier:@"orderDetails" sender:nil];
        [tableView reloadData];
    }
    @catch (NSException *exception) {
        
    }
}

@end
