//
//  ForgetPasswordViewController.h
//  Mandobi
//
//  Created by Assem Imam on 6/1/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface ChangePasswordViewController : MasterViewController
{
    
    __weak IBOutlet UIButton *sendButton;
    __weak IBOutlet UITextField *passwordTextField;
}
@property(nonatomic)UserType currentUserType;
- (IBAction)sendButtonAction:(UIButton *)sender;
@end
