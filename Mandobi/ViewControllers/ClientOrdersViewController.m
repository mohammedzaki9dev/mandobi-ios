//
//  ClientOrdersViewController.m
//  Mandobi
//
//  Created by Assem Imam on 3/12/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MandoobMapFollowViewController.h"
#import "ClientOrdersViewController.h"
#import "OrderDetailCell.h"
#import "OrderHeaderCell.h"
#import "LMGeocoder.h"
#import "NoOrdersView.h"
#import "OrderDetailsViewController.h"
@interface ClientOrdersViewController ()
{
    __weak IBOutlet UISegmentedControl *orderTypeSegment;
    NSArray*ordersList;
              
}
@end

@implementation ClientOrdersViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
   
//    
//    
//    NSString *fontName = ARABIC_FONT;
//    
//    
//    //Arabic Font :GESSTwoMedium-Medium
//    //English Font :SFUIDisplay-Regular
//    
//    if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
//        
//        
//        fontName = ENGLISH_FONT;
//    }
//    UIFont *font = [UIFont fontWithName:fontName size:17.0f];
//    
//    UILabel* lbNavTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,40,320,40)];
//    lbNavTitle.textAlignment = UITextAlignmentLeft;
//    lbNavTitle.text = NSLocalizedString(@"MYORDER",@"");
//    lbNavTitle.textColor = [UIColor blackColor];
//    lbNavTitle.font = font;
//    self.navigationItem.titleView = lbNavTitle;
//
    
    
     // [self.ordersTableView registerClass:[OrderDetailCell self] forCellReuseIdentifier:@"cleOrderDetail"];
    
 
    

     self.orderType = (OrderType*)OPEN;
    
//    [self loadData];
    
    
   
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [self.tabBarController.tabBar setHidden:NO];
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    
    return UIStatusBarStyleDefault;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
     [self loadData];
}

#pragma mark - Segment Action

- (IBAction)segmentDidChange:(id)sender {
    
    switch (orderTypeSegment.selectedSegmentIndex) {
        case 0:
            if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
               
                self.orderType = (OrderType*)OPEN;
            }else {
                self.self.orderType = (OrderType*)CANCELED;
            }
            
                        break;
            
        case 1:
            self.orderType = (OrderType*)EXECUTED;
            break;
            
        case 2:
            
            if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
                
                 self.self.orderType = (OrderType*)CANCELED;
            }else {
                 self.orderType = (OrderType*)OPEN;
            }
            
           
            break;
        default:self.orderType = (OrderType*)OPEN;
            
            break;
    }
    
    [self loadData];
}



#pragma mark - LoadData
-(void)loadData{
    
    @try {
        if (internetConnection) {
            SHOW_FULL_LOADING
            
            
            [[ServiceLogic sharedInstance]getOrdersForUser:CurrentLogedUserInfo WithOrderType:self.orderType Complession:^(id response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                       self.ordersTableView.delegate = self;
                        ordersList = nil;
                        
                        ordersList = [[NSArray alloc] initWithArray: (NSArray*)response ];
                        NSLog(@"%lu",(unsigned long)ordersList.count);
                        [self.ordersTableView reloadData];
                    }
                    else{
                        SHOW_NO_DATA_MESSAGE
                       self.ordersTableView.delegate = self;
                        ordersList = nil;
                        NSLog(@"%lu",(unsigned long)ordersList.count);
                        [self.ordersTableView reloadData];

                    }
                });
            }];
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
    }
    @catch (NSException *exception) {
        
    }
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSInteger numOfRows = [ordersList count];
    if (numOfRows < 1)
    {

    }
    else{
       self.ordersTableView.backgroundView = nil;
    }
    return  numOfRows;
    
    }

- (NSInteger)tableView:(UITableView *)tableView numberOfSubRowsAtIndexPath:(NSIndexPath *)indexPath
{
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{


    static NSString *CellIdentifier = @"cleOrderDetail";
    
    
    OrderDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    @try {
//        [CommonMethods FormatView:cell WithShadowRadius:2 CornerRadius:0 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];
       
        Order*currentOrder = [ordersList objectAtIndex:indexPath.row];

        cell.fromLabel.text =  currentOrder.fromAddress ;
        cell.toLabel.text = currentOrder.toAddress;
        cell.timeLabel.text = currentOrder.orderDate;
        cell.feesLabel.text = [NSString stringWithFormat:@"%.f",currentOrder.price];
        cell.vehicleTypeLabel.text = currentOrder.worker.licenceNumber;
        cell.driverName.text = currentOrder.driverName;
        cell.driverStars.rating  = currentOrder.driverRate;
        if (internetConnection) {
           // SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance]getDriverRateForDriverId:[NSNumber numberWithInt:currentOrder.worker.userID] Complession:^(id response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                 //   HIDE_FULL_LOADING
                    if (response) {
                        if ([response count]>0) {
                            if ([[[response lastObject] objectForKey:@"AVG(rate)"] isEqual:[NSNull null]]) {
                                cell.driverStars.rating = 0.0;// .text = @"0";
                            }else {
                              cell.driverStars.rating =  [[[response lastObject] objectForKey:@"AVG(rate)"]floatValue];
                            }
                        }
                        else{
                            
                        }
                        
                    }
                    else{
                        //SHOW_NO_DATA_MESSAGE
                    }
                });
            }];
            
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
        
        NSURL *imageUrl =[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGES_BASE_URL,currentOrder.worker.photoFile]];
        if (imageUrl) {
            [cell.driverImage setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"user-2"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }

        
    }
    @catch (NSException *exception) {
        
    }
    
    return cell;
    
}

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForSubRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"cleOrderDetail";
//    
//    OrderDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    @try {
//        [CommonMethods FormatView:cell WithShadowRadius:2 CornerRadius:0 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];
//        cell.backgroundView.backgroundColor = [UIColor whiteColor];
//        Order*currentOrder = [ordersList objectAtIndex:indexPath.row];
//        [[LMGeocoder sharedInstance] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(currentOrder.toLocation.latitude, currentOrder.toLocation.longitude)
//                                                      service:kLMGeocoderGoogleService
//                                            completionHandler:^(NSArray *results, NSError *error) {
//                                                if (results.count && !error) {
//                                                    LMAddress *address = [results firstObject];
//                                                    cell.toLabel.text =[address.formattedAddress stringByAppendingString:@"\n\n\n"];
//                                                    
//                                                    
//                                                    
//                                                }
//                                            }];
//        [[ServiceLogic sharedInstance]reverseGeocodeWithLatitude:currentOrder.fromLocation.latitude Longitude:currentOrder.fromLocation.longitude APIKEY:@"AIzaSyDOzBuFo5ZxCjlW-4k7NaxkOE-1YdD64_o" Complession:^(id response) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                if (response) {
//                    cell.fromLabel.text = [response stringByAppendingString:@"\n\n\n \n\n\n"];
//                }
//            });
//        }];
//        
//        
//        
//        switch (currentOrder.worker.vehicleType) {
//            case SMALL_VEHICLE:
//            {
//                cell.vehicleTypeLabel.text= NSLocalizedString(@"small_vehicle", nil);
//                cell.vehicleTypeImageView.image = [UIImage imageNamed:@"vehicle-1"];
//            }
//                break;
//                
//            case MEDIUM_VEHICLE:
//            {
//                cell.vehicleTypeLabel.text= NSLocalizedString(@"medium_vehicle", nil);
//                cell.vehicleTypeImageView.image = [UIImage imageNamed:@"vehicle-2"];
//            }
//                break;
//            case LARGE_VEHICLE:
//            {
//                cell.vehicleTypeLabel.text= NSLocalizedString(@"large_vehicle", nil);
//                cell.vehicleTypeImageView.image = [UIImage imageNamed:@"vehicle-3"];
//            }
//                break;
//        }
//
//    }
//    @catch (NSException *exception) {
//        
//    }
//
//    return cell;
//}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return  135 ;//70.0f;
//}
//
//-(CGFloat)tableView:(UITableView *)tableView heightForSubRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 180;
//}



#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    @try {
        if ([segue.identifier isEqualToString:@"follow"]) {
            
        
        if (self.orderType == (OrderType*)OPEN) {
           
            
            ((MandoobMapFollowViewController *) segue.destinationViewController).currentOrder = _selectedOrder;
            ((MandoobMapFollowViewController*)segue.destinationViewController).CurrentOrderType = self.orderType;
//            ((OrderDetailsViewController*)segue.destinationViewController).currentOrder = selectedOrder;
            
        }
            
        }else
        
           {
            
            ((OrderDetailsViewController*)segue.destinationViewController).CurrentOrderType = self.orderType;
            ((OrderDetailsViewController*)segue.destinationViewController).currentOrder = _selectedOrder;

            
            
        }
        
        
        
            }
    @catch (NSException *exception) {
        
    }
    
   
    
}



#pragma -mark uitableview delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        _selectedOrder  =(Order*) [ordersList objectAtIndex:indexPath.row];
        ((Order*) [ordersList objectAtIndex:indexPath.row]).isViewed=YES;
        
        if (self.orderType == (OrderType*)OPEN) {
            [self performSegueWithIdentifier:@"follow" sender:nil];
            
                   }else   {
                       [self performSegueWithIdentifier:@"orderDetails" sender:nil];
            
            
            
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [[UIView alloc] initWithFrame:CGRectZero];
 
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section

{
    
    return [[UIView alloc] initWithFrame:CGRectZero];
}


@end
