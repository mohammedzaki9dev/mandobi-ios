//
//  WorkerOrdersViewController.h
//  Mandobi
//
//  Created by Assem Imam on 2/3/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface WorkerOrdersViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UITableView *ordersTableView;
}
@end
