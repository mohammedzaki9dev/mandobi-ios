//
//  FollowListViewController.m
//  Mandobi
//
//  Created by Assem Imam on 3/27/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "FollowListViewController.h"
#import "FollowCell.h"
#import "LMGeocoder.h"
#import "MandoobMapFollowViewController.h"

@interface FollowListViewController ()
{
    NSArray*ordersList;
    Order*selectedOrder;
}
@end

@implementation FollowListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    @try {
        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance]getFollowesForUser:CurrentLogedUserInfo Complession:^(id response) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     HIDE_FULL_LOADING
                     if (response) {
                         ordersList = response;
                         [followersTableView reloadData];
                     }
                     else{
                         SHOW_NO_DATA_MESSAGE
                     }
                 });
            }];
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    @try {
        if (selectedOrder) {
            ((MandoobMapFollowViewController*)segue.destinationViewController).currentOrder = selectedOrder;
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

#pragma -mark uitableview datasource methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ordersList count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FollowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cleFollow"];
    @try {
        Order *currentOrder = (Order*)[ordersList objectAtIndex:indexPath.row];
        cell.orderMessageLabel.text = currentOrder.message;
        cell.followButton.tag = indexPath.row;

        [self formatButtonViews:cell.followButton];
        [[ServiceLogic sharedInstance]reverseGeocodeWithLatitude:currentOrder.toLocation.coordinate.latitude Longitude:currentOrder.toLocation.coordinate.longitude APIKEY:@"AIzaSyDOzBuFo5ZxCjlW-4k7NaxkOE-1YdD64_o" Complession:^(id response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (response) {
                    cell.toAddressLabel.text = [response stringByAppendingString:@"\n\n\n \n\n\n"];
                }
            });
        }];
        

        [[ServiceLogic sharedInstance]reverseGeocodeWithLatitude:currentOrder.fromLocation.coordinate.latitude Longitude:currentOrder.fromLocation.coordinate.longitude APIKEY:@"AIzaSyDOzBuFo5ZxCjlW-4k7NaxkOE-1YdD64_o" Complession:^(id response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (response) {
                    cell.fromAddresslabel.text = [response stringByAppendingString:@"\n\n\n \n\n\n"];
                }
            });
        }];
        
        switch (currentOrder.worker.vehicleType) {
            case SMALL_VEHICLE:
            {
                cell.vehicleTypeLabel.text= NSLocalizedString(@"small_vehicle", nil);
                cell.vehicleTypeImageView.image = [UIImage imageNamed:@"vehicle-1"];
            }
                break;
                
            case MEDIUM_VEHICLE:
            {
                cell.vehicleTypeLabel.text= NSLocalizedString(@"medium_vehicle", nil);
                cell.vehicleTypeImageView.image = [UIImage imageNamed:@"vehicle-2"];
            }
                break;
            case LARGE_VEHICLE:
            {
                cell.vehicleTypeLabel.text= NSLocalizedString(@"large_vehicle", nil);
                cell.vehicleTypeImageView.image = [UIImage imageNamed:@"vehicle-3"];
            }
                break;
        }

    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
#pragma -mark uitableview delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        selectedOrder  =(Order*) [ordersList objectAtIndex:indexPath.row];
       
    }
    @catch (NSException *exception) {
        
    }
}

#pragma -mark IBActions
- (IBAction)followButtonAction:(UIButton *)sender {
    @try {
        selectedOrder  =(Order*) [ordersList objectAtIndex:sender.tag];

        [self performSegueWithIdentifier:@"follow" sender:sender];
    }
    @catch (NSException *exception) {
        
    }
}
@end
