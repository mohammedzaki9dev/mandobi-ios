//
//  MasterViewController.m
//  Amana
//
//  Created by Assem Imam on 11/27/13.
//  Copyright (c) 2013 assemimam. All rights reserved.
//

#import "MasterViewController.h"
#import "MenuViewController.h"
#import "MandoobArrivalViewController.h"
#import "MandoobMapFollowViewController.h"
#import "OrderDetailsViewController.h"
#import "RTLController.h"
#import "PublicNotificationsViewController.h"

@interface MasterViewController ()
{
    
}
@end

@implementation MasterViewController
@synthesize headerView;
@synthesize comeFromMenu;
@synthesize isComeFromNotification;
#pragma -mark Worker remote notification method
-(void)didReceiveWorkerRemoteNotification{
    @try {
        if (remoteNotificationInfo) {
            if (remoteNotificationInfo[@"aps"][@"type"]) {
                if ([remoteNotificationInfo[@"aps"][@"type"]intValue] == 1) {
                    // handle notification comming from accept order from mandoob
                    OrderDetailsViewController*vcClientOrderRequest = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"OrderDetailsViewController"];
                    Order*receivedorder = [[Order alloc]init];
                    receivedorder.orderID =[remoteNotificationInfo[@"aps"][@"msg_id"]intValue];
                    receivedorder.orderType = RECEIVE;
                    Worker*orderWorker =[[Worker alloc]init];
                    orderWorker.userType = WORKER;
                    receivedorder.worker = orderWorker;
                    orderWorker.userID = [remoteNotificationInfo[@"aps"][@"member_id"]intValue];
                    vcClientOrderRequest.currentOrder = receivedorder;
                    vcClientOrderRequest.isComeFromMandoobResponse=YES;
                    vcClientOrderRequest.isComeFromNotification = YES;
                    [self.navigationController pushViewController:vcClientOrderRequest animated:NO];
                }
//                else if ([remoteNotificationInfo[@"aps"][@"type"]intValue] == 1) {
//                    // handle notification comming from confirm order from mandoob
//                    MandoobMapFollowViewController *vcFollow = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"MandoobMapFollowViewController"];
//                    Order*receivedorder = [[Order alloc]init];
//                    receivedorder.orderID =[remoteNotificationInfo[@"aps"][@"msg_id"]intValue];
//                    receivedorder.orderType = RECEIVE;
//                    Worker*orderWorker =[[Worker alloc]init];
//                    orderWorker.userType = WORKER;
//                    receivedorder.worker = orderWorker;
//                    vcFollow.isComeFromNotification = YES;
//                    [self.navigationController pushViewController:vcFollow animated:YES];
//                }
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark Client remote notification method
-(void)didReceiveClientRemoteNotification{
    @try {
        if (remoteNotificationInfo) {
            if (remoteNotificationInfo[@"aps"][@"type"]) {
                if ([remoteNotificationInfo[@"aps"][@"type"]intValue] == 2) {
                    //arrival
                    MandoobArrivalViewController*vcArrival = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"MandoobArrivalViewController"];
                    Order*receivedorder = [[Order alloc]init];
                    receivedorder.orderID =[remoteNotificationInfo[@"aps"][@"msg_id"]intValue];
                    receivedorder.orderType = RECEIVE;
                    receivedorder.receiverID = [remoteNotificationInfo[@"aps"][@"member_id"]intValue];
                    Worker*orderWorker =[[Worker alloc]init];
                    orderWorker.userType = WORKER;
                    receivedorder.worker = orderWorker;
                    vcArrival.currentOrder = receivedorder;
                    vcArrival.isComeFromNotification = YES;
                    [self.navigationController pushViewController:vcArrival animated:YES];
                }
                else if ([remoteNotificationInfo[@"aps"][@"type"]intValue] == 0){
                    //handle notification comming from client new order
                    OrderDetailsViewController*vcClientOrderRequest = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"OrderDetailsViewController"];
                    Order*receivedorder = [[Order alloc]init];
                    receivedorder.orderID =[remoteNotificationInfo[@"aps"][@"msg_id"]intValue];
                    receivedorder.senderID = [remoteNotificationInfo[@"aps"][@"member_id"]intValue];
                    receivedorder.orderType = RECEIVE;
                    Worker*orderWorker =[[Worker alloc]init];
                    orderWorker.userType = WORKER;
                    receivedorder.worker = orderWorker;
                    vcClientOrderRequest.currentOrder = receivedorder;
                    vcClientOrderRequest.isComeFromNotification = YES;
                    [self.navigationController pushViewController:vcClientOrderRequest animated:NO];
                }
                else if ([remoteNotificationInfo[@"aps"][@"type"]intValue] == 3) {
                    // handle notification comming from cancel  order from client

                    OrderDetailsViewController*vcClientOrderRequest = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"OrderDetailsViewController"];
                    Order*receivedorder = [[Order alloc]init];
                    receivedorder.orderID =[remoteNotificationInfo[@"aps"][@"msg_id"]intValue];
                    receivedorder.orderType = RECEIVE;
                    Worker*orderWorker =[[Worker alloc]init];
                    orderWorker.userType = WORKER;
                    receivedorder.worker = orderWorker;
                    vcClientOrderRequest.isComeFromCancllingReason = YES;
                    vcClientOrderRequest.currentOrder = receivedorder;
                    vcClientOrderRequest.isComeFromNotification = YES;
                    [self.navigationController pushViewController:vcClientOrderRequest animated:NO];

                }
            }
        
        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark public remote notification method
-(void)didReceivePublicRemoteNotification{
    @try {

        if (remoteNotificationInfo) {
            PublicNotificationsViewController*vcPublicNotification= [applicationStroryBoard instantiateViewControllerWithIdentifier:@"PublicNotificationsViewController"];
            vcPublicNotification.messageID = [remoteNotificationInfo[@"aps"][@"msg_id"]intValue];
            [self.navigationController pushViewController:vcPublicNotification animated:NO];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidDisappear:(BOOL)animated
{
   // ( (AppDelegate*)[[UIApplication sharedApplication]delegate]).evaluationDelegate=nil;
}

- (void)viewDidLoad
{
    @try {
        
        [super viewDidLoad];
//        RTLController *rtl = [[RTLController alloc] init];
//        [rtl disableRTLForView:self.view];
        self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
        [SlideNavigationController sharedInstance].rightBarButtonItem = menuButton;
        [SlideNavigationController sharedInstance].leftbarButtonItem = leftMenuButton;
        [CommonMethods FormatView:headerView WithShadowRadius:2 CornerRadius:0 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];
        
        if (comeFromMenu==YES) {
            [menuButton setHidden:NO];
            [leftMenuButton setHidden:NO];
            [backButton setHidden:YES];
            [self.view sendSubviewToBack:backButton];
            [self.view bringSubviewToFront:menuButton];
            [self.view bringSubviewToFront:leftMenuButton];
            [[SlideNavigationController sharedInstance] setEnableSwipeGesture:YES];
            
        }
        else{
            [menuButton setHidden:YES];
            [leftMenuButton setHidden:YES];
            [backButton setHidden:NO];
            [self.view sendSubviewToBack:menuButton];
            [self.view sendSubviewToBack:leftMenuButton];
            [self.view bringSubviewToFront:backButton];
            [[SlideNavigationController sharedInstance] setEnableSwipeGesture:NO];
            
        }

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveClientRemoteNotification) name:CLIENT_REQUEST_REMOTE_NOTIFICATION object:nil];

         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceivePublicRemoteNotification) name:PUBLIC_REMOTE_NOTIFICATION object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveWorkerRemoteNotification) name:WORKER_REQUEST_REMOTE_NOTIFICATION object:nil];

    }
    @catch (NSException *exception) {
        
    }
}

-(void)back:(UIButton*)sender
{
    HIDE_FULL_LOADING
    HIDE_MODAL_VIEW
    if (isComeFromNotification) {
        [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
    }
    else{
          [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)formatButtonViews:(UIButton*)targetView
{
    @try {
        
        [(UIButton*)targetView setBackgroundImage:[CommonMethods imageWithColor:targetView.backgroundColor ] forState:UIControlStateNormal];
        
        [CommonMethods FormatView:(UIButton*)targetView.superview  WithShadowRadius:2 CornerRadius:0 ShadowOpacity:0.5f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor blackColor] andXPosition:1.0f];
        targetView.layer.cornerRadius = 5;
        targetView.clipsToBounds = YES;
        
    }
    @catch (NSException *exception) {
        
    }
    
}

#pragma -mark slidenavigation delegate
- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return [[BundleLocalization sharedInstance].language isEqualToString:ARABIC_LANG]?YES:NO;
}
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return [[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]?YES:NO;
}
- (IBAction)backButtonAction:(UIButton *)sender {
    @try {
        HIDE_FULL_LOADING
        HIDE_MODAL_VIEW
        if (isComeFromNotification) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    @catch (NSException *exception) {
        
    }
}

@end
