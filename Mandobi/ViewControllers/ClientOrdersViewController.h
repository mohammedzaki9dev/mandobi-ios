//
//  ClientOrdersViewController.h
//  Mandobi
//
//  Created by Assem Imam on 3/12/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"
#import "SKSTableView.h"
@interface ClientOrdersViewController : MasterViewController<UITableViewDelegate, UITableViewDataSource>
{
    

 
}

@property (weak, nonatomic) IBOutlet UITableView *ordersTableView;

@property NSInteger orderID;
@property  OrderType *orderType;
@property  Order*selectedOrder;


@end
