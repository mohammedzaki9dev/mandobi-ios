//
//  RateViewController.h
//  Mandobi
//
//  Created by Bassem on 10/9/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
@import Cosmos;


@interface RateViewController : UIViewController


@property(weak,nonatomic) Order *currentOrder;
@property (weak ,nonatomic) PopupDialog *popup;
@end
