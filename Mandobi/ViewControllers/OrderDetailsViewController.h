//
//  OrderDetailsViewController.h
//  Mandobi
//
//  Created by Assem Imam on 2/12/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface OrderDetailsViewController : MasterViewController<UIAlertViewDelegate>
{
    __weak IBOutlet UIView *rejectView;
    __weak IBOutlet UIView *acceptView;
    __weak IBOutlet UILabel *cancelReasonLabel;
    __weak IBOutlet NSLayoutConstraint *cancellingReasonHeightConstraint;
    __weak IBOutlet UILabel *clientNameTitleLabel;
    __weak IBOutlet NSLayoutConstraint *priceViewHeightConstraint;
    __weak IBOutlet UILabel *priceLabel;
    __weak IBOutlet UIActivityIndicatorView *shipToActivityIndicator;
    __weak IBOutlet UIActivityIndicatorView *shipFromActivityIndicator;
    __weak IBOutlet UILabel *consigneeMobileLabel;
    __weak IBOutlet UILabel *consigneeNameLabel;
    __weak IBOutlet UIImageView *userImageView;
    __weak IBOutlet UIImageView *vehicleTypeImageView;
    __weak IBOutlet UILabel *vehicleTypelabel;
    __weak IBOutlet UILabel *shipToLabel;
    __weak IBOutlet UILabel *shipFromLabel;
    __weak IBOutlet UILabel *mobileLabel;
    __weak IBOutlet UILabel *shipmentTypeLabel;
    __weak IBOutlet UIScrollView *containerScrollView;
    __weak IBOutlet UILabel *currentLocationLabel;
    __weak IBOutlet UILabel *clientNameLabel;
    __weak IBOutlet UILabel *orderTitleLabel;
}
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverName;
@property (weak, nonatomic) IBOutlet UILabel *driverMobile;
@property (weak, nonatomic) IBOutlet UILabel *driverRate;
@property  (weak, nonatomic) PopupDialog *popup;
@property (nonatomic) OrderType  CurrentOrderType;

@property (nonatomic) Order*orderDetail;

@property(nonatomic)BOOL isComeFromCancllingReason;
@property(nonatomic)BOOL isComeFromMandoobResponse;
@property(nonatomic,retain)Order*currentOrder;
- (IBAction)rejectButtonAction:(UIButton *)sender;
- (IBAction)acceptButtonAction:(UIButton *)sender;
@end
