//
//  RegisterClientViewController.h
//  Mandobi
//
//  Created by Assem Imam on 2/3/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface RegisterClientViewController : MasterViewController<UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate>
{
    __weak IBOutlet UIButton *registerButton;
    __weak IBOutlet NSLayoutConstraint *cameraButtonHeightConstraint;
    __weak IBOutlet UITextField *emailTextField;
    __weak IBOutlet UITextField *mobileTextField;
    __weak IBOutlet UITextField *confirmPasswordTextField;
    __weak IBOutlet UITextField *passwordTextField;
    __weak IBOutlet UITextField *userNameTextField;
    __weak IBOutlet UITextField *fullNameTexyField;
    __weak IBOutlet UIButton *cameraButton;
    __weak IBOutlet UITextField *firstNameTextField;
    __weak IBOutlet UIView *containerView;

}

@property(nonatomic)BOOL openForEdit;
- (IBAction)cameraButtonAction:(UIButton *)sender;
@end
