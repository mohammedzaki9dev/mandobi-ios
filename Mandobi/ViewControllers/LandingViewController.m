//
//  LandingViewController.m
//  Mandobi
//
//  Created by Bassem on 9/4/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "LandingViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface LandingViewController ()
@property (weak, nonatomic) IBOutlet UIView *signupView;
@property (weak, nonatomic) IBOutlet UIButton *signupBtn;
@property (weak, nonatomic) IBOutlet UIButton *signinBtn;

@end

@implementation LandingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.view setNeedsLayout];
    
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    gradient.frame = self.signupView.bounds;
//    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed: 193/255.0 green: 120/255.0 blue: 120/255.0 alpha: 1.0] CGColor],
//                                                     [[UIColor colorWithRed: 245/255.0 green: 164/255.0 blue: 164/255.0 alpha: 1.0] CGColor ],
//                                                     [[UIColor colorWithRed: 255/255.0 green: 203/255.0 blue: 203/255.0 alpha: 1.0] CGColor], nil];
//    [self.signupView.layer insertSublayer:gradient atIndex:0];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
