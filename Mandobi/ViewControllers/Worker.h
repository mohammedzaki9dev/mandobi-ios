//
//  Worker.h
//  EasyHome
//
//  Created by Assem Imam on 12/5/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#import "User.h"
#import "Profession.h"
typedef enum {
    SMALL_VEHICLE = 1,
    MEDIUM_VEHICLE = 2,
    LARGE_VEHICLE = 3
}VehicleType;
@interface Worker : User
@property(nonatomic,retain)Profession*profession;
@property(nonatomic)int badVoteCount;
@property(nonatomic)int starsVoteCount;
@property(nonatomic)int favouriteID;
@property(nonatomic,retain)NSString*country;
@property(nonatomic,retain)NSString*city;
@property(nonatomic,retain)NSString*place;
@property(nonatomic)VehicleType vehicleType;
@property(nonatomic,retain)NSString*licenceNumber;
@property(nonatomic,retain)NSString*vehicleColor;

-(instancetype)initWithProfession:(Profession*)worker_profession FullName:(NSString *)full_name UserName:(NSString *)user_name Email:(NSString *)user_email Mobile:(NSString *)user_mobile  UserType:(UserType)type Password:(NSString *)user_password Photo:(UIImage *)user_photo PhotoFile:(NSString*)file;
-(instancetype)initWithFullName:(NSString *)full_name UserName:(NSString *)user_name Email:(NSString *)user_email Mobile:(NSString *)user_mobile  UserType:(UserType)type Password:(NSString *)user_password VehicleTyepe:(VehicleType)vehicle_type LicenceNumber:(NSString*)licence_number VehicleColor:(NSString*)vehicle_color Photo:(UIImage *)user_photo PhotoFile:(NSString*)file;
@end
