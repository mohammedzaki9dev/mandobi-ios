//
//  RegisterWorkerViewController.h
//  Mandobi
//
//  Created by Assem Imam on 2/3/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface RegisterWorkerViewController : MasterViewController<UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate>

{
    __weak IBOutlet UIButton *registerButton;
    __weak IBOutlet NSLayoutConstraint *cameraButtonHeightConstraint;
    __weak IBOutlet UIImageView *largeCheckMarkImageView;
    __weak IBOutlet UIImageView *mediumCheckMarkImageView;
    __weak IBOutlet UIImageView *smallCheckMarkImageView;
    __weak IBOutlet UITextField *licenceNumberTextField;
    __weak IBOutlet UITextField *vehicleNumberTextField;
    __weak IBOutlet UITextField *emailTextField;
    __weak IBOutlet UITextField *mobileTextField;
    __weak IBOutlet UITextField *confirmPasswordTextField;
    __weak IBOutlet UITextField *passwordTextField;
    __weak IBOutlet UITextField *addressTextField;
    __weak IBOutlet UITextField *userNameTextField;
    __weak IBOutlet UITextField *fullNameTexyField;
    __weak IBOutlet UIButton *cameraButton;
    __weak IBOutlet UITextField *firstNameTextField;
    __weak IBOutlet UIView *containerView;
}
@property(nonatomic)BOOL openForEdit;
- (IBAction)vehicleTypeButtonAction:(UIButton *)sender;
- (IBAction)registerButtonAction:(UIButton *)sender;
- (IBAction)cameraButtonAction:(UIButton *)sender;
@end
