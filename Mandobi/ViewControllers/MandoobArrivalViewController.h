//
//  MandoobArrivalViewController.h
//  Mandobi
//
//  Created by Assem Imam on 3/23/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface MandoobArrivalViewController : MasterViewController
{
    __weak IBOutlet UIButton *arrivedButton;
    __weak IBOutlet UILabel *clientNameLabel;
    __weak IBOutlet UIView *mapViewContainer;
    __weak IBOutlet UIImageView *clientImageView;
    __weak IBOutlet UIView *clientView;
}
@property(nonatomic,retain)Order*currentOrder;
- (IBAction)arrivedButtonAction:(UIButton *)sender;
@end
