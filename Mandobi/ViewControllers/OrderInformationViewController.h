//
//  OrderInformationViewController.h
//  Mandobi
//
//  Created by Assem Imam on 3/10/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface OrderInformationViewController : MasterViewController
{
    
    __weak IBOutlet UILabel *distanceLabel;
    __weak IBOutlet UILabel *nameLabel;
    __weak IBOutlet UITextView *phoneTextView;
    __weak IBOutlet UILabel *vehicleTypeLabel;
    __weak IBOutlet UIImageView *vehicleTypeImageView;
    __weak IBOutlet UIButton *rejectButton;
    __weak IBOutlet UIButton *acceptButton;
    __weak IBOutlet UIView *mapContainerView;
    __weak IBOutlet UIScrollView *containerScrollView;
}
@property(nonatomic,retain)Order*submittedOrder;
- (IBAction)acceptButtonAction:(UIButton *)sender;
- (IBAction)rejectButtonAction:(UIButton *)sender;
@end
