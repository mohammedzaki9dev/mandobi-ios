//
//  ForgetPasswordViewController.h
//  Mandobi
//
//  Created by Assem Imam on 6/1/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface ForgetPasswordViewController : MasterViewController
{
    
    __weak IBOutlet UIButton *sendButton;
    __weak IBOutlet UITextField *emailTextField;
}
@property(nonatomic)UserType currentUserType;
- (IBAction)sendButtonAction:(UIButton *)sender;
@end
