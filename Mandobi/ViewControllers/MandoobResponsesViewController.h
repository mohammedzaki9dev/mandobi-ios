//
//  MandoobResponsesViewController.h
//  Mandobi
//
//  Created by Assem Imam on 3/19/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface MandoobResponsesViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UITableView *ordersTableView;
}
@end
