//
//  RequestCancelingViewController.h
//  ChauffeurTaxi
//
//  Created by Assem Imam on 12/30/15.
//  Copyright © 2015 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface RequestCancelingViewController : MasterViewController<UITableViewDataSource>
{
    __weak IBOutlet UITableView *cancelReasonsTableView;
    __weak IBOutlet UIView *allContainerView;
    __weak IBOutlet UIView *optionsContainerView;
    
}
@property(nonatomic,retain) Order*currentOrder;
- (IBAction)radioButtonButtonAction:(UIButton *)sender;
- (IBAction)sendButtonAction:(UIButton *)sender;
@end
