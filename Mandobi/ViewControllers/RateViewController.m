//
//  RateViewController.m
//  Mandobi
//
//  Created by Bassem on 10/9/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "RateViewController.h"

@interface RateViewController ()


//Outlet

@property (weak, nonatomic) IBOutlet UIBarButtonItem *closeBtn;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *driverCarnameLabel;
@property (weak, nonatomic) IBOutlet CosmosView *rateControl;
@property (weak, nonatomic) IBOutlet UITextView *commentTextview;
@property (weak, nonatomic) IBOutlet UIImageView *driverImage;

@end

@implementation RateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view layoutSubviews];
    
//    orderTitleLabel.text = returnedOrder.message;
    NSURL *imageUrl =[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGES_BASE_URL,self.currentOrder.worker.photoFile]];
    if (imageUrl) {
        [self.driverImage setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"user-2"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    self.driverImage.layer.cornerRadius = 38;
    self.driverImage.layer.borderWidth = 2;
    self.driverImage.layer.borderColor = [UIColor whiteColor].CGColor;
    self.driverImage.layer.masksToBounds = YES;
    self.driverNameLabel.text = self.currentOrder.worker.fullName;
//    currentLocationLabel.text=returnedOrder.worker.place;
//    mobileLabel.text=returnedOrder.worker.mobile;
//    consigneeNameLabel.text=returnedOrder.receipientName;
//    consigneeMobileLabel.text=returnedOrder.receipientMobile;
    self.priceLabel.text =[NSString stringWithFormat:@"%0.2f", self.currentOrder.price];
    self.driverCarnameLabel.text = self.currentOrder.worker.city;
//    shipFromLabel.text = [returnedOrder.fromAddress stringByAppendingString:@"\n\n\n"];
//    shipToLabel.text =[returnedOrder.toAddress stringByAppendingString:@"\n\n\n"];
//    _dateLabel.text = returnedOrder.orderDate;
//    _driverName.text = returnedOrder.worker.fullName;
//    _driverMobile.text = returnedOrder.worker.mobile;
//    _driverRate.text = [NSString stringWithFormat:@"%0.1d", returnedOrder.worker.starsVoteCount];
    
    [self getDriverRate];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close:(UIBarButtonItem *)sender {
    
    [self.popup dismiss:^{
        
        NSString*storyBoardName=@"Main_ar";
        //        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
            storyBoardName=@"Main_en";
        }
        
        
        [APP_DELEGATE instantiateInitialViewControllerWithstoryboard:storyBoardName];
    }];
    
}

-(void)getDriverRate{
    if (internetConnection) {
        SHOW_FULL_LOADING
        [[ServiceLogic sharedInstance]getDriverRateForDriverId:[NSNumber numberWithInt:self.currentOrder.worker.userID] Complession:^(id response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                HIDE_FULL_LOADING
                if (response) {
                    if ([response count]>0) {
                        if ([[[response lastObject] objectForKey:@"AVG(rate)"] isEqual:[NSNull null]]) {
                            self.driverRateLabel.text = @"0";
                        }else {
                    self.driverRateLabel.text = [NSString stringWithFormat:@"%0.1f", [[[response lastObject] objectForKey:@"AVG(rate)"]floatValue]];
                        }
                    }
                    else{
                        
                    }
                    
                }
                else{
                    //SHOW_NO_DATA_MESSAGE
                }
            });
        }];
        
    }
    else{
        SHOW_NO_INTERNET_MESSAGE
    }
    
}

-(IBAction)submitrate:(id)sender{
    if (internetConnection) {
        SHOW_FULL_LOADING
        [[ServiceLogic sharedInstance] rateandCommitWithType:[NSNumber  numberWithInt:6] WithOrder: [NSNumber numberWithInt: self.currentOrder.orderID] WithSender: [NSNumber numberWithInt:CurrentLogedUserInfo.userID] WithRate:[NSNumber numberWithDouble: self.rateControl.rating] Withcomment:self.commentTextview.text Complession:^(id response) {
            
        
        //[[ServiceLogic sharedInstance]getDetailsForOrder:currentOrder Complession:^(id response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                HIDE_FULL_LOADING
                if (response) {
                    if ([response count]>0) {
                        
                    }
                    else{
                        
                    }
                    
                }
                else{
                    //SHOW_NO_DATA_MESSAGE
                }
                
                [self dismissViewControllerAnimated:YES completion:nil];
                [self.navigationController popToRootViewControllerAnimated:NO];

                [self.popup dismiss:^{
                    
                    NSString*storyBoardName=@"Main_ar";
                    //        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    
                    if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
                        storyBoardName=@"Main_en";
                    }
                    
                    
                    [APP_DELEGATE instantiateInitialViewControllerWithstoryboard:storyBoardName];
                }];

                
            });
        }];
        
    }
    else{
        SHOW_NO_INTERNET_MESSAGE
    }

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
