//
//  MandoobMapFollowViewController.m
//  Mandobi
//
//  Created by Assem Imam on 3/23/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "OrderDetailsViewController.h"
#import "MandoobMapFollowViewController.h"
#import "ClientMainViewController.h"
#import "MDDirectionService.h"
#import "RateViewController.h"
#import <PopupDialog/PopupDialog-Swift.h>

@interface MandoobMapFollowViewController ()<CLLocationManagerDelegate,GMSMapViewDelegate>
{
    GMSMapView*mapView;
    GMSMarker*toMarker;
    GMSMarker *fromMarker;
    GMSMarker*driverMarker;
    CLLocationManager *locationManager;
    NSTimer *locationTimer;
    Worker*currentMandoob;
    
    MDDirectionService *routingService;
    CLLocationCoordinate2D driverCoordinates;
    CLLocationCoordinate2D fromAddressLocation;
    CLLocationCoordinate2D toAddressLocation;
    NSString *TotalDistance;
    NSMutableArray *RouteMarkersStrings;
    NSMutableArray*mapRoutingPathes;;
    NSMutableArray*addedMarkersList;
    
    NSMutableArray *RouteMarkersStringsMain;
    NSMutableArray*mapRoutingPathesMain;;
    NSMutableArray*addedMarkersListMain;
    
    
}


@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *rateLabel;



    @property (weak, nonatomic) IBOutlet UILabel *DriverNameLabel;
    @property (weak, nonatomic) IBOutlet UIImageView *driverPhotoImage;
    @property (weak, nonatomic) IBOutlet UILabel *driverCarNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *callBtn;
@property (weak, nonatomic) IBOutlet UIView *containerView;
    
    
    
@end

@implementation MandoobMapFollowViewController
@synthesize currentOrder;
- (void)initGPS {
    @try {
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
        }
        if([CLLocationManager locationServicesEnabled]){
            
        }
        else{
            
        }
        [locationManager startMonitoringSignificantLocationChanges];
        
        [locationManager startUpdatingLocation];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)zoomToMyLocation
{
    @try {
        [mapView setCamera:[GMSCameraPosition cameraWithLatitude:CurrentLogedUserInfo.latitude
                                                       longitude:CurrentLogedUserInfo.longitude
                            
                                                            zoom:17
                                                         bearing:30
                                                    viewingAngle:40]];
        [mapView animateToLocation:CLLocationCoordinate2DMake(CurrentLogedUserInfo.latitude, CurrentLogedUserInfo.longitude)];
    }
    @catch (NSException *exception) {
        
    }
}

-(void)zoomToMandoobLocation
{
    @try {
        [mapView setCamera:[GMSCameraPosition cameraWithLatitude:currentMandoob.latitude
                                                       longitude:currentMandoob.longitude
                                                            zoom:14
                                                         bearing:30
                                                    viewingAngle:40]];
        
        [mapView animateToLocation:CLLocationCoordinate2DMake(currentMandoob.latitude, currentMandoob.longitude)];
    }
    @catch (NSException *exception) {
        
    }
}

-(void)getDriverRate{
    if (internetConnection) {
        SHOW_FULL_LOADING
        [[ServiceLogic sharedInstance]getDriverRateForDriverId:[NSNumber numberWithInt:self.currentOrder.worker.userID] Complession:^(id response) {
            dispatch_async(dispatch_get_main_queue(), ^{
                HIDE_FULL_LOADING
                if (response) {
                    if ([response count]>0) {
                        if ([[[response lastObject] objectForKey:@"AVG(rate)"] isEqual:[NSNull null]]) {
                            self.rateLabel.text = @"0";
                        }else {
                            self.rateLabel.text = [NSString stringWithFormat:@"%0.1f", [[[response lastObject] objectForKey:@"AVG(rate)"]floatValue]];
                        }
                    }
                    else{
                        
                    }
                    
                }
                else{
                    //SHOW_NO_DATA_MESSAGE
                }
            });
        }];
        
    }
    else{
        SHOW_NO_INTERNET_MESSAGE
    }
    
}

-(void)locationTimerSelectorUpdateLocation{
    @try {
        if (internetConnection) {
            Worker*worker = [[Worker alloc]init];
            worker.userID = currentOrder.receiverID;
           [[ServiceLogic sharedInstance]getLocationForMandoob:currentOrder.worker Complession:^(id response) {
               dispatch_sync(dispatch_get_main_queue(), ^{
                   if (response) {
                        if ([response isKindOfClass:[worker class] ]) {
                       currentMandoob = response;
                    driverMarker.position =   CLLocationCoordinate2DMake(currentMandoob.latitude, currentMandoob.longitude)  ;
                            
                            [self addMarkerWith:driverMarker.position];
//                       [self zoomToMandoobLocation];
                        }else {
                            [self addMarkerWith:driverMarker.position];

                            
                        }
                   }
               });
           }];
        }
    }
    @catch (NSException *exception) {
        
    }

}

-(void)PerformRoutingMainRoute
{
    
    if(currentOrder.fromLocation){
        CLLocationCoordinate2D fromPosition =currentOrder.fromLocation.coordinate;
        //CLLocationCoordinate2DMake(currentOrder.fromLocation.coordinate.latitude, currentOrder.fromLocation.longitude)  ;
        fromMarker = [GMSMarker markerWithPosition:fromPosition];
        fromMarker.title = @"From Location";
        fromMarker.icon = [UIImage imageNamed:@"marker٣٣"];
        fromMarker.map = mapView;
        
    }
    if(currentOrder.toLocation){
        CLLocationCoordinate2D toPosition =currentOrder.toLocation.coordinate;//CLLocationCoordinate2DMake(currentOrder.toLocation.latitude, currentOrder.toLocation.longitude)  ;
        toMarker = [GMSMarker markerWithPosition:toPosition];
        toMarker.title = @"To Location";
        toMarker.icon = [UIImage imageNamed:@"marker٣٣"];
        toMarker.icon  = [GMSMarker markerImageWithColor:[UIColor greenColor]];
        toMarker.map = mapView;
    }
    
    
    @try {
        
        toAddressLocation = currentOrder.toLocation.coordinate;
        
        
        //[RouteMarkersStrings removeAllObjects];
        
        
        NSString *currentLocationString = [[NSString alloc] initWithFormat:@"%f,%f",
                                           currentOrder.toLocation.coordinate.latitude,currentOrder.toLocation.coordinate.longitude];
        NSString *destinationString = [[NSString alloc] initWithFormat:@"%f,%f",
                                       currentOrder.fromLocation.coordinate.latitude,currentOrder.fromLocation.coordinate.longitude];
        [RouteMarkersStringsMain addObject:currentLocationString];
        [RouteMarkersStringsMain addObject:destinationString];
        
        NSString *sensor = @"false";
        NSArray *parameters = [NSArray arrayWithObjects:sensor, RouteMarkersStringsMain,
                               nil];
        NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
        NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                          forKeys:keys];
        routingService=[[MDDirectionService alloc] init];
        SEL selector = @selector(addDirectionsMainRoute:);
        [routingService setDirectionsQuery:query
                              withSelector:selector
                              withDelegate:self];
        
    }
    @catch (NSException *exception) {
        
    }
    
    
}

-(void)PerformRouting
{
//    @try {
//        
//        toAddressLocation = currentOrder.toLocation.coordinate;
//        
//        
//        //[RouteMarkersStrings removeAllObjects];
//        
//        
//        NSString *currentLocationString = [[NSString alloc] initWithFormat:@"%f,%f",
//                                           currentOrder.toLocation.coordinate.latitude,currentOrder.toLocation.coordinate.longitude];
//        NSString *destinationString = [[NSString alloc] initWithFormat:@"%f,%f",
//                                       currentOrder.fromLocation.coordinate.latitude,currentOrder.fromLocation.coordinate.longitude];
//        [RouteMarkersStrings addObject:currentLocationString];
//        [RouteMarkersStrings addObject:destinationString];
//        
//        NSString *sensor = @"false";
//        NSArray *parameters = [NSArray arrayWithObjects:sensor, RouteMarkersStrings,
//                               nil];
//        NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
//        NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
//                                                          forKeys:keys];
//        routingService=[[MDDirectionService alloc] init];
//        SEL selector = @selector(addDirections:);
//        [routingService setDirectionsQuery:query
//                              withSelector:selector
//                              withDelegate:self];
//        
//    }
//    @catch (NSException *exception) {
//        
//    }
    
    
    @try {
        
        driverCoordinates.latitude = currentMandoob.latitude;
        driverCoordinates.longitude = currentMandoob.longitude;
        fromAddressLocation = currentOrder.fromLocation.coordinate;
        
        [RouteMarkersStrings removeAllObjects];
        
        
        NSString *currentLocationString = [[NSString alloc] initWithFormat:@"%f,%f",
                                           fromAddressLocation.latitude,fromAddressLocation.longitude];
        NSString *destinationString = [[NSString alloc] initWithFormat:@"%f,%f",
                                       driverCoordinates.latitude,driverCoordinates.longitude];
        [RouteMarkersStrings addObject:currentLocationString];
        [RouteMarkersStrings addObject:destinationString];
        
        NSString *sensor = @"false";
        NSArray *parameters = [NSArray arrayWithObjects:sensor, RouteMarkersStrings,
                               nil];
        NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
        NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                          forKeys:keys];
        routingService=[[MDDirectionService alloc] init];
        SEL selector = @selector(addDirections:);
        [routingService setDirectionsQuery:query
                              withSelector:selector
                              withDelegate:self];
        
    }
    @catch (NSException *exception) {
        
    }
    
  
}
-(void)clearAllAddedRoutingPathesFromMap
{
    @try {
        for (GMSPolyline *routingPath in mapRoutingPathes) {
            routingPath.map = nil;
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (void)addDirectionsMainRoute:(NSDictionary *)json{
    @try {
        
        if ([json objectForKey:@"routes"]) {
            if ([[json objectForKey:@"routes"] count]>0) {
                
                GMSMutablePath *path = [GMSMutablePath path];
                NSMutableArray *polyLinesArray = [[NSMutableArray alloc] init];
                int zero=0;
                
                TotalDistance = [[[[[[json objectForKey:@"routes"] objectAtIndex:zero] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"distance"] objectForKey:@"text"];
                
                NSLog(@"%@", TotalDistance);
                //                dispatch_sync(dispatch_get_main_queue(), ^{
                ////                    distanceLabel.text = TotalDistance;
                ////                    submittedOrder.distance = [TotalDistance floatValue];
                //                });
                
                NSArray *steps = [[[[[json objectForKey:@"routes"] objectAtIndex:zero] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"steps"];
                for (int i=0; i<[[[[[[json objectForKey:@"routes"] objectAtIndex:zero] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"steps"]count]; i++) {
                    
                    NSString* encodedPoints =[[[steps objectAtIndex:i]objectForKey:@"polyline"]valueForKey:@"points"];
                    polyLinesArray = [routingService decodePolyLine:encodedPoints];
                    NSUInteger numberOfCC=[polyLinesArray count];
                    for (NSUInteger index = 0; index < numberOfCC; index++) {
                        CLLocation *location = [polyLinesArray objectAtIndex:index];
                        CLLocationCoordinate2D coordinate = location.coordinate;
                        [path addLatitude:coordinate.latitude longitude:coordinate.longitude];
                        if (index==0) {
                            //[self.coordinates addObject:location];
                        }
                    }
                }
//                [self clearAllAddedRoutingPathesFromMap];
                GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
                polyline.strokeWidth = 6 ;
                polyline.geodesic = YES;
                polyline.strokeColor = [UIColor purpleColor];
                polyline.map = mapView;
                [mapRoutingPathesMain addObject:polyline];
                
                [self  performSelectorOnMainThread:@selector(HideLoading) withObject:nil waitUntilDone:NO  ];
            }
            else{
                //                [self performSelectorOnMainThread:@selector(ShowNoRouteAlert) withObject:nil waitUntilDone:NO];
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

- (void)addDirections:(NSDictionary *)json{
    @try {
        
        if ([json objectForKey:@"routes"]) {
            if ([[json objectForKey:@"routes"] count]>0) {
                
                GMSMutablePath *path = [GMSMutablePath path];
                NSMutableArray *polyLinesArray = [[NSMutableArray alloc] init];
                int zero=0;
                
                TotalDistance = [[[[[[json objectForKey:@"routes"] objectAtIndex:zero] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"distance"] objectForKey:@"text"];
                
                NSLog(@"%@", TotalDistance);
//                dispatch_sync(dispatch_get_main_queue(), ^{
////                    distanceLabel.text = TotalDistance;
////                    submittedOrder.distance = [TotalDistance floatValue];
//                });
                
                NSArray *steps = [[[[[json objectForKey:@"routes"] objectAtIndex:zero] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"steps"];
                for (int i=0; i<[[[[[[json objectForKey:@"routes"] objectAtIndex:zero] objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"steps"]count]; i++) {
                    
                    NSString* encodedPoints =[[[steps objectAtIndex:i]objectForKey:@"polyline"]valueForKey:@"points"];
                    polyLinesArray = [routingService decodePolyLine:encodedPoints];
                    NSUInteger numberOfCC=[polyLinesArray count];
                    for (NSUInteger index = 0; index < numberOfCC; index++) {
                        CLLocation *location = [polyLinesArray objectAtIndex:index];
                        CLLocationCoordinate2D coordinate = location.coordinate;
                        [path addLatitude:coordinate.latitude longitude:coordinate.longitude];
                        if (index==0) {
                            //[self.coordinates addObject:location];
                        }
                    }
                }
                [self clearAllAddedRoutingPathesFromMap];
                GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
                polyline.strokeWidth =4;
                polyline.geodesic = YES;
                polyline.strokeColor = [UIColor purpleColor];
                polyline.map = mapView;
                [mapRoutingPathes addObject:polyline];
                
                [self  performSelectorOnMainThread:@selector(HideLoading) withObject:nil waitUntilDone:NO  ];
            }
            else{
//                [self performSelectorOnMainThread:@selector(ShowNoRouteAlert) withObject:nil waitUntilDone:NO];
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)HideLoading
{
    @try {
        
        HIDE_FULL_LOADING
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)ShowNoRouteAlert
{
    [[[UIAlertView alloc]initWithTitle:nil message:@"   تعذر الوصول الى طريق" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil]show];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    @try {
//        [self formatButtonViews:doneButton];
//        [self formatButtonViews:_callBtn];

        
        
        [self.navigationController setNavigationBarHidden:true];
        
              //adding googlemaps mapview
        mapView = [GMSMapView mapWithFrame:CGRectZero camera:nil];
        mapView.mapType=kGMSTypeNormal;
        mapView.delegate = self;
        [mapView setMyLocationEnabled:NO];
        [mapView setMinZoom:5 maxZoom:30];
        [mapView setIndoorEnabled:YES];
        mapView.settings.compassButton = YES;
        mapView.settings.myLocationButton=NO;
        [mapViewContainer addSubview:mapView];
        [mapViewContainer layoutIfNeeded];
        [mapView setFrame:CGRectMake(0, 0, self.view.frame.size.width, mapViewContainer.bounds.size.height)];
        
        
        self.driverCarNumberLabel.layer.borderWidth = 1;
        
        self.driverCarNumberLabel.layer.borderColor= [UIColor lightGrayColor].CGColor;
        self.driverCarNumberLabel.layer.masksToBounds = YES;
//        self.containerView.layer.cornerRadius = 5;
//        
//        self.containerView.layer.borderWidth = 1;
//         self.containerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//         self.containerView.layer.masksToBounds = true;
//        
        
//        doneButton.layer.borderWidth = 1;
//        doneButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        doneButton.layer.masksToBounds = true;
        
//        
//       self.callBtn.layer.borderWidth = 1;
//        self.callBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        self.callBtn.layer.masksToBounds = true;
//
        self.driverPhotoImage.layer.cornerRadius =  self.driverPhotoImage.frame.size.height / 2;
        
        self.driverPhotoImage.layer.borderWidth = 1;
        self.driverPhotoImage.layer.borderColor = [UIColor whiteColor].CGColor;
        self.driverPhotoImage.layer.masksToBounds = true;
//
//        self.liceinLabel.layer.cornerRadius = 1;
//        
//        self.liceinLabel.layer.borderWidth = 1;
//        self.liceinLabel.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        self.liceinLabel.layer.masksToBounds = true;
//
        
        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance]getDetailsForOrder:currentOrder Complession:^(id response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        if ([response count]>0) {
                            Order *returnedOrder =(Order*)response[0];
                            self.currentOrder = returnedOrder;
                            
                            [self compledeWork];
                        }
                        else{
                            
                        }
                        
                    }
                    else{
                        SHOW_NO_DATA_MESSAGE
                    }
                });
            }];
            
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }

        
    
    }
    @catch (NSException *exception) {
        
    }
}


- (IBAction)callDriver:(id)sender {
    NSString *num = [NSString stringWithFormat:@"tel:%@",self.currentOrder.worker.mobile];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:num]];

}

-(void)compledeWork{
    //adding markers to mapview
//    if(currentOrder.fromLocation){
//        
//        
//        CLLocationCoordinate2D fromcoordi=currentOrder.fromLocation.coordinate;        //CLLocationCoordinate2DMake(currentOrder.fromLocation.coordinate.latitude, currentOrder.fromLocation.longitude)  ;
//        fromMarker = [GMSMarker markerWithPosition:fromcoordi];
//        fromMarker.title = @"From Location";
//        fromMarker.icon = [UIImage imageNamed:@"marker٣٣"];
//        fromMarker.map = mapView;
//    }
    
//    if(currentOrder.toLocation){
//      CLLocationCoordinate2D tocoordi=currentOrder.toLocation.coordinate;
//        
//        toMarker = [GMSMarker markerWithPosition:tocoordi];
//        toMarker.title = @"To Location";
//        toMarker.icon = [UIImage imageNamed:@"marker٣٣"];
//        toMarker.icon  = [GMSMarker markerImageWithColor:[UIColor greenColor]];
//        toMarker.map = mapView;
//    }
    
    //GPS Code
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    [self initGPS];
    [self zoomToMyLocation];
    
    RouteMarkersStrings =[[NSMutableArray alloc]init];
     RouteMarkersStringsMain =[[NSMutableArray alloc]init];
    
    locationTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(locationTimerSelectorUpdateLocation) userInfo:nil repeats:YES];
    
//    if (internetConnection) {
//        
//        [[ServiceLogic sharedInstance]getLocationForMandoob:currentOrder.worker Complession:^(id response) {
//            dispatch_sync(dispatch_get_main_queue(), ^{
//                if (response) {
//                    if ([response isKindOfClass:[Worker class] ]) {
//                        currentMandoob = response;
//                        currentMarker.position =   CLLocationCoordinate2DMake(currentMandoob.latitude, currentMandoob.longitude)  ;
//                        currentMarker.map = mapView;
//                        
//                        [NSThread detachNewThreadSelector:@selector(PerformRouting) toTarget:self withObject:nil];
//                        [self zoomToMandoobLocation];
//                        
//                        
//                    }
//                    
//                }
//               
//            });
//        }];
//    }
//    
    
    self.locationLabel.text = currentOrder.toAddress;
    self.DriverNameLabel.text = currentOrder.worker.fullName;
  
    self.driverCarNumberLabel.text = currentOrder.worker.licenceNumber;
    [self PerformRoutingMainRoute];
    
    [self getDriverRate];
    
}
-(void) addMarkerWith:(CLLocationCoordinate2D) driverLocation{
    
   
//    [mapView clear];
        driverMarker = [GMSMarker markerWithPosition:driverLocation];
        driverMarker.title = @"Driver Location";
        driverMarker.icon = [UIImage imageNamed:@"Pin"];
        driverMarker.map = mapView;
    
    
//   if(currentOrder.fromLocation){
//    CLLocationCoordinate2D fromPosition =currentOrder.fromLocation.coordinate;
//    //CLLocationCoordinate2DMake(currentOrder.fromLocation.coordinate.latitude, currentOrder.fromLocation.longitude)  ;
//    fromMarker = [GMSMarker markerWithPosition:fromPosition];
//    fromMarker.title = @"From Location";
//    fromMarker.icon = [UIImage imageNamed:@"marker٣٣"];
//    fromMarker.map = mapView;
//
//   }
//if(currentOrder.toLocation){
//    CLLocationCoordinate2D toPosition =currentOrder.toLocation.coordinate;//CLLocationCoordinate2DMake(currentOrder.toLocation.latitude, currentOrder.toLocation.longitude)  ;
//    toMarker = [GMSMarker markerWithPosition:toPosition];
//    toMarker.title = @"To Location";
//    toMarker.icon = [UIImage imageNamed:@"marker٣٣"];
//    toMarker.icon  = [GMSMarker markerImageWithColor:[UIColor greenColor]];
//    toMarker.map = mapView;
//}

     [NSThread detachNewThreadSelector:@selector(PerformRouting) toTarget:self withObject:nil];
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)cancelOrder{
    
    @try {
        if (internetConnection) {
            SHOW_FULL_LOADING
            
            [[ServiceLogic sharedInstance] ActionForInboxMail:[NSNumber numberWithInt:currentOrder.orderID] ForSender:[NSNumber numberWithInt:currentOrder.senderID] forReciver:[NSNumber numberWithInt:currentOrder.receiverID] withTye:[NSNumber numberWithInt:3] Complession:^(id response) {
                //            [[ServiceLogic sharedInstance]finishSubmittingForOrder:currentOrder Complession:^(id response) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        if ([response objectForKey:@"message"] ) {
                            [TSMessage dismissActiveNotification];
                            
                            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"success_finish_order", nil)  image:nil type:TSMessageNotificationTypeSuccess duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                            
                            [self prsentRate];
                        }
                        else {
                            [TSMessage dismissActiveNotification];
                            [TSMessage showNotificationInViewController:self title:nil subtitle:[response objectForKey:@"message"]  image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                            
                            
                        }
                    }
                    else{
                        [TSMessage dismissActiveNotification];
                        [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"fail_send_order", nil)  image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                    }
                    
                });
            }];
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)doneButtonAction:(UIButton *)sender {
  
    PopupDialog *cancelPopup  = [[PopupDialog alloc] initWithTitle:nil message:NSLocalizedString(@"cancel_order_message", nil)  image:nil buttonAlignment: UILayoutConstraintAxisHorizontal transitionStyle: PopupDialogTransitionStyleFadeIn gestureDismissal:NO completion:nil];
    
    
    CancelButton *cancel = [[CancelButton alloc] initWithTitle:NSLocalizedString(@"CLOSE", nil)  dismissOnTap:YES action:nil];
    
    DefaultButton * confirm = [[DefaultButton alloc] initWithTitle:NSLocalizedString(@"CONFIRM_CANCEL", nil) dismissOnTap:YES action:^{
        
        [self cancelOrder];
    }];
    
    
    if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
        
        [cancelPopup addButton:confirm];
        [cancelPopup addButton:cancel];
    }else {
        [cancelPopup addButton:cancel];
        [cancelPopup addButton:confirm];
       
    }
    
   
    
    [self presentViewController:cancelPopup animated:YES completion:nil];
    
}

-(void) prsentRate{
    
    NSString*storyBoardName=@"Main_ar";
    
    
    if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
        
        
        storyBoardName=@"Main_en";
        
    }
    applicationStroryBoard  = [UIStoryboard storyboardWithName:storyBoardName bundle:nil];
    
    RateViewController*vc = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"RateViewController"];
    
    vc.currentOrder = currentOrder;
    PopupDialog *popup =  [[PopupDialog alloc]initWithViewController:vc buttonAlignment:UILayoutConstraintAxisHorizontal transitionStyle:PopupDialogTransitionStyleBounceUp
                                                    gestureDismissal:YES completion:nil ];
    vc.popup = popup;
    
    //    CancelButton *cancel = [[CancelButton alloc] initWithTitle:NSLocalizedString(@"close", nil) dismissOnTap:YES action:nil];
    //    [popup addButton:cancel];
    //
    [self presentViewController:popup animated:YES completion:nil];
    
    
    //                            vcClientMain.comeFromMenu = YES;
//     [self.navigationController pushViewController:vc animated:YES];//viewControllers = @[vcClientMain];
//    
//      [self performSegueWithIdentifier:@"rate" sender:self];
}
#pragma -mark Location manager delegate methods
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    @try {
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    @try {
        [locationManager startUpdatingLocation];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    @try {
        
        CLLocation *currentLocation = newLocation;
        NSString *_lat,*_long;
        if (currentLocation != nil) {
            _long= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
            _lat= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
            
            CurrentLogedUserInfo.longitude =currentLocation.coordinate.longitude;
            CurrentLogedUserInfo.latitude =currentLocation.coordinate.latitude;
            CLLocationCoordinate2D position = currentLocation.coordinate;
           
        }
    }
    @catch (NSException *exception) {
        
    }
}
- (IBAction)showDetails:(id)sender {
    
//    [self performSegueWithIdentifier:@"details" sender:self];
    
    
    
    
    OrderDetailsViewController *vc = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"OrderDetailsViewController"];
    
    vc.CurrentOrderType = OPEN;
    vc.currentOrder = self.currentOrder;
    
    PopupDialog *popup =  [[PopupDialog alloc]initWithViewController:vc buttonAlignment:UILayoutConstraintAxisHorizontal transitionStyle:PopupDialogTransitionStyleBounceUp
                                                    gestureDismissal:YES completion:nil ];
    vc.popup = popup;
    
//    CancelButton *cancel = [[CancelButton alloc] initWithTitle:NSLocalizedString(@"close", nil) dismissOnTap:YES action:nil];
//    [popup addButton:cancel];
//    
    [self presentViewController:popup animated:YES completion:nil];
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    @try {
         if ([segue.identifier isEqualToString:@"rate"]){
//            ((RateViewController *) segue.destinationViewController)
        }else if ([segue.identifier isEqualToString:@"details"])
        {
            ((OrderDetailsViewController*)segue.destinationViewController).CurrentOrderType = OPEN;
            ((OrderDetailsViewController*)segue.destinationViewController).currentOrder = self.currentOrder;
            
        }
    }
    @catch (NSException *exception) {
        
    }
}




@end
