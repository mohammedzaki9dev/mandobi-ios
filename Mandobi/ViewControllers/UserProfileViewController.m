//
//  UserProfileViewController.m
//  Mandobi
//
//  Created by Assem Imam on 2/3/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "UserProfileViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "RegisterClientViewController.h"

#define CAMERA_BUTTON_INDEX 0
#define GALARRY_BUTTON_INDEX 1
#define CANCEL_BUTTON_INDEX 2
#define PHOTO_ACTIONSHEET_TAG 0

@interface UserProfileViewController ()
{
    UIImage *userImage;
    BOOL enablePickingImage;
    NSNotification*loginNotification;

}
@end

@implementation UserProfileViewController
-(void)formateViews{
    @try {
        userImageView.layer.cornerRadius =userImageView.frame.size.width/2;
        [userImageView setClipsToBounds:YES];
        userImageView.layer.borderWidth = 2;
        userImageView.layer.borderColor = [[UIColor whiteColor]CGColor];
        
        userImageView.superview.layer.cornerRadius =userImageView.superview.frame.size.width/2;
        userImageView.superview.layer.shadowColor = [UIColor blackColor].CGColor;
        userImageView.superview.layer.shadowRadius = 3.f;
        userImageView.superview.layer.shadowOffset = CGSizeMake(0.f, 0.f);
        userImageView.superview.layer.shadowOpacity = 0.4f;
        
        
        
        for (UIView*view in containerScrollView.subviews) {
            if (view.tag == -1) {
                for (UIView*button in view.subviews) {
                    [self formatButtonViews:(UIButton*)button];
                }
            }
        }
    }
    @catch (NSException *exception) {
        
    }
}
-(void)viewDidAppear:(BOOL)animated{
    @try {
        if (CurrentLogedUserInfo.photo) {
            userImageView.image = CurrentLogedUserInfo.photo;
        }
        else{
            NSURL *imageUrl =[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGES_BASE_URL,CurrentLogedUserInfo.photoFile]];
            if (imageUrl) {
                [userImageView setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"square"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            }
        }
        nameLabel.text = CurrentLogedUserInfo.fullName;
        mobileLabel.text = CurrentLogedUserInfo.mobile;
        emailLabel.text = CurrentLogedUserInfo.email;
        
    }
    @catch (NSException *exception) {
        
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    @try {
         [self formateViews];
        loginNotification = [NSNotification notificationWithName:LOGIN_NOTIFICATION object:nil];
        //camera & photos privacy authorization
        enablePickingImage = NO;
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        if(status == AVAuthorizationStatusAuthorized) {
            enablePickingImage =YES;
        } else if(status == AVAuthorizationStatusDenied){
        } else if(status == AVAuthorizationStatusRestricted){
        } else if(status == AVAuthorizationStatusNotDetermined){
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                enablePickingImage = granted;
            }];
        }
        

    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma -mark IBActions
- (IBAction)editUserImageButtonAction:(UIButton *)sender {
    @try {
        
        [self.view endEditing:YES];
        
        if (enablePickingImage) {
            UIActionSheet *photoActionSheet = [[UIActionSheet alloc]initWithTitle:NSLocalizedString(@"choose_image", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"camera", nil),NSLocalizedString(@"album", nil), nil];
            photoActionSheet.tag = PHOTO_ACTIONSHEET_TAG;
            [photoActionSheet showInView:self.view];
        }
        else{
            
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"enableCamera", nil) image:nil type:TSMessageNotificationTypeWarning duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)editButtonAction:(UIButton *)sender {
    @try {
        RegisterClientViewController *vcRegister = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"RegisterClientViewController"];
        vcRegister.comeFromMenu = NO;
        vcRegister.openForEdit = YES;
        [self.navigationController pushViewController:vcRegister animated:YES];
        
    }
    @catch (NSException *exception) {
        
    }
}
- (void)UpdateUserImage {
    @try {
        
        if (internetConnection) {
            SHOW_FULL_LOADING
            User*updatedUser = [[User alloc]init];
            updatedUser.userID = CurrentLogedUserInfo.userID;
            updatedUser.photo = userImage;
            updatedUser.userType = CLIENT;
            
            [[ServiceLogic sharedInstance]  updateImageForUser:updatedUser Complession:^(id response) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    
                    HIDE_FULL_LOADING
                    if (response) {
                        updatedUser.photo = userImage;
                        userImageView.image = userImage;
                        CurrentLogedUserInfo.photoFile = [response objectForKey:@"img_name"]?[response objectForKey:@"img_name"]:nil;
                        CurrentLogedUserInfo.photo = userImage;
                        [((User*)CurrentLogedUserInfo) saveToUserDefaults];
                        
                        [[NSNotificationCenter defaultCenter] postNotification:loginNotification];
                        
                        [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"update_data_success", nil) image:nil type:TSMessageNotificationTypeSuccess duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                        
                    }
                    else{
                        updatedUser.photo = nil;
                        [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"update_data_fail", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                    }
                });
                
            }];
            
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

#pragma -mark uiactionsheet delegate methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    @try {
        
        if (buttonIndex!=CANCEL_BUTTON_INDEX) {
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            switch (buttonIndex) {
                case CAMERA_BUTTON_INDEX:
                {
                    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                }
                    break;
                case GALARRY_BUTTON_INDEX:
                {
                    imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                }
                    break;
            }
            
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self presentViewController:imagePicker animated:YES completion:nil];
            }];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark imagepicker delegate methods
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    @try {
        UIImage* pickedImage = (UIImage *) [info objectForKey:
                                            UIImagePickerControllerOriginalImage];
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        userImage = [UIImage imageWithCGImage:pickedImage.CGImage scale:0.25 orientation:pickedImage.imageOrientation];
        NSData *imageData = [CommonMethods compressImage:userImage WithSize:CGSizeMake(5, 5)];
        if (imageData.length > 2097152) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"image_max_size", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            return;
        }
        [self performSelector:@selector(UpdateUserImage) withObject:nil afterDelay:0.5 ];
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    @try {
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
    @catch (NSException *exception) {
        
    }
}

@end
