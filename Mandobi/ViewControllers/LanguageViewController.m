//
//  LanguageViewController.m
//  EasyHome
//
//  Created by Assem Imam on 12/23/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#import "LanguageViewController.h"
#import "LanguageCell.h"
#import "WelcomeViewController.h"
#import "WorkerMainViewController.h"
#import "ClientMainViewController.h"
#import "MenuViewController.h"
@interface LanguageViewController ()
{
    NSNotification *languageNotification;
}
@end

@implementation LanguageViewController

- (void)viewDidLoad {
    self.comeFromMenu = NO;
    [super viewDidLoad];
    @try {
        [CommonMethods FormatView:containerView WithShadowRadius:2 CornerRadius:10 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:3.0f];
        languageNotification = [[NSNotification alloc]initWithName:CHANGE_LANGUAGE_NOTIFICATION object:nil userInfo:nil];
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"lang"]) {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"lang"] isEqualToString:@"ar"]) {
                [languagetableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]].accessoryType= UITableViewCellAccessoryCheckmark;
                
            }
            else{
                [languagetableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]].accessoryType= UITableViewCellAccessoryCheckmark;
            }
        }
        else{
            [languagetableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]].accessoryType= UITableViewCellAccessoryCheckmark;
        }
        
 
    }
    @catch (NSException *exception) {
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LanguageCell*cell = [tableView dequeueReusableCellWithIdentifier:@"cleLanguage"];
    if (indexPath.row == 0) {
       cell.languageLabel.text = @"اللغة العربية";
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"lang"]) {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"lang"] isEqualToString:@"ar"]) {
                cell.accessoryType= UITableViewCellAccessoryCheckmark;
                
            }
        } else{
            cell.accessoryType= UITableViewCellAccessoryCheckmark;
            
        }
        
    }
    else if (indexPath.row == 1){
      cell.languageLabel.text = @"English";
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"lang"]) {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"lang"] isEqualToString:@"en"]) {
                cell.accessoryType= UITableViewCellAccessoryCheckmark;
                
            }
        }
    }
    
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  2;
}
#pragma -mark uitableview delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        
        

        
        [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]].accessoryType= UITableViewCellAccessoryNone;
        [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]].accessoryType= UITableViewCellAccessoryNone;
        [tableView cellForRowAtIndexPath:indexPath].accessoryType= UITableViewCellAccessoryCheckmark;

        if (indexPath.row == 0) {
            [[BundleLocalization sharedInstance] setLanguage:ARABIC_LANG];
            [[NSUserDefaults standardUserDefaults] setObject:ARABIC_LANG forKey:@"lang"];
            
        }
        else if (indexPath.row == 1){
            [[BundleLocalization sharedInstance]setLanguage:ENGLISH_LANG];
            [[NSUserDefaults standardUserDefaults] setObject:ENGLISH_LANG forKey:@"lang"];
        }
        
        [[NSUserDefaults standardUserDefaults]synchronize];

        NSString*storyBoardName=@"Main_ar";
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
            if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
                storyBoardName=@"Main_en";
            }
        
        
        
        [APP_DELEGATE instantiateInitialViewControllerWithstoryboard:storyBoardName];
//        }
//        else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//            storyBoardName=@"Main_iPad";
//            if ([[BundleLocalization sharedInstance].language isEqualToString:ENGLISH_LANG]) {
//                storyBoardName=@"Main_iPad_en";
//            }
//        }
        
//        applicationStroryBoard  = [UIStoryboard storyboardWithName:storyBoardName bundle:nil];

        
//        if (CurrentUserType==CLIENT) {
//            ClientMainViewController *vcMainClient = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"ClientMainViewController"];
//            vcMainClient.comeFromMenu=YES;
//            [SlideNavigationController sharedInstance].viewControllers =@[vcMainClient];
//        }
//        else if (CurrentUserType==WORKER) {
//            WorkerMainViewController *vcMainWorker= [applicationStroryBoard instantiateViewControllerWithIdentifier:@"WorkerMainViewController"];
//            vcMainWorker.comeFromMenu=YES;
//            [SlideNavigationController sharedInstance].viewControllers =@[vcMainWorker];
//        }
//        else if (CurrentUserType==PUBLIC_USER) {
//            WelcomeViewController *vcMain = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"WelcomeViewController"];
//            [SlideNavigationController sharedInstance].viewControllers =@[vcMain];
//        }
//        MenuViewController *vcMenu = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"MenuViewController"];
        
//        if ([[BundleLocalization sharedInstance].language isEqualToString:ARABIC_LANG]) {
//            
//            [[SlideNavigationController sharedInstance].rightMenu.view removeFromSuperview];
//            [SlideNavigationController sharedInstance].rightMenu = vcMenu ;
//        }
//        else{
//            [[SlideNavigationController sharedInstance].leftMenu.view removeFromSuperview];
//            [SlideNavigationController sharedInstance].leftMenu = vcMenu ;
//
//        }
//        
//        [SlideNavigationController sharedInstance].rightBarButtonItem = menuButton;
//        [SlideNavigationController sharedInstance].leftbarButtonItem = leftMenuButton;

//         UIViewController *vc = [applicationStroryBoard  instantiateInitialViewController];
        
//        [[NSNotificationCenter defaultCenter] postNotification:languageNotification];
        
       
        
    }
    @catch (NSException *exception) {
        
    }
}
@end
