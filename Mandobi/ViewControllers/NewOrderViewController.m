//
//  NewOrderViewController.m
//  Mandobi
//
//  Created by Bassem on 9/4/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//rbenv global

#import "NewOrderViewController.h"

@interface NewOrderViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *containerScrollView;
@property (weak, nonatomic) IBOutlet UITextField *currentLocationTextField;
@property (weak, nonatomic) IBOutlet UITextField *shipmentDescriptionTextField;

@property (weak, nonatomic) IBOutlet UILabel *subCurrentLocationLabel;
@property (weak, nonatomic) IBOutlet UITextField *destinationTextField;
@property (weak, nonatomic) IBOutlet UILabel *subDestinationLabel;
//@property (weak, nonatomic) IBOutlet UITextField *desiredShipmentTextField;

@property (weak, nonatomic) IBOutlet UITextField *consigneeNemeTextField;

@property (weak, nonatomic) IBOutlet UITextField *consigneePhooneTextField;

@property (weak, nonatomic) IBOutlet UITextField *rateTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@end

@implementation NewOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController setNavigationBarHidden:NO animated:NO];

//    self.currentLocationTextField.text = _submittedOrder.fromAddress;
//    self.subCurrentLocationLabel.text = _submittedOrder.fromPlacemark.administrativeArea;
//    self.destinationTextField.text = _submittedOrder.toAddress;
//    self.subDestinationLabel.text = _submittedOrder.toPlacemark.administrativeArea;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backBtn:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(BOOL)validateUserInputs{
    @try {
        
        if ([_shipmentDescriptionTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"Enter_Description", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
            
            return NO;
        }else {
            
            _submittedOrder.message =_shipmentDescriptionTextField.text;
        }

        
        if ([_rateTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"Enter_Rate", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            
            
            return NO;
        }else {
            
            _submittedOrder.price =[ _rateTextField.text floatValue];
        }
        if ([_consigneeNemeTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"Reception_Name", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            return NO;
        }else {
            
            _submittedOrder.receipientName =_consigneeNemeTextField.text;
        }
        
        if ([_consigneePhooneTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
            [TSMessage dismissActiveNotification];
            [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"Reception_Phone", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
            return NO;
        }else {
            
            _submittedOrder.receipientMobile =_consigneePhooneTextField.text;
        }

        
        return YES;
    }
    @catch (NSException *exception) {
        
    }
}


#pragma -mark IBActions
- (IBAction)acceptButtonAction:(UIButton *)sender {
    
    if ([self validateUserInputs]) {
        
    
    @try {
        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance] sendOrder:_submittedOrder ForUser:CurrentLogedUserInfo Complession:^(id response) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        [TSMessage dismissActiveNotification];
                        [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"send_order_success", nil) image:nil type:TSMessageNotificationTypeSuccess duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                        NSMutableDictionary* track_order_data = [[NSMutableDictionary alloc]initWithDictionary: [response objectForKey:@"track_order_data"]] ;
                        //@{@"parent_id":@"parent_id",@"lang":[NSString stringWithFormat:@"%@",[BundleLocalization sharedInstance].language],@"client_id":[NSString stringWithFormat:@"%i",CurrentLogedUserInfo.userID]};
//                        NSDictionary* order =@{
//                                               @"some_data":@"", @"track_order_endpoint": @"http://mandobiapp.com/c_m_s/track_order_for_client.php", @"track_order_data":track_order_data,@"price":[NSNumber numberWithFloat: _submittedOrder.price]};
                        [self.navigationController popToRootViewControllerAnimated:NO];
                        [self.tabBarController setSelectedIndex:0];
                        track_order_data[@"price"]  = [NSNumber numberWithFloat: _submittedOrder.price ];
                                                       
                        [APP_DELEGATE prepartoAskForAsyncTask:track_order_data];
                        
//                        ClientOrdersViewController*vcOrders = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"ClientOrdersViewController"];
//                        vcOrders.comeFromMenu = YES;
//                        self.navigationController.viewControllers=@[vcOrders];
                       
                    }
                    else{
                        [TSMessage dismissActiveNotification];
//                        [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"send_order_fail", nil) image:nil type:TSMessageNotificationTypeError duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                        NSDictionary* track_order_data = @{@"parent_id":@"parent_id",@"lang":[NSString stringWithFormat:@"%@",[BundleLocalization sharedInstance].language],@"client_id":[NSString stringWithFormat:@"%i",CurrentLogedUserInfo.userID]};
                        NSDictionary* order =@{
                                               @"some_data":@"", @"track_order_endpoint": @"http://mandobiapp.com/c_m_s/track_order_for_client.php", @"track_order_data":track_order_data};
                        [self.navigationController popToRootViewControllerAnimated:NO];
                        [self.tabBarController setSelectedIndex:0];
                         [APP_DELEGATE prepartoAskForAsyncTask:order];
                    }
                });
            } ];
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
    }
    @catch (NSException *exception) {
        
    }
    
}
}
//- (IBAction)rejectButtonAction:(UIButton *)sender {
//    @try {
//        UserProfileViewController*vcUserProfile = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
//        vcUserProfile.comeFromMenu = YES;
//        self.navigationController.viewControllers=@[vcUserProfile];
//        
//    }
//    @catch (NSException *exception) {
//        
//    }
//}


@end
