//
//  Worker.m
//  EasyHome
//
//  Created by Assem Imam on 12/5/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#import "Worker.h"

@implementation Worker

@synthesize profession,badVoteCount,starsVoteCount,favouriteID,country,city,place,vehicleType,licenceNumber,vehicleColor;

- (instancetype)initWithProfession:(Profession *)worker_profession FullName:(NSString *)full_name UserName:(NSString *)user_name Email:(NSString *)user_email Mobile:(NSString *)user_mobile  UserType:(UserType)type Password:(NSString *)user_password Photo:(UIImage *)user_photo PhotoFile:(NSString*)file
{
    self = [super init];
    if (self) {
      self =  [super initWithFullName:full_name UserName:user_name Email:user_email Mobile:user_mobile Password:user_password  UserType:type Photo:user_photo PhotoFile:file];
        self.profession = worker_profession;
    }
    return self;
}
-(instancetype)initWithFullName:(NSString *)full_name UserName:(NSString *)user_name Email:(NSString *)user_email Mobile:(NSString *)user_mobile  UserType:(UserType)type Password:(NSString *)user_password VehicleTyepe:(VehicleType)vehicle_type LicenceNumber:(NSString*)licence_number VehicleColor:(NSString*)vehicle_color Photo:(UIImage *)user_photo PhotoFile:(NSString*)file
{
    self = [super init];
    if (self) {
        self =  [super initWithFullName:full_name UserName:user_name Email:user_email Mobile:user_mobile Password:user_password  UserType:type Photo:user_photo PhotoFile:file];
        self.licenceNumber = licence_number;
        self.vehicleType = vehicle_type;
        self.vehicleColor = vehicle_color;
    }
    return self;

}
-(instancetype)initFromUserDefaults{
     self =  [super initFromUserDefaults];
    if (self) {
         id CurrentUser =[[NSUserDefaults standardUserDefaults] objectForKey:@"User"];
        self.licenceNumber  = CurrentUser[@"licenceNumber"];
        self.vehicleType  = [CurrentUser[@"vehicleType"]intValue];

    }
    return  self;
}
-(void)saveToUserDefaults{
    NSMutableDictionary*userDictionary = [NSMutableDictionary new];
    [userDictionary setObject:self.mobile?self.mobile :@"" forKey:@"mobile"];
    [userDictionary setObject:self.password?self.password :@"" forKey:@"password"];
    [userDictionary setObject:self.fullName?self.fullName :@"" forKey:@"fullName"];
    [userDictionary setObject:self.userName?self.userName :@"" forKey:@"userName"];
    [userDictionary setObject:[NSString stringWithFormat:@"%i",self.userType]  forKey:@"userType"];
    [userDictionary setObject:self.photoFile?self.photoFile:@"" forKey:@"photoFile"];
    [userDictionary setObject:[NSString stringWithFormat:@"%i", self.userID] forKey:@"userID"];
    [userDictionary setObject:self.email?self.email:@"" forKey:@"email"];
    [userDictionary setObject:self.nationality?self.nationality:@"" forKey:@"nationality"];
    [userDictionary setObject:[NSString stringWithFormat:@"%i", self.userLevel] forKey:@"userLevel"];
    [userDictionary setObject:[NSString stringWithFormat:@"%i", self.isActive] forKey:@"isActive"];
    NSData *imageData = [CommonMethods compressImage:self.photo WithSize:CGSizeMake(80, 80)];
    [userDictionary setObject:imageData?imageData:@"" forKey:@"photo"];
    [userDictionary setObject:[NSString stringWithFormat:@"%i", self.vehicleType ] forKey:@"vehicleType"];
    [userDictionary setObject:self.licenceNumber forKey:@"licenceNumber"];
    [[NSUserDefaults standardUserDefaults] setObject:userDictionary forKey:@"User"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}
@end
