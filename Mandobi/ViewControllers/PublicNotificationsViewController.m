//
//  PublicNotificationsViewController.m
//  ChauffeurTaxi
//
//  Created by Assem Imam on 1/14/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "PublicNotificationsViewController.h"

@interface PublicNotificationsViewController ()

@end

@implementation PublicNotificationsViewController
@synthesize messageID;
- (void)viewDidLoad {
    [super viewDidLoad];
    @try {
        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance]  getPublicNotificationDetails:self.messageID Complession:^(id response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING;
                    if (response) {
                        notificationDetailsTextView.text =[NSNull null]!= [response objectForKey:@"msg"]?[response objectForKey:@"msg"]:@"";
                    }
                    else{
                        [TSMessage showNotificationInViewController:self title:nil subtitle:NSLocalizedString(@"no_data_found", nil)  image:nil type:TSMessageNotificationTypeWarning duration:3.0 callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
                    }
                });
            }];
        }
        else{
            SHOW_NO_INTERNET_MESSAGE
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
