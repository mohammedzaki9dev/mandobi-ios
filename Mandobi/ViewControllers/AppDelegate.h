//
//  AppDelegate.h
//  EasyHome
//
//  Created by Assem Imam on 11/28/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SHOW_FULL_LOADING   [((AppDelegate*)  [UIApplication sharedApplication].delegate) showFullLoadingView:YES WithContainerViewController:self];
#define HIDE_FULL_LOADING [(AppDelegate*)[[UIApplication sharedApplication]delegate] showFullLoadingView:NO WithContainerViewController:nil];
#define SHOW_NO_INTERNET_MESSAGE  [((AppDelegate*)  [UIApplication sharedApplication].delegate) showNoInternetMessage];
#define SHOW_NO_DATA_MESSAGE  [((AppDelegate*)  [UIApplication sharedApplication].delegate) showNoDataMessage];

#define SHOW_MODAL_VIEW  [((AppDelegate*)  [UIApplication sharedApplication].delegate) showModalView:YES WithContainerViewController:self];
#define HIDE_MODAL_VIEW  [((AppDelegate*)  [UIApplication sharedApplication].delegate) showModalView:NO WithContainerViewController:self];

#define ENGLISH_FONT @"SFUIDisplay-Semibold"//"SFUIDisplay-Regular"
//#define ARABIC_FONT @"GESSTwoMedium-Medium"
#define ARABIC_FONT @"SFUIDisplay-Semibold"//"HacenBeirutLight"//"HacenBeirutLtX3"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
-(void)showFullLoadingView:(BOOL)show WithContainerViewController:(UIViewController*)controller;
-(void)showNoInternetMessage;
-(void)showModalView:(BOOL)show WithContainerViewController:(UIViewController*)controller;
-(void)showNoDataMessage;
-(void)showRequestCanceledMessage;
-(void)instantiateInitialViewControllerWithstoryboard:(NSString*)name;
-(void) prepartoAskForAsyncTask :(NSDictionary*) order;

@end

