//
//  UsePolicyViewController.h
//  Mandobi
//
//  Created by Assem Imam on 3/12/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface UsePolicyViewController : MasterViewController
{
    __weak IBOutlet UITextView *detailTextView;
    
    __weak IBOutlet UILabel *titleLabel;
}
@end
