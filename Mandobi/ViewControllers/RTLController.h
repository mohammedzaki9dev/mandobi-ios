//
//  RTLController.h
//  ChauffeurTaxi
//
//  Created by Assem Imam on 2/7/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTLController : NSObject
- (void)disableRTLForView:(UIView *)view;
@end
