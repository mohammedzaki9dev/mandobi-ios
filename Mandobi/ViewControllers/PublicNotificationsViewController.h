//
//  PublicNotificationsViewController.h
//  ChauffeurTaxi
//
//  Created by Assem Imam on 1/14/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface PublicNotificationsViewController : MasterViewController
{
    __weak IBOutlet UITextView *notificationDetailsTextView;
    __weak IBOutlet UIView *containerView;
}
@property(nonatomic)int messageID;
@end
