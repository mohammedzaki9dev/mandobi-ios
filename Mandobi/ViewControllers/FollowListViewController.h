//
//  FollowListViewController.h
//  Mandobi
//
//  Created by Assem Imam on 3/27/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface FollowListViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UITableView *followersTableView;
}
- (IBAction)followButtonAction:(UIButton *)sender;

@end
