//
//  WorkerMainViewController.h
//  Mandobi
//
//  Created by Assem Imam on 2/7/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface WorkerMainViewController : MasterViewController
{
    __weak IBOutlet UIView *containerView;
}
- (IBAction)logoutButtonAction:(UIButton *)sender;
@end
