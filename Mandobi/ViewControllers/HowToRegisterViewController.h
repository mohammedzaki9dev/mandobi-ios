//
//  HowToRegisterViewController.h
//  Mandobi
//
//  Created by Assem Imam on 2/9/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface HowToRegisterViewController : MasterViewController
{
    __weak IBOutlet UITextView *detailTextView;
    __weak IBOutlet UILabel *titleLabel;
    
}
@end
