//
//  UserProfileViewController.h
//  Mandobi
//
//  Created by Assem Imam on 2/3/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface UserProfileViewController : MasterViewController<UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate>
{
    __weak IBOutlet UILabel *emailLabel;
    __weak IBOutlet UILabel *mobileLabel;
    __weak IBOutlet UILabel *nameLabel;
    __weak IBOutlet UIImageView *userImageView;
    __weak IBOutlet UIView *imageContainerView;
    __weak IBOutlet UIScrollView *containerScrollView;
}
- (IBAction)editUserImageButtonAction:(UIButton *)sender;
- (IBAction)editButtonAction:(UIButton *)sender;
@end
