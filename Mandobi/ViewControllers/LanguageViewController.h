//
//  LanguageViewController.h
//  EasyHome
//
//  Created by Assem Imam on 12/23/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface LanguageViewController : MasterViewController<UITableViewDataSource,UITableViewDelegate,SlideNavigationControllerDelegate>
{
    __weak IBOutlet UIView *containerView;

    IBOutlet UITableView *languagetableView;
}
@end
