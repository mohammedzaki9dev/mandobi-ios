//
//  WorkerProfileViewController.h
//  Mandobi
//
//  Created by Assem Imam on 2/3/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MasterViewController.h"

@interface WorkerProfileViewController : MasterViewController<UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate>
{
   
    __weak IBOutlet UIButton *availableButton;
    __weak IBOutlet UIButton *largeVehicleButton;
    __weak IBOutlet UIButton *unAvailableButton;
    __weak IBOutlet UIButton *mediumVehicleButton;
    __weak IBOutlet UIButton *smallVehicleButton;
    __weak IBOutlet UILabel *licenceNumberLabel;
    __weak IBOutlet UILabel *vehicleColorLabel;
    __weak IBOutlet UILabel *emailLabel;
    __weak IBOutlet UILabel *mobileLabel;
    __weak IBOutlet UILabel *addressLabel;
    __weak IBOutlet UILabel *nameLabel;
    __weak IBOutlet UIImageView *userImageView;
    __weak IBOutlet UIView *imageContainerView;
    __weak IBOutlet UIScrollView *containerScrollView;
}
- (IBAction)editUserImageButtonAction:(UIButton *)sender;
- (IBAction)editButtonAction:(UIButton *)sender;
- (IBAction)availableButtonAction:(UIButton *)sender;
- (IBAction)unavailableButtonAction:(UIButton *)sender;
@end
