//
//  WorkerMainViewController.m
//  Mandobi
//
//  Created by Assem Imam on 2/7/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "WorkerMainViewController.h"
#import "WelcomeViewController.h"

@interface WorkerMainViewController ()<CLLocationManagerDelegate>
{
    CLLocationManager*locationManager;
    NSTimer *locationTimer;
    BOOL locationSubmitted;
    NSNotification*loginNotification;

}
@end

@implementation WorkerMainViewController
-(void)locationTimerSelector
{
    locationSubmitted=NO;
    [locationManager startMonitoringSignificantLocationChanges];
    [locationManager startUpdatingLocation];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    @try {
        locationSubmitted = NO;
        loginNotification = [NSNotification notificationWithName:LOGIN_NOTIFICATION object:nil];

        [CommonMethods FormatView:containerView WithShadowRadius:2 CornerRadius:5 ShadowOpacity:0.8f BorderWidth:0 BorderColor:[UIColor blackColor]  ShadowColor:[UIColor lightGrayColor] andXPosition:2.0f];
        
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"]==nil) {
            locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
            if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [locationManager requestWhenInUseAuthorization];
            }
            [locationManager startMonitoringSignificantLocationChanges];
            
            [locationManager startUpdatingLocation];
        }
        else{
            CurrentLogedUserInfo.longitude =[[[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"]doubleValue];
            CurrentLogedUserInfo.latitude =[[[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"]doubleValue];
            if (internetConnection) {
                [[ServiceLogic sharedInstance]  updateLocationForUser:CurrentLogedUserInfo Complession:^(id response) {
                    if (response) {
                       
                    }
                    
                }];
            }
            else{
                SHOW_NO_INTERNET_MESSAGE
            }

        }
         locationTimer = [NSTimer scheduledTimerWithTimeInterval:60*60*5 target:self selector:@selector(locationTimerSelector) userInfo:nil repeats:YES];
        
    }
    @catch (NSException *exception) {
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma -mark Location manager delegate methods
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    @try {
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    @try {
        [locationManager startUpdatingLocation];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    @try {
        
        CLLocation *currentLocation = newLocation;
        NSString *_lat,*_long;
        if (currentLocation != nil) {
            _long= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
            _lat= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
            
            CurrentLogedUserInfo.longitude =currentLocation.coordinate.longitude;
            CurrentLogedUserInfo.latitude =currentLocation.coordinate.latitude;
            
            if (locationSubmitted== NO) {
                if (internetConnection) {
                    [[ServiceLogic sharedInstance]  updateLocationForUser:CurrentLogedUserInfo Complession:^(id response) {
                        locationSubmitted = YES;
                    }];
                }
                else{
                    SHOW_NO_INTERNET_MESSAGE
                }
            }
            
        }
        
        [locationManager stopUpdatingLocation];
        

        
    }
    @catch (NSException *exception) {
        
    }
}


- (IBAction)logoutButtonAction:(UIButton *)sender {
    @try {
        if (internetConnection) {
            SHOW_FULL_LOADING
            [[ServiceLogic sharedInstance]  logOutForUser:CurrentLogedUserInfo Complession:^(id response) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    HIDE_FULL_LOADING
                    if (response) {
                        CurrentLogedUserInfo = nil;
                        CurrentUserType =PUBLIC_USER;
                        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%i",CurrentUserType] forKey:@"loged_user_type"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [[NSNotificationCenter defaultCenter] postNotification:loginNotification];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        WelcomeViewController*vcWelcome = [applicationStroryBoard instantiateViewControllerWithIdentifier:@"WelcomeViewController"];
                        vcWelcome.comeFromMenu = YES;
                        [SlideNavigationController sharedInstance].viewControllers=@[vcWelcome];
                    }
                });
                
            }];
            
        }
        else{//no internet message
           SHOW_NO_INTERNET_MESSAGE
        }
  
    }
    @catch (NSException *exception) {
        
    }
}
@end
