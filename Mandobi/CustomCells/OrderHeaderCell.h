//
//  OrderHeaderCell.h
//  Mandobi
//
//  Created by Assem Imam on 3/15/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SKSTableViewCell.h"

@interface OrderHeaderCell : SKSTableViewCell
{
   
    
}

@property (weak, nonatomic) __weak IBOutlet UILabel *orderTitleLabel;
@end
