//
//  CancelCell.h
//  ChauffeurTaxi
//
//  Created by Assem Imam on 1/18/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cancelReasonLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelOptionButton;

@end
