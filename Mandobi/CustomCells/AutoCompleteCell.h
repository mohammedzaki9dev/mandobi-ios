//
//  AutoCompleteCell.h
//  Mandobi
//
//  Created by Assem Imam on 4/11/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutoCompleteCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *placeNameLabel;

@end
