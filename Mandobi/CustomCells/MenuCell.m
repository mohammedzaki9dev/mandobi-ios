//
//  MenuCellCell.m
//  Mandobi
//
//  Created by Assem Imam on 2/9/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
