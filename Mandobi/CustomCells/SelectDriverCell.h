//
//  OrderDetailCell.h
//  Mandobi
//
//  Created by Assem Imam on 3/15/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectDriverCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *DriverName;
@property (weak, nonatomic) IBOutlet UILabel *DriverCarName;
@property (weak, nonatomic) IBOutlet UILabel *CarColor;
@property (weak, nonatomic) IBOutlet UILabel *DriverRate;
@property (weak, nonatomic) IBOutlet UILabel *OrderPrice;
@property (weak, nonatomic) IBOutlet UILabel *vehicleTypeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectionImage;

@end
