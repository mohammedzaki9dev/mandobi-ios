//
//  MenuCellCell.h
//  Mandobi
//
//  Created by Assem Imam on 2/9/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *menuItemLabel;
@property (weak, nonatomic) IBOutlet UIImageView *menuItemImageView;

@end
