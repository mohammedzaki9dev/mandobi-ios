//
//  OrderDetailCell.h
//  Mandobi
//
//  Created by Assem Imam on 3/15/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Cosmos/Cosmos-Swift.h>
@interface OrderDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fromLabel;
@property (weak, nonatomic) IBOutlet UILabel *toLable;

@property (weak, nonatomic) IBOutlet UILabel *toLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *feesLabel;
@property (weak, nonatomic) IBOutlet UILabel *vehicleTypeLabel;


@property (weak, nonatomic) IBOutlet UIImageView *driverImage;
@property (weak, nonatomic) IBOutlet UILabel *driverName;
@property (weak, nonatomic) IBOutlet CosmosView *driverStars;

@end
