//
//  FollowCell.h
//  Mandobi
//
//  Created by Assem Imam on 3/27/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *fromAddresslabel;
@property (weak, nonatomic) IBOutlet UILabel *toAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *vehicleTypeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *vehicleTypeImageView;
@property (weak, nonatomic) IBOutlet UIButton *followButton;

@end
