//
//  WorkerOrderCell.h
//  Mandobi
//
//  Created by Assem Imam on 2/11/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkerOrderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *orderStatusImageView;

@end
