//
//  LanguageCell.h
//  EasyHome
//
//  Created by Assem Imam on 12/23/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *languageLabel;

@end
