//
//  WebService.h
//  Shauffeur Taxi
//
//  Created by Assem Imam on 12/2/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#import <Foundation/Foundation.h>
#define REQUEST_CONTENT_TYPE @"application/x-www-form-urlencoded"
#define TIME_OUT_INTERVAL 20.0f
@interface WebService : NSObject
+(void)performGetRequestWithURL:(NSURL*)url  Delegate:(id<NSURLSessionDelegate>)delegate Complession:(void(^)(id response) )complession;
+(void)performPostRequestWithURL:(NSURL*)url  Parameters:(NSDictionary*)parameters Delegate:(id<NSURLSessionDelegate>)delegate Complession:(void(^)(id response) )complession;
+(void)performPostRequestWithURL:(NSURL*)url Parameters:(NSDictionary*)parameters WithUploadFile:(NSString*)file_path Complession:(void(^)(id response) )complession;

+(void)performPostJSONRequestWithURL:(NSURL*)url Parameters:(NSDictionary*)parameters  Delegate:(id<NSURLSessionDelegate>)delegate Complession:(void(^)(id response) )complession;

@end
