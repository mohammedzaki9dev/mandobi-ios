//
//  ServiceLogic.m
//  Shauffeur Taxi
//
//  Created by Assem Imam on 12/2/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#import "ServiceLogic.h"
#import "CommonMethods.h"
#import "SelectedDriver.h"

@implementation ServiceLogic
@synthesize delegate;
+ (ServiceLogic*) sharedInstance {
    @synchronized(self) {
        static ServiceLogic* instance = nil;
        if (instance == nil) {
            instance = [[ServiceLogic alloc] init];
        }
        return instance;
    }
}
-(void)autoCompleteForPlace:(NSString *)place Complession:(void (^)(id))complession{
    @try {
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=geocode&key=%@",place ,GOOGLE_MAPS_API_KEY];
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        NSLog(@"url : %@",[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=geocode&key=%@",place,GOOGLE_MAPS_API_KEY]);
        NSLog(@"url encode : %@",[[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=geocode&key=%@",place,GOOGLE_MAPS_API_KEY]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            
            if (response) {
                if ([[response objectForKey:@"status"] isEqualToString:@"OK"]) {
                    if ([[response objectForKey:@"predictions"]count]>0) {
                        NSMutableArray *placesList = [[NSMutableArray alloc]init];
                        for (id place in [response objectForKey:@"predictions"]) {
                            Place *addedPlace = [[Place alloc]init];
                            addedPlace.placeName = [NSNull null]!=[place objectForKey:@"description"] && [place objectForKey:@"description"]?[place objectForKey:@"description"]:@"";
                            addedPlace.PlaceID = [NSNull null]!=[place objectForKey:@"place_id"] && [place objectForKey:@"place_id"]?[place objectForKey:@"place_id"]:@"";
                            addedPlace.PlaceReferceID = [NSNull null]!=[place objectForKey:@"reference"] && [place objectForKey:@"reference"]?[place objectForKey:@"reference"]:@"";
                            [placesList addObject:addedPlace];
                        }
                        complession(placesList);
                        
                    }
                    else{
                        complession(nil);
                        
                    }
                }
                else{
                    complession(nil);
                    
                }
            }
            else{
                complession(nil);
            }
        }];
    }
    @catch (NSException *exception) {
        
    }

}
-(void)getFollowesForUser:(User*)user Complession:(void (^)(id))complession{
    @try {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@view_inbox.php?member_id=%i&type=send&u_t=w&t=2",WEB_SERVICE_URL,user.userID]];
    
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"] count]>0) {
                            NSMutableArray *ordersList = [[NSMutableArray alloc]init];
                            for (id order in [response objectForKey:@"data"] ) {
                                Order *addedOrder = [[Order alloc]init];
                                addedOrder.orderID = [NSNull null]!=[order objectForKey:@"mail_id"]?[[order objectForKey:@"mail_id"]intValue]:-1;
                                addedOrder.senderID =[NSNull null]!=[order objectForKey:@"sender_id"]?[[order objectForKey:@"sender_id"]intValue]:-1;
                                addedOrder.receiverID =[NSNull null]!=[order objectForKey:@"reciever_id"]?[[order objectForKey:@"reciever_id"]intValue]:-1;
                                addedOrder.message =[NSNull null]!=[order objectForKey:@"msg"]?[order objectForKey:@"msg"]:@"";
                                addedOrder.subject =[NSNull null]!=[order objectForKey:@"subject"]?[order objectForKey:@"subject"]:@"";
                                addedOrder.orderDate =[NSNull null]!=[order objectForKey:@"date"]?[order objectForKey:@"date"]:@"";
                                addedOrder.orderTime =[NSNull null]!=[order objectForKey:@"time"]?[order objectForKey:@"time"]:@"";
                                addedOrder.receipientMobile =[NSNull null]!=[order objectForKey:@"recipient_tel"]?[order objectForKey:@"recipient_tel"]:@"";
                                addedOrder.receipientName =[NSNull null]!=[order objectForKey:@"recipient_user"]?[order objectForKey:@"recipient_user"]:@"";
                                addedOrder.isViewed =[NSNull null]!=[order objectForKey:@"is_view"]?[[order objectForKey:@"is_view"]intValue]==1?YES:NO:NO;
                                double fromLatitude = [NSNull null]!=[order objectForKey:@"from_latitude"]?[[order objectForKey:@"from_latitude"]doubleValue]:0;
                                double fromLongitude = [NSNull null]!=[order objectForKey:@"from_longitude"]?[[order objectForKey:@"from_longitude"]doubleValue]:0;
                                double toLatitude = [NSNull null]!=[order objectForKey:@"to_latitude"]?[[order objectForKey:@"to_latitude"]doubleValue]:0;
                                double toLongitude = [NSNull null]!=[order objectForKey:@"to_longitude"]?[[order objectForKey:@"to_longitude"]doubleValue]:0;
                                
//                                Location *fromLocation = [[Location alloc]initWithLatitude:fromLatitude AndLongitude:fromLongitude] ;
//                                Location *toLocation =[[Location alloc]initWithLatitude:toLatitude AndLongitude:toLongitude] ;
                                
                                addedOrder.fromLocation = [[CLLocation alloc] initWithLatitude: fromLatitude  longitude:fromLongitude];;
                                addedOrder.toLocation = [[CLLocation alloc] initWithLatitude: toLatitude  longitude:toLongitude];;
                                addedOrder.orderType = RECEIVE;
                                Worker *orderWorker = [[Worker alloc]init];
                                orderWorker.userID =[NSNull null]!=[order objectForKey:@"member_id"]?[[order objectForKey:@"member_id"]intValue]:-1;
                                orderWorker.fullName =[NSNull null]!=[order objectForKey:@"f_name"]?[order objectForKey:@"f_name"]:@"";
                                orderWorker.mobile =[NSNull null]!=[order objectForKey:@"mobile"]?[order objectForKey:@"mobile"]:@"";
                                orderWorker.place =[NSNull null]!=[order objectForKey:@"place"]?[order objectForKey:@"place"]:@"";
                                orderWorker.photoFile =[NSNull null]!=[order objectForKey:@"member_image"]?[order objectForKey:@"member_image"]:@"";
                                orderWorker.vehicleType =[NSNull null]!=[order objectForKey:@"vehicle_type"]?[[order objectForKey:@"vehicle_type"]intValue]:-1;
                                addedOrder.worker = orderWorker;
                                addedOrder.worker.userType = user.userType;
                                [ordersList addObject:addedOrder];
                            }
                            complession(ordersList);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }

}


-(void)getMandoobResponsesForUser:(User*)user Complession:(void (^)(id))complession{
    
    @try {
    
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@view_inbox.php?member_id=%i&type=recieve&u_t=m&t=1",WEB_SERVICE_URL,user.userID]];
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"] count]>0) {
                            NSMutableArray *ordersList = [[NSMutableArray alloc]init];
                            for (id order in [response objectForKey:@"data"] ) {
                                Order *addedOrder = [[Order alloc]init];
                                addedOrder.orderID = [NSNull null]!=[order objectForKey:@"mail_id"]?[[order objectForKey:@"mail_id"]intValue]:-1;
                                addedOrder.senderID =[NSNull null]!=[order objectForKey:@"sender_id"]?[[order objectForKey:@"sender_id"]intValue]:-1;
                                addedOrder.receiverID =[NSNull null]!=[order objectForKey:@"reciever_id"]?[[order objectForKey:@"reciever_id"]intValue]:-1;
                                addedOrder.message =[NSNull null]!=[order objectForKey:@"msg"]?[order objectForKey:@"msg"]:@"";
                                addedOrder.subject =[NSNull null]!=[order objectForKey:@"subject"]?[order objectForKey:@"subject"]:@"";
                                addedOrder.orderDate =[NSNull null]!=[order objectForKey:@"date"]?[order objectForKey:@"date"]:@"";
                                addedOrder.orderTime =[NSNull null]!=[order objectForKey:@"time"]?[order objectForKey:@"time"]:@"";
                                addedOrder.receipientMobile =[NSNull null]!=[order objectForKey:@"recipient_tel"]?[order objectForKey:@"recipient_tel"]:@"";
                                addedOrder.receipientName =[NSNull null]!=[order objectForKey:@"recipient_user"]?[order objectForKey:@"recipient_user"]:@"";
                                addedOrder.isViewed =[NSNull null]!=[order objectForKey:@"is_view"]?[[order objectForKey:@"is_view"]intValue]==1?YES:NO:NO;
                                double fromLatitude = [NSNull null]!=[order objectForKey:@"from_latitude"]?[[order objectForKey:@"from_latitude"]doubleValue]:0;
                                double fromLongitude = [NSNull null]!=[order objectForKey:@"from_longitude"]?[[order objectForKey:@"from_longitude"]doubleValue]:0;
                                double toLatitude = [NSNull null]!=[order objectForKey:@"to_latitude"]?[[order objectForKey:@"to_latitude"]doubleValue]:0;
                                double toLongitude = [NSNull null]!=[order objectForKey:@"to_longitude"]?[[order objectForKey:@"to_longitude"]doubleValue]:0;
                                
                                CLLocation * fromLocation=[[CLLocation alloc]initWithLatitude:fromLatitude longitude:fromLongitude];
                                
                                
                                CLLocation * toLocation=[[CLLocation alloc]initWithLatitude:toLatitude longitude:toLongitude];

                                
                                addedOrder.fromLocation = fromLocation;
                                addedOrder.toLocation = toLocation;
                                addedOrder.orderType = RECEIVE;
                                Worker *orderWorker = [[Worker alloc]init];
                                orderWorker.userID =[NSNull null]!=[order objectForKey:@"member_id"]?[[order objectForKey:@"member_id"]intValue]:-1;
                                orderWorker.fullName =[NSNull null]!=[order objectForKey:@"f_name"]?[order objectForKey:@"f_name"]:@"";
                                orderWorker.mobile =[NSNull null]!=[order objectForKey:@"mobile"]?[order objectForKey:@"mobile"]:@"";
                                orderWorker.place =[NSNull null]!=[order objectForKey:@"place"]?[order objectForKey:@"place"]:@"";
                                orderWorker.photoFile =[NSNull null]!=[order objectForKey:@"member_image"]?[order objectForKey:@"member_image"]:@"";
                                orderWorker.vehicleType =[NSNull null]!=[order objectForKey:@"vehicle_type"]?[[order objectForKey:@"vehicle_type"]intValue]:-1;
                                addedOrder.worker = orderWorker;
                                addedOrder.worker.userType = user.userType;
                                [ordersList addObject:addedOrder];
                            }
                            complession(ordersList);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }

    
}
-(void)getUsePolicyWithComplession:(void (^)(id))complession{
    @try {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@detail.php?d=2",WEB_SERVICE_URL]];
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]count]>0) {
                            
                            complession([response objectForKey:@"data"][0]);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                    
                }
                else{
                    complession(nil);
                }
                
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }

}
-(void)getHowToRegisterWithComplession:(void (^)(id))complession{
    @try {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@detail.php?d=3",WEB_SERVICE_URL]];
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]count]>0) {
                            
                            complession([response objectForKey:@"data"][0]);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                    
                }
                else{
                    complession(nil);
                }
                
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
  
}




-(void)reverseGeocodeWithLatitude:(double)latitude Longitude:(double)longitude APIKEY:(NSString*)api_key Complession:(void (^)(id))complession{
    @try {

        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&key=%@",latitude,longitude,api_key]];
        
       
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            
            if (response) {
                if ([[response objectForKey:@"status"] isEqualToString:@"OK"]) {
                    if ([[response objectForKey:@"results"]count]>0) {
                        complession([[response objectForKey:@"results"][0]objectForKey:@"formatted_address"]);

                    }
                    else{
                        complession(nil);
  
                    }
                }
                else{
                    complession(nil);

                }
            }
            else{
                complession(nil);
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
}

-(void)getOrdersResponsesFor:(User *)user  With:(NSInteger)parentId Complession:(void (^)(id))complession{
    @try {
      
        //TODO:-
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@get_order_responses.php",WEB_SERVICE_URL]];
        
        
        NSDictionary*parameters =@{@"parent_id":[NSString stringWithFormat:@"%i",parentId]};
        [WebService performPostRequestWithURL:url Parameters:parameters Delegate:delegate Complession:^(id response) {
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"] count]>0) {
                            NSMutableArray *ordersList = [[NSMutableArray alloc]init];
                            for (id order in [response objectForKey:@"data"] ) {
//                                Order *addedOrder = [[Order alloc]init];
                                
                                SelectedDriver *driver = [[SelectedDriver alloc] init];
                                
                                driver.DriverID =[NSNull null]!=[order objectForKey:@"driver_id"]?[[order objectForKey:@"driver_id"]integerValue]:0;
                                driver.DriverName =[NSNull null]!=[order objectForKey:@"driver_name"]?[order objectForKey:@"driver_name"]:@"";
                                driver.CareColorName =[NSNull null]!=[order objectForKey:@"taxi_color"]?[order objectForKey:@"taxi_color"]:@"";

                                driver.Price =[NSNull null]!=[order objectForKey:@"rate"]?[order objectForKey:@"rate"]:@"";
                                driver.OrderID = [NSNumber numberWithInt:  [[order objectForKey:@"order_id"] intValue]];

                                [ordersList addObject:driver];
//                                addedOrder.fromAddress =[NSNull null]!=[order objectForKey:@"order_from_address"]?[order objectForKey:@"order_from_address"]:@"";
//                                addedOrder.toAddress =[NSNull null]!=[order objectForKey:@"order_to_address"]?[order objectForKey:@"order_to_address"]:@"";
//                                addedOrder.orderDate =[NSNull null]!=[order objectForKey:@"order_date"]?[order objectForKey:@"order_date"]:@"";
//                                addedOrder.price = [NSNull null]!=[order objectForKey:@"price"]?[[order objectForKey:@"price"]floatValue]:-1;
//                                addedOrder.orderID = [NSNull null]!=[order objectForKey:@"order_id"]?[[order objectForKey:@"order_id"]intValue]:-1;
//                                addedOrder.senderID =[NSNull null]!=[order objectForKey:@"sender_id"]?[[order objectForKey:@"sender_id"]intValue]:-1;
//                                addedOrder.receiverID =[NSNull null]!=[order objectForKey:@"reciever_id"]?[[order objectForKey:@"reciever_id"]intValue]:-1;
//                                addedOrder.message =[NSNull null]!=[order objectForKey:@"msg"]?[order objectForKey:@"msg"]:@"";
//                                addedOrder.subject =[NSNull null]!=[order objectForKey:@"subject"]?[order objectForKey:@"subject"]:@"";
//                                
//                                addedOrder.orderTime =[NSNull null]!=[order objectForKey:@"time"]?[order objectForKey:@"time"]:@"";
//                                addedOrder.receipientMobile =[NSNull null]!=[order objectForKey:@"recipient_phone"]?[order objectForKey:@"recipient_phone"]:@"";
//                                addedOrder.receipientName =[NSNull null]!=[order objectForKey:@"recipient_name"]?[order objectForKey:@"recipient_name"]:@"";
//                                addedOrder.isViewed =[NSNull null]!=[order objectForKey:@"is_view"]?[[order objectForKey:@"is_view"]intValue]==1?YES:NO:NO;
//                                double fromLatitude = [NSNull null]!=[order objectForKey:@"from_latitude"]?[[order objectForKey:@"from_latitude"]doubleValue]:0;
//                                double fromLongitude = [NSNull null]!=[order objectForKey:@"from_longitude"]?[[order objectForKey:@"from_longitude"]doubleValue]:0;
//                                double toLatitude = [NSNull null]!=[order objectForKey:@"to_latitude"]?[[order objectForKey:@"to_latitude"]doubleValue]:0;
//                                double toLongitude = [NSNull null]!=[order objectForKey:@"to_longitude"]?[[order objectForKey:@"to_longitude"]doubleValue]:0;
//                                
//                                Location *fromLocation = [[Location alloc]initWithLatitude:fromLatitude AndLongitude:fromLongitude] ;
//                                Location *toLocation =[[Location alloc]initWithLatitude:toLatitude AndLongitude:toLongitude] ;
//                                
//                                //                                addedOrder.fromLocation = fromLocation;
//                                //                                addedOrder.toLocation = toLocation;
//                                addedOrder.orderType = orderType;
//                                Worker *orderWorker = [[Worker alloc]init];
//                                orderWorker.userID =[NSNull null]!=[order objectForKey:@"user_id"]?[[order objectForKey:@"user_id"]intValue]:-1;
//                                orderWorker.fullName =[NSNull null]!=[order objectForKey:@"user_name"]?[order objectForKey:@"user_name"]:@"";
//                                orderWorker.mobile =[NSNull null]!=[order objectForKey:@"mobile"]?[order objectForKey:@"mobile"]:@"";
//                                orderWorker.place =[NSNull null]!=[order objectForKey:@"place"]?[order objectForKey:@"place"]:@"";
//                                orderWorker.photoFile =[NSNull null]!=[order objectForKey:@"member_image"]?[order objectForKey:@"member_image"]:@"";
//                                orderWorker.vehicleType =[NSNull null]!=[order objectForKey:@"vehicle_type"]?[[order objectForKey:@"vehicle_type"]intValue]:-1;
//                                addedOrder.worker = orderWorker;
//                                addedOrder.worker.userType = user.userType;
                                
                            }
                             NSString *fromAddress =[[[response objectForKey:@"data"] firstObject] valueForKey:@"from_address"] ;
                            NSString *toAddress =[[[response objectForKey:@"data"] firstObject] valueForKey:@"to_address"] ;
                             NSString *contactName =[[[response objectForKey:@"data"] firstObject] valueForKey:@"recipient_user"]  ;
                            NSNumber *orderID = [NSNumber numberWithInteger:  [[[response objectForKey:@"data"] firstObject] objectForKey:@"order_id"]]   ;
                            
                            NSDictionary *data = @{@"toAddress":toAddress,@"fromAddress":fromAddress,@"contactName":contactName,@"orderID":orderID,@"drivers":ordersList};
                            complession(data);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}

-(void)getProfitForUser:(User *)user ForDate:(NSDate *)date Complession:(void (^)(id))complession{
    @try {
        NSDateFormatter*dateFormatter = [[NSDateFormatter alloc]init];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];

        NSString*dateString =@"";
        if (date) {
            dateString = [dateFormatter stringFromDate:date];
        }
        user.userID = 2;
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@w_profits.php?member_id=%i&t=6&type=recieve&u_t=w&date=%@",WORKER_SERVICE_URL,user.userID,dateString]];
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    complession(response);
                }
                else{
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }
        }];

    }
    @catch (NSException *exception) {
        
    }
}
-(void)setAvailabilityForUser:(User*)user  Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@update_user_is_busy.php?member_id=%i",WEB_SERVICE_URL,user.userID]];

        if (user.userType ==WORKER) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@update_user_is_busy.php?member_id=%i",WORKER_SERVICE_URL,user.userID]];
        }
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]isEqualToString:@"success"]) {
                            complession(response);
                        }
                        else{
                            complession(nil);
                        }
                        
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession( @{@"message":[response objectForKey:@"data"]});
                }
            }
            else{
                complession(nil);
            }
        }];

    }
    @catch (NSException *exception) {
        
    }
}

-(void)loginWithUser:(User*)user  Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@login_ws.php",WEB_SERVICE_URL]];
        if (user.userType == WORKER) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@login_ws.php",WORKER_SERVICE_URL]];
        }
        NSDictionary*parameters =@{@"username":user.userName,@"password":user.password};
        [WebService performPostRequestWithURL:url Parameters:parameters Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([[response objectForKey:@"data"] count]>0) {
                        id result =[response objectForKey:@"data"][0];
                        Worker *responseUser = [[Worker alloc]init];
                        responseUser.userID = [result objectForKey:@"member_id"]?[[result objectForKey:@"member_id"]intValue]:0;
                        responseUser.userName = user.userName;
                        responseUser.password = user.password;
                        responseUser.fullName =[result objectForKey:@"f_name"]?[result objectForKey:@"f_name"]:@"";
                        responseUser.email =[result objectForKey:@"email"]?[result objectForKey:@"email"]:@"";
                        responseUser.mobile =[result objectForKey:@"mobile"]?[result objectForKey:@"mobile"]:@"";
                        responseUser.address =[result objectForKey:@"place"]?[result objectForKey:@"place"]:@"";
                        responseUser.licenceNumber =[result objectForKey:@"plate_number"]?[result objectForKey:@"plate_number"]:@"";
                         responseUser.vehicleColor =[result objectForKey:@"color"]?[result objectForKey:@"color"]:@"";
                        responseUser.vehicleType = [result objectForKey:@"taxi_type"]?[[result objectForKey:@"taxi_type"]intValue]:0;
                        responseUser.badVoteCount = [result objectForKey:@"bad_vot"]?[[result objectForKey:@"bad_vot"]intValue]:0;
                        responseUser.starsVoteCount = [result objectForKey:@"stars_vot"]?[[result objectForKey:@"stars_vot"]intValue]:0;
                        responseUser.isActive = [result objectForKey:@"is_busy"]?[[result objectForKey:@"is_busy"]intValue]==1?YES:NO:NO;
                        responseUser.photoFile =[result objectForKey:@"member_image"]?[result objectForKey:@"member_image"]:nil;

                        complession(responseUser);

                    }
                    else{
                        complession(nil);
 
                    }
                }
                else{
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }
        }];
    }
    @catch (NSException *exception) {
    }
}
-(void)registerUser:(User *)user Complession:(void (^)(id))complession{
    @try {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@register_ws.php",WEB_SERVICE_URL]];
        NSDictionary*parameters =@{@"username":user.userName,@"password":user.password,@"email":user.email,@"f_name":user.fullName,@"mobile":user.mobile};

        if (user.userType == WORKER) {
           url = [NSURL URLWithString:[NSString stringWithFormat:@"%@register_ws.php",WORKER_SERVICE_URL]];
          parameters =@{@"username":user.userName,@"password":user.password,@"email":user.email,@"f_name":user.fullName,@"mobile":user.mobile,@"place":user.address,@"taxi_type":[NSString stringWithFormat:@"%i",((Worker*)user).vehicleType],@"plate_number":((Worker*)user).licenceNumber,@"color":((Worker*)user).vehicleColor};

        }
        
        NSData *imageData = [CommonMethods compressImage:user.photo WithSize:CGSizeMake(80, 80)];
        NSArray *pathArr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                               NSUserDomainMask,
                                                               YES);
        NSString*documentPath = [pathArr objectAtIndex:0];
        NSString*filePath = [documentPath stringByAppendingString:@"/photo.png"];
       [imageData writeToFile:filePath atomically:YES];
        
        [WebService performPostRequestWithURL:url Parameters:parameters WithUploadFile:filePath  Complession:^(id response) {
            if (response) {
                [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    complession(response);
                }
                else{
                    complession(@{@"message":[response objectForKey:@"data"]});
                }
                
            }
            else{
                complession(nil);
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)getAboutApplicationWithComplession:(void (^)(id))complession{
    @try {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@detail.php?d=1",WEB_SERVICE_URL]];
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {

            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]count]>0) {
                            
                            complession([response objectForKey:@"data"][0]);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                    
                }
                else{
                    complession(nil);
                }
                
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}


-(void)getInformationForUserID:(int)userID Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@get_user_byid.php?u_id=%i",WEB_SERVICE_URL,userID]];
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]count]>0) {
                            id item = [response objectForKey:@"data"][0];
                            
                            User *userInformation = [[User alloc]init];
                            userInformation.userID = [item objectForKey:@"member_id"]?[[item objectForKey:@"member_id"]intValue ]:0;
                            userInformation.fullName = [item objectForKey:@"f_name"]?[item objectForKey:@"f_name"]:@"";
                            userInformation.userName = [item objectForKey:@"user_name"]?[item objectForKey:@"user_name"]:@"";
                            userInformation.email = [item objectForKey:@"email"]?[item objectForKey:@"email"]:@"";
                            userInformation.mobile = [item objectForKey:@"mobile"]?[item objectForKey:@"mobile"]:@"";
                            userInformation.isActive = [item objectForKey:@"is_active"]?[[item objectForKey:@"is_active"]intValue ]:0;
                            userInformation.userLevel = [item objectForKey:@"user_level"]?[[item objectForKey:@"user_level"]intValue ]:0;
                            userInformation.latitude = [item objectForKey:@"latitude"]?[[item objectForKey:@"latitude"]doubleValue ]:0;
                            userInformation.longitude = [item objectForKey:@"longitude"]?[[item objectForKey:@"longitude"]doubleValue ]:0;
                            userInformation.photoFile =[item objectForKey:@"member_image"]?[item objectForKey:@"member_image"]:nil;
                            
                            complession(userInformation);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                    
                }
                else{
                    complession(nil);
                    
                }
                
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)getOnlineWorkersForLatitude:(double)latitude Longitude:(double)longitude  Complession:(void(^)(id response) ) complession{

    @try {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@search_in_location_first.php?longitude=%f&latitude=%f",WEB_SERVICE_URL,longitude,latitude]];
    
    [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
        if (response) {
            if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                if ([response objectForKey:@"data"]) {
                    if ([[response objectForKey:@"data"]count]>0) {
                        NSMutableArray *WorkersList = [NSMutableArray new];
                        
                        for (id item in [response objectForKey:@"data"]) {
                            Worker *WorkerInformation = [[Worker alloc]init];
                            WorkerInformation.userID = [item objectForKey:@"member_id"]?[[item objectForKey:@"member_id"]intValue ]:0;
                            WorkerInformation.fullName = [item objectForKey:@"f_name"]?[item objectForKey:@"f_name"]:@"";
                            WorkerInformation.latitude = [item objectForKey:@"latitude"]?[[item objectForKey:@"latitude"]doubleValue ]:0;
                            WorkerInformation.longitude = [item objectForKey:@"longitude"]?[[item objectForKey:@"longitude"]doubleValue ]:0;
                         
                            
                            [WorkersList addObject:WorkerInformation];
                            
                        }
                        complession(WorkersList);
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
                
            }
            else{
                complession(nil);
                
            }
            
        }
        else{
            complession(nil);
        }
    }];
}
@catch (NSException *exception) {
    
}
}
-(void)sendRequestForomUser:(Worker *)user Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@search_in_location.php?longitude=%f&latitude=%f&member_id=%i",WEB_SERVICE_URL,user.longitude,user.latitude,user.userID]];
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]count]>0) {
                            NSMutableArray *WorkersList = [NSMutableArray new];
                            
                            for (id item in [response objectForKey:@"data"]) {
                                Worker *WorkerInformation = [[Worker alloc]init];
                                WorkerInformation.userID = [item objectForKey:@"member_id"]?[[item objectForKey:@"member_id"]intValue ]:0;
                                WorkerInformation.fullName = [item objectForKey:@"f_name"]?[item objectForKey:@"f_name"]:@"";
                                WorkerInformation.latitude = [item objectForKey:@"latitude"]?[[item objectForKey:@"latitude"]doubleValue ]:0;
                                WorkerInformation.longitude = [item objectForKey:@"longitude"]?[[item objectForKey:@"longitude"]doubleValue ]:0;
                                
                               
                                
                                [WorkersList addObject:WorkerInformation];
                                
                            }
                            complession(WorkersList);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                    
                }
                else{
                    complession(nil);
                    
                }
                
            }
            else{
                complession(nil);
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
}
- (void)getInformationForWorkerByID:(int)WorkerID Complession:(void (^)(id))complession
{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@get_user_byid.php?u_id=%i",WORKER_SERVICE_URL,WorkerID]];
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]count]>0) {
                            id item = [response objectForKey:@"data"][0];
                            
                            Worker *WorkerInformation = [[Worker alloc]init];
                            WorkerInformation.userID = [item objectForKey:@"member_id"]?[[item objectForKey:@"member_id"]intValue ]:0;
                            WorkerInformation.fullName = [item objectForKey:@"f_name"]?[item objectForKey:@"f_name"]:@"";
                            WorkerInformation.email = [item objectForKey:@"email"]?[item objectForKey:@"email"]:@"";
                            WorkerInformation.mobile = [item objectForKey:@"mobile"]?[item objectForKey:@"mobile"]:@"";
                            WorkerInformation.userName = [item objectForKey:@"user_name"]?[item objectForKey:@"user_name"]:@"";
                            WorkerInformation.latitude = [item objectForKey:@"latitude"]?[[item objectForKey:@"latitude"]doubleValue ]:0;
                            WorkerInformation.longitude = [item objectForKey:@"longitude"]?[[item objectForKey:@"longitude"]doubleValue ]:0;
                          
                            WorkerInformation.photoFile  =[item objectForKey:@"member_image"]?[item objectForKey:@"member_image"]:@"";
                            
                            complession(WorkerInformation);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                    
                }
                else{
                    complession(nil);
                    
                }
                
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }

}
-(void)confirmRequestFromUserUpdates:(NSInteger)userID ForOrder:(NSNumber*)orderID forWorker:(NSInteger)WorkerID Complession:(void(^)(id response) ) complession
{
    @try {
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@re_reply_search.php",WEB_SERVICE_URL]];
        
        
        NSDictionary*parameters =@{@"sender_id":[NSString stringWithFormat:@"%li",(long)userID],
                                   @"reciever_id":[NSString stringWithFormat:@"%li",(long)WorkerID],
                                   @"msg_id":[NSString stringWithFormat:@"%@",orderID],
                                   
                                   };
        [WebService performPostRequestWithURL:url Parameters:parameters Delegate:delegate Complession:^(id response) {
            
    
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]isEqualToString:@"success"]) {
                            complession(response);
                        }
                        else{
                            complession(nil);
                        }
                        
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession( @{@"message":[response objectForKey:@"data"]});
                }
            }
            else{
                complession(nil);
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
}

-(void)confirmRequestFromUser:(User *)user ForOrder:(Order*)order Complession:(void (^)(id))complession
{
    @try {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@re_reply_search.php?sender_id=%i&reciever_id=%i&msg_id=%i&lang=1",WEB_SERVICE_URL,user.userID,order.worker.userID,order.orderID]];
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]isEqualToString:@"success"]) {
                            complession(@{@"response":[response objectForKey:@"data"]});
                        }
                        else{
                            complession(nil);
                        }
                        
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession( @{@"message":[response objectForKey:@"data"]});
                }
            }
            else{
                complession(nil);
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
}


-(void)updateImageForUser:(User *)user Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload_mem_img.php",WEB_SERVICE_URL]];
        if (user.userType ==WORKER) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload_mem_img.php",WORKER_SERVICE_URL]];
        }
        NSData *imageData = [CommonMethods compressImage:user.photo WithSize:CGSizeMake(5, 5)];
        NSArray *pathArr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                               NSUserDomainMask,
                                                               YES);
        NSString*documentPath = [pathArr objectAtIndex:0];
        NSString*filePath = [documentPath stringByAppendingString:@"/photo.png"];
        
        [imageData writeToFile:filePath atomically:YES];
        NSDictionary*parameters =@{@"member_id":[NSString stringWithFormat:@"%i",user.userID]};
        
        [WebService performPostRequestWithURL:url Parameters:parameters WithUploadFile:filePath  Complession:^(id response) {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
            if (response) {
                
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"] count]>0) {
                            complession([[response objectForKey:@"data"] objectAtIndex:0]);
                        }
                        else{
                            complession(nil);
                        }
                        
                    }
                    else{
                        complession(nil);
                    }
                    
                }
                else{
                    complession(nil);
                }
                
            }
            else{
                complession(nil);
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)getFavouriteWorkersForUser:(User *)user Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@view_favo.php?member_id=%i",WEB_SERVICE_URL,user.userID]];
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]count]>0) {
                            NSMutableArray*WorkersList = [NSMutableArray new];
                            for (id item in [response objectForKey:@"data"]) {
                                Worker *addedWorker = [[Worker alloc]init];
                                addedWorker.userID = [item objectForKey:@"member_id"]?[[item objectForKey:@"member_id"]intValue ]:0;
                                addedWorker.photoFile = [item objectForKey:@"member_image"]?[item objectForKey:@"member_image"]:nil;
                                addedWorker.fullName = [item objectForKey:@"f_name"]?[item objectForKey:@"f_name"]:@"";
                                addedWorker.favouriteID = [item objectForKey:@"favorite_id"]?[[item objectForKey:@"favorite_id"]intValue ]:0;
                                [WorkersList addObject:addedWorker];
                            }
                            complession(WorkersList);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                    
                }
                else{
                    complession(nil);
                    
                }
                
            }
            else{
                complession(nil);
            }
        }];

    }
    @catch (NSException *exception) {
        
    }
}
-(void)deleteFromFavouriteWorkers:(Worker *)Worker Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@del_favo.php?favorite_id=%i",WEB_SERVICE_URL,Worker.favouriteID]];
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]isEqualToString:@"success"]) {
                            complession([NSNumber numberWithBool:YES]);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
                
            }
            else{
                complession(nil);
            }
        }];

    }
    @catch (NSException *exception) {
        
    }
}
-(void)addToFavouriteWorkers:(Worker *)Worker ForUser:(User *)user Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@addto_favo.php",WEB_SERVICE_URL]];
        NSDictionary*parameters =@{@"member_id":[NSString stringWithFormat:@"%i",user.userID],@"favorite_user":[NSString stringWithFormat:@"%i",Worker.userID]};
        [WebService performPostRequestWithURL:url Parameters:parameters Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        complession([response objectForKey:@"data"]);
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }
        }];
        

    }
    @catch (NSException *exception) {
        
    }
}
-(void)voteWorker:(Worker *)Worker ForUser:(User *)user IsLike:(BOOL)isLike Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@voting_member.php?member_id=%i&worker_id=%i&voting=%i",WEB_SERVICE_URL,user.userID,Worker.userID,isLike?1:0]];
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]isEqualToString:@"success"]) {
                            complession(@{@"response":[response objectForKey:@"data"]});
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                    
                }
                else{
                    
                    complession( @{@"message":[response objectForKey:@"data"]});
                }
            }
            else{
                complession(nil);
            }
        }];
        

    }
    @catch (NSException *exception) {
        
    }
}
-(void)cancelUserRequest:(User *)user ForOrder:(Order *)order WithCancelReasonID:(int)cancelReason Complession:(void (^)(id))complession{
    @try {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@re_reply_search_cancel.php?sender_id=%i&reciever_id=%i&msg_id=%i&refuse=%i&lang=1",WEB_SERVICE_URL,user.userID,order.worker.userID,order.orderID,cancelReason]];
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]isEqualToString:@"success"]) {
                            complession(@{@"response":[response objectForKey:@"data"]});
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession( @{@"message":[response objectForKey:@"data"]});
                }
            }
            else{
                complession(nil);
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)getPublicNotificationDetails:(int)message_id Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@view_msg_by_id.php?msg_id=%i",WEB_SERVICE_URL,message_id]];
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"] count]>0) {
                            id item = [response objectForKey:@"data"][0];
                            complession(item);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                    
                }
                else{
                    
                    complession( @{@"message":[response objectForKey:@"data"]});
                }
            }
            else{
                complession(nil);
            }
        }];
        

    }
    @catch (NSException *exception) {
        
    }
}
-(void)getCancellingReasonsWithComplession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@suggestion_massages.php",WEB_SERVICE_URL]];
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"] count]>0) {
                            complession([response objectForKey:@"data"]);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                    
                }
                else{
                    
                    complession( @{@"message":[response objectForKey:@"data"]});
                }
            }
            else{
                complession(nil);
            }
        }];
        

    }
    @catch (NSException *exception) {
        
    }
}



// Working Service

-(void)resetPasswordForUserType:(UserType )user_type WithEmail:(NSString *)email Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@reset_password.php?lang=0",WEB_SERVICE_URL]];
        if (user_type == WORKER) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@reset_password.php?lang=0",WEB_SERVICE_URL]];
        }
        NSDictionary*parameters =@{@"email":email};
        [WebService performPostRequestWithURL:url Parameters:parameters Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        complession([response objectForKey:@"data"]);
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession( @{@"message":[response objectForKey:@"data"]});
                }
            }
            else{
                complession(nil);
            }
        }];
        
        
    }
    @catch (NSException *exception) {
        
    }
}





-(void)ActionForInboxMail:(NSNumber*)orderId ForSender:(NSNumber*)senderId forReciver:(NSNumber*)recieverId withTye:(NSNumber*)type Complession:(void(^)(id response) ) complession
{
    @try {
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@re_reply_search_choose.php",WEB_SERVICE_URL]];
        
        
        NSDictionary*parameters =@{@"sender_id":[NSString stringWithFormat:@"%@",senderId],
                                   @"reciever_id":[NSString stringWithFormat:@"%@",recieverId],
                                   @"order_id":[NSString stringWithFormat:@"%@",orderId],
                                   @"type":[NSString stringWithFormat:@"%@",type]
                                   };
        [WebService performPostRequestWithURL:url Parameters:parameters Delegate:delegate Complession:^(id response) {
            
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"order"]) {
                        if ([[response objectForKey:@"message"]isEqualToString:@"success"]) {
                            complession(response);
                        }
                        else{
                            complession(nil);
                        }
                        
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession( @{@"message":[response objectForKey:@"data"]});
                }
            }
            else{
                complession(nil);
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
}




-(void)getOrderById:(NSNumber *)orderID Complession:(void (^)(id))complession{
        @try {
            NSString *orderTypeString =@"recieve";
           
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@view_msg_by_id.php?msg_id=%@&u_t=m&type=%@",WEB_SERVICE_URL,orderID,orderTypeString]];
            
         
            [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
                
                if (response) {
                    if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                        if ([response objectForKey:@"data"]) {
                            if ([[response objectForKey:@"data"] count]>0) {
                                NSMutableArray *ordersList = [[NSMutableArray alloc]init];
                                for (id currentOrder in [response objectForKey:@"data"] ) {
                                    Order *addedOrder = [[Order alloc]init];
                                    addedOrder.orderID = [currentOrder objectForKey:@"mail_id"] && [NSNull null]!=[currentOrder objectForKey:@"mail_id"]?[[currentOrder objectForKey:@"mail_id"]intValue]:-1;
                                    addedOrder.price = [currentOrder objectForKey:@"price_move"] && [NSNull null]!=[currentOrder objectForKey:@"price_move"]?[[currentOrder objectForKey:@"price_move"]floatValue]:0;
                                    addedOrder.senderID =[currentOrder objectForKey:@"sender_id"]&& [NSNull null]!=[currentOrder objectForKey:@"sender_id"]?[[currentOrder objectForKey:@"sender_id"]intValue]:-1;
                                    addedOrder.receiverID =[currentOrder objectForKey:@"reciever_id"]&& [NSNull null]!=[currentOrder objectForKey:@"reciever_id"]?[[currentOrder objectForKey:@"reciever_id"]intValue]:-1;
                                    addedOrder.message =[currentOrder objectForKey:@"from"]&& [NSNull null]!=[currentOrder objectForKey:@"msg"]?[currentOrder objectForKey:@"msg"]:@"";
                                    addedOrder.fromAddress =[currentOrder objectForKey:@"from"]&& [NSNull null]!=[currentOrder objectForKey:@"from"]?[currentOrder objectForKey:@"from"]:@"";
                                    addedOrder.toAddress =[currentOrder objectForKey:@"to"]&& [NSNull null]!=[currentOrder objectForKey:@"to"]?[currentOrder objectForKey:@"to"]:@"";
                                    addedOrder.subject =[currentOrder objectForKey:@"subject"]&&[NSNull null]!=[currentOrder objectForKey:@"subject"]?[currentOrder objectForKey:@"subject"]:@"";
                                    addedOrder.orderDate =[currentOrder objectForKey:@"date"]&&[NSNull null]!=[currentOrder objectForKey:@"date"]?[currentOrder objectForKey:@"date"]:@"";
                                    addedOrder.orderTime =[currentOrder objectForKey:@"time"]&&[NSNull null]!=[currentOrder objectForKey:@"time"]?[currentOrder objectForKey:@"time"]:@"";
                                    addedOrder.receipientMobile =[currentOrder objectForKey:@"recipient_tel"]&&[NSNull null]!=[currentOrder objectForKey:@"recipient_tel"]?[currentOrder objectForKey:@"recipient_tel"]:@"";
                                    addedOrder.receipientName =[currentOrder objectForKey:@"recipient_user"]&& [NSNull null]!=[currentOrder objectForKey:@"recipient_user"]?[currentOrder objectForKey:@"recipient_user"]:@"";
                                    addedOrder.isViewed =[currentOrder objectForKey:@"is_view"]&&[NSNull null]!=[currentOrder objectForKey:@"is_view"]?[[currentOrder objectForKey:@"is_view"]intValue]==1?YES:NO:NO;
                                    double fromLatitude = [currentOrder objectForKey:@"from_latitude"]&&[NSNull null]!=[currentOrder objectForKey:@"from_latitude"]?[[currentOrder objectForKey:@"from_latitude"]doubleValue]:0;
                                    double fromLongitude = [currentOrder objectForKey:@"from_longitude"]&&[NSNull null]!=[currentOrder objectForKey:@"from_longitude"]?[[currentOrder objectForKey:@"from_longitude"]doubleValue]:0;
                                    double toLatitude = [currentOrder objectForKey:@"to_latitude"]&&[NSNull  null]!=[currentOrder objectForKey:@"to_latitude"]?[[currentOrder objectForKey:@"to_latitude"]doubleValue]:0;
                                    double toLongitude = [currentOrder objectForKey:@"to_longitude"]&&[NSNull null]!=[currentOrder objectForKey:@"to_longitude"]?[[currentOrder objectForKey:@"to_longitude"]doubleValue]:0;
                                    
                                    addedOrder.fromLocation = [[CLLocation alloc] initWithLatitude: fromLatitude  longitude:fromLongitude];;
                                    addedOrder.toLocation = [[CLLocation alloc] initWithLatitude: toLatitude  longitude:toLongitude];;//                                    addedOrder.orderType = OPEN;
                                    Worker *orderWorker = [[Worker alloc]init];
                                    orderWorker.userID =[currentOrder objectForKey:@"member_id"]&&[NSNull null]!=[currentOrder objectForKey:@"member_id"]?[[currentOrder objectForKey:@"member_id"]intValue]:-1;
                                    orderWorker.fullName =[currentOrder objectForKey:@"f_name"]&&[NSNull null]!=[currentOrder objectForKey:@"f_name"]?[currentOrder objectForKey:@"f_name"]:@"";
                                    orderWorker.mobile =[currentOrder objectForKey:@"mobile"]&&[NSNull null]!=[currentOrder objectForKey:@"mobile"]?[currentOrder objectForKey:@"mobile"]:@"";
                                    orderWorker.place =[currentOrder objectForKey:@"place"]&&[NSNull null]!=[currentOrder objectForKey:@"place"]?[currentOrder objectForKey:@"place"]:@"";
                                    orderWorker.photoFile =[currentOrder objectForKey:@"member_image"]&&[NSNull null]!=[currentOrder objectForKey:@"member_image"]?[currentOrder objectForKey:@"member_image"]:@"";
                                    orderWorker.vehicleType =[currentOrder objectForKey:@"vehicle_type"]&&[NSNull null]!=[currentOrder objectForKey:@"vehicle_type"]?[[currentOrder objectForKey:@"vehicle_type"]intValue]:-1;
                                    addedOrder.worker = orderWorker;
                                    [ordersList addObject:addedOrder];
                                }
                                complession(ordersList);
                            }
                            else{
                                complession(nil);
                            }
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
            }];
            
        }
        @catch (NSException *exception) {
            
        }
    }
    




//Main View Controller

-(void)updateLocationForUser:(Worker *)user  Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@update_user_place1.php?member_id=%i&longitude=%f&latitude=%f",WEB_SERVICE_URL,user.userID,user.longitude,user.latitude]];
        if (user.userType ==WORKER) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@update_user_place1.php?member_id=%i&longitude=%f&latitude=%f",WORKER_SERVICE_URL,user.userID,user.longitude,user.latitude]];
        }
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]count]>0) {
                            complession([response objectForKey:@"data"][0]);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                    
                }
                else{
                    complession(nil);
                    
                }
                
            }
            else{
                complession(nil);
            }
        }];
    }
    @catch (NSException *exception) {
        
    }
}




-(void)getAvalibleDriverinRangeForUserID:(NSNumber*)userID ForLocation:(CLLocation *)currentLocation forCarType:(NSNumber *)type Complession:(void (^)(id))complession{
   
    @try {
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@get_close_drivers.php",WEB_SERVICE_URL]];
        
        
        NSDictionary*parameters =@{@"client_id":[NSString stringWithFormat:@"%@",userID],
                                   @"client_longitude":[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude],
                                   @"client_latitude":[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude],
                                   @"vehicle_type":[NSString stringWithFormat:@"%@",type],

                                   
                                   };
        
        
//        NSDictionary*parameters =@{@"client_id":@"340",
//                                   @"client_longitude":@"29.926200",
//                                   @"client_latitude":@"31.193236",
//                                   @"vehicle_type":@"1",
//                                   
//                                   
//                                   };
        [WebService performPostRequestWithURL:url Parameters:parameters Delegate:delegate Complession:^(id response) {
            
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"] count]>0) {
                            
                            complession([response objectForKey:@"data"]);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }

}



//OrderView Controller

-(void)sendOrder:(Order *)order ForUser:(User *)user Complession :(void (^)(id))complession{
    @try {
        
        
        
        //,WEB_SERVICE_URL,user.longitude,user.latitude,user.userID,order.fromLocation.coordinate.longitude,order.fromLocation.coordinate.latitude,order.toLocation.coordinate.longitude,order.toLocation.coordinate.latitude,order.worker.vehicleType,order.distance,order.message,order.receipientMobile,order.receipientName]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@new_order.php",WEB_SERVICE_URL]];
        
        if (user.userType == WORKER) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@new_order.php",WEB_SERVICE_URL]];
        }
        
        NSDictionary*parameters =@{
                                   
                                   
                                   @"from_longitude":[NSString stringWithFormat:@"%f",order.fromLocation.coordinate.longitude],
                                   @"from_latitude":[NSString stringWithFormat:@"%f",order.fromLocation.coordinate.latitude],
                                   @"from_address":[NSString stringWithFormat:@"%@",order.fromAddress],
                                   @"to_longitude":[NSString stringWithFormat:@"%f",order.toLocation.coordinate.longitude],
                                   @"to_latitude":[NSString stringWithFormat:@"%f",order.toLocation.coordinate.latitude],
                                   @"to_address":[NSString stringWithFormat:@"%@",order.toAddress],
                                   @"rate":[NSString stringWithFormat:@"%f",order.price],
                                   @"price_move":[NSString stringWithFormat:@"%f",order.price],
                                   @"receiver_name":[NSString stringWithFormat:@"%@",order.receipientName],
                                   @"receiver_phone":[NSString stringWithFormat:@"%@",order.receipientMobile],
                                   @"client_id":[NSString stringWithFormat:@"%i",user.userID],
                                   @"client_longitude":[NSString stringWithFormat:@"%f",order.fromLocation.coordinate.longitude],
                                   @"client_latitude":[NSString stringWithFormat:@"%f",order.fromLocation.coordinate.latitude],
                                   @"vehicle_type":[NSString stringWithFormat:@"%i",1],
                                   @"description":order.message
                                   //                                                                       @"radius":[NSString stringWithFormat:@"%i",10
                                   //                                                                                  ],
                                   };
        [WebService performPostRequestWithURL:url Parameters:parameters Delegate:delegate Complession:^(id response) {
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"track_order_data"]) {
                        if ([[response objectForKey:@"track_order_data"] count]>0) {
                            complession(response);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}


// My Orders View Controller
-(void)getOrdersForUser:(User *)user  WithOrderType:(OrderType)orderType Complession:(void (^)(id))complession{
    @try {
        NSInteger orderTypeString = 1;
        if (orderType == OPEN) {
            orderTypeString = 2;
        }else if (orderType == EXECUTED){
            orderTypeString =6;
        }else if (orderType == CANCELED){
            orderTypeString = 3;
            
        }
        //TODO:-
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@get_user_orders.php",WEB_SERVICE_URL]];
        
        if (user.userType == WORKER) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@get_user_orders.php",WEB_SERVICE_URL]];
        }
        
        NSDictionary*parameters =@{@"member_id":[NSString stringWithFormat:@"%i",user.userID],@"user_type":@"m",@"order_type":[NSString stringWithFormat:@"%i",orderTypeString]};
        [WebService performPostRequestWithURL:url Parameters:parameters Delegate:delegate Complession:^(id response) {
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"] count]>0) {
                            NSMutableArray *ordersList = [[NSMutableArray alloc]init];
                            for (id order in [response objectForKey:@"data"] ) {
                                Order *addedOrder = [[Order alloc]init];
                                
                                
                                addedOrder.fromAddress =[NSNull null]!=[order objectForKey:@"order_from_address"]?[order objectForKey:@"order_from_address"]:@"";
                                addedOrder.toAddress =[NSNull null]!=[order objectForKey:@"order_to_address"]?[order objectForKey:@"order_to_address"]:@"";
                                addedOrder.orderDate =[NSNull null]!=[order objectForKey:@"order_date"]?[order objectForKey:@"order_date"]:@"";
                                addedOrder.price = [NSNull null]!=[order objectForKey:@"price"]?[[order objectForKey:@"price"]floatValue]:-1;
                                addedOrder.orderID = [NSNull null]!=[order objectForKey:@"order_id"]?[[order objectForKey:@"order_id"]intValue]:-1;
                                addedOrder.senderID =[NSNull null]!=[order objectForKey:@"sender_id"]?[[order objectForKey:@"sender_id"]intValue]:-1;
                                addedOrder.receiverID =[NSNull null]!=[order objectForKey:@"reciever_id"]?[[order objectForKey:@"reciever_id"]intValue]:-1;
                                addedOrder.message =[NSNull null]!=[order objectForKey:@"msg"]?[order objectForKey:@"msg"]:@"";
                                addedOrder.subject =[NSNull null]!=[order objectForKey:@"subject"]?[order objectForKey:@"subject"]:@"";
                   
                                addedOrder.driverName =[NSNull null]!=[order objectForKey:@"driver_name"]?[order objectForKey:@"driver_name"]:@"";
                                addedOrder.driverMobile =[NSNull null]!=[order objectForKey:@"driver_mobile"]?[order objectForKey:@"driver_mobile"]:@"";
                                addedOrder.driverRate =[NSNull null]!= [order objectForKey:@"driver_rate"]?[[order objectForKey:@"driver_rate"]doubleValue]:0;                                 
                                addedOrder.orderTime =[NSNull null]!=[order objectForKey:@"time"]?[order objectForKey:@"time"]:@"";
                                addedOrder.receipientMobile =[NSNull null]!=[order objectForKey:@"recipient_phone"]?[order objectForKey:@"recipient_phone"]:@"";
                                addedOrder.receipientName =[NSNull null]!=[order objectForKey:@"recipient_name"]?[order objectForKey:@"recipient_name"]:@"";
                                addedOrder.isViewed =[NSNull null]!=[order objectForKey:@"is_view"]?[[order objectForKey:@"is_view"]intValue]==1?YES:NO:NO;
                                double fromLatitude = [NSNull null]!=[order objectForKey:@"from_latitude"]?[[order objectForKey:@"from_latitude"]doubleValue]:0;
                                double fromLongitude = [NSNull null]!=[order objectForKey:@"from_longitude"]?[[order objectForKey:@"from_longitude"]doubleValue]:0;
                                double toLatitude = [NSNull null]!=[order objectForKey:@"to_latitude"]?[[order objectForKey:@"to_latitude"]doubleValue]:0;
                                double toLongitude = [NSNull null]!=[order objectForKey:@"to_longitude"]?[[order objectForKey:@"to_longitude"]doubleValue]:0;
                                
                                
                                CLLocation *fromLocation = [[CLLocation alloc] initWithLatitude:fromLatitude longitude:fromLongitude];
                                CLLocation *toLocation = [[CLLocation alloc] initWithLatitude:toLatitude longitude:toLongitude];
                                addedOrder.fromLocation = fromLocation;
                                addedOrder.toLocation = toLocation;
                                addedOrder.orderType = orderType;
                                Worker *orderWorker = [[Worker alloc]init];
                                orderWorker.userID =[NSNull null]!=[order objectForKey:@"user_id"]?[[order objectForKey:@"user_id"]intValue]:-1;
                                orderWorker.fullName =[NSNull null]!=[order objectForKey:@"user_name"]?[order objectForKey:@"user_name"]:@"";
                                orderWorker.mobile =[NSNull null]!=[order objectForKey:@"mobile"]?[order objectForKey:@"mobile"]:@"";
                                orderWorker.place =[NSNull null]!=[order objectForKey:@"place"]?[order objectForKey:@"place"]:@"";
                                orderWorker.photoFile =[NSNull null]!=[order objectForKey:@"member_image"]?[order objectForKey:@"member_image"]:@"";
                                orderWorker.vehicleType =[NSNull null]!=[order objectForKey:@"vehicle_type"]?[[order objectForKey:@"vehicle_type"]intValue]:-1;
                                orderWorker.licenceNumber =[NSNull null]!=[order objectForKey:@"licence_number"]?[order objectForKey:@"licence_number"]:@"";

                                addedOrder.worker = orderWorker;
                                addedOrder.worker.userType = user.userType;
                                [ordersList addObject:addedOrder];
                            }
                            complession(ordersList);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}
//Order Details View Controller

-(void)getDetailsForOrder:(Order *)order Complession:(void (^)(id))complession{
    @try {
        NSString *orderTypeString =@"recieve";
        if (order.orderType == SEND) {
            orderTypeString =@"send";
        }
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@view_msg_by_id.php?msg_id=%i&u_t=m&type=%@",WEB_SERVICE_URL,order.orderID,orderTypeString]];
        
        if (order.worker.userType == WORKER) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@view_msg_by_id.php?msg_id=%i&u_t=w&type=%@",WEB_SERVICE_URL,order.orderID,orderTypeString]];
        }
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"] count]>0) {
                            NSMutableArray *ordersList = [[NSMutableArray alloc]init];
                            for (id currentOrder in [response objectForKey:@"data"] ) {
                                Order *addedOrder = [[Order alloc]init];
                                addedOrder.orderID = [currentOrder objectForKey:@"mail_id"] && [NSNull null]!=[currentOrder objectForKey:@"mail_id"]?[[currentOrder objectForKey:@"mail_id"]intValue]:-1;
                                addedOrder.price = [currentOrder objectForKey:@"price_move"] && [NSNull null]!=[currentOrder objectForKey:@"price_move"]?[[currentOrder objectForKey:@"price_move"]floatValue]:0;
                                addedOrder.senderID =[currentOrder objectForKey:@"sender_id"]&& [NSNull null]!=[currentOrder objectForKey:@"sender_id"]?[[currentOrder objectForKey:@"sender_id"]intValue]:-1;
                                addedOrder.receiverID =[currentOrder objectForKey:@"reciever_id"]&& [NSNull null]!=[currentOrder objectForKey:@"reciever_id"]?[[currentOrder objectForKey:@"reciever_id"]intValue]:-1;
                                addedOrder.message =[currentOrder objectForKey:@"from"]&& [NSNull null]!=[currentOrder objectForKey:@"msg"]?[currentOrder objectForKey:@"msg"]:@"";
                                addedOrder.fromAddress =[currentOrder objectForKey:@"from"]&& [NSNull null]!=[currentOrder objectForKey:@"from"]?[currentOrder objectForKey:@"from"]:@"";
                                addedOrder.toAddress =[currentOrder objectForKey:@"to"]&& [NSNull null]!=[currentOrder objectForKey:@"to"]?[currentOrder objectForKey:@"to"]:@"";
                                addedOrder.subject =[currentOrder objectForKey:@"subject"]&&[NSNull null]!=[currentOrder objectForKey:@"subject"]?[currentOrder objectForKey:@"subject"]:@"";
                                addedOrder.orderDate =[currentOrder objectForKey:@"date"]&&[NSNull null]!=[currentOrder objectForKey:@"date"]?[currentOrder objectForKey:@"date"]:@"";
                                addedOrder.orderTime =[currentOrder objectForKey:@"time"]&&[NSNull null]!=[currentOrder objectForKey:@"time"]?[currentOrder objectForKey:@"time"]:@"";
                                addedOrder.receipientMobile =[currentOrder objectForKey:@"recipient_tel"]&&[NSNull null]!=[currentOrder objectForKey:@"recipient_tel"]?[currentOrder objectForKey:@"recipient_tel"]:@"";
                                addedOrder.receipientName =[currentOrder objectForKey:@"recipient_user"]&& [NSNull null]!=[currentOrder objectForKey:@"recipient_user"]?[currentOrder objectForKey:@"recipient_user"]:@"";
                                addedOrder.isViewed =[currentOrder objectForKey:@"is_view"]&&[NSNull null]!=[currentOrder objectForKey:@"is_view"]?[[currentOrder objectForKey:@"is_view"]intValue]==1?YES:NO:NO;
                                double fromLatitude = [currentOrder objectForKey:@"from_latitude"]&&[NSNull null]!=[currentOrder objectForKey:@"from_latitude"]?[[currentOrder objectForKey:@"from_latitude"]doubleValue]:0;
                                double fromLongitude = [currentOrder objectForKey:@"from_longitude"]&&[NSNull null]!=[currentOrder objectForKey:@"from_longitude"]?[[currentOrder objectForKey:@"from_longitude"]doubleValue]:0;
                                double toLatitude = [currentOrder objectForKey:@"to_latitude"]&&[NSNull  null]!=[currentOrder objectForKey:@"to_latitude"]?[[currentOrder objectForKey:@"to_latitude"]doubleValue]:0;
                                double toLongitude = [currentOrder objectForKey:@"to_longitude"]&&[NSNull null]!=[currentOrder objectForKey:@"to_longitude"]?[[currentOrder objectForKey:@"to_longitude"]doubleValue]:0;
                                
                                CLLocation * fromLocation=[[CLLocation alloc]initWithLatitude:fromLatitude longitude:fromLongitude];
                                
                                
                                CLLocation * toLocation=[[CLLocation alloc]initWithLatitude:toLatitude longitude:toLongitude];
                                
                                
                                addedOrder.fromLocation = fromLocation;
                                addedOrder.toLocation = toLocation;
                                addedOrder.orderType = order.orderType;
                                Worker *orderWorker = [[Worker alloc]init];
                                orderWorker.userID =[currentOrder objectForKey:@"member_id"]&&[NSNull null]!=[currentOrder objectForKey:@"member_id"]?[[currentOrder objectForKey:@"member_id"]intValue]:-1;
                                orderWorker.fullName =[currentOrder objectForKey:@"f_name"]&&[NSNull null]!=[currentOrder objectForKey:@"f_name"]?[currentOrder objectForKey:@"f_name"]:@"";
                                orderWorker.mobile =[currentOrder objectForKey:@"mobile"]&&[NSNull null]!=[currentOrder objectForKey:@"mobile"]?[currentOrder objectForKey:@"mobile"]:@"";
                                orderWorker.place =[currentOrder objectForKey:@"place"]&&[NSNull null]!=[currentOrder objectForKey:@"place"]?[currentOrder objectForKey:@"place"]:@"";
                                orderWorker.photoFile =[currentOrder objectForKey:@"member_image"]&&[NSNull null]!=[currentOrder objectForKey:@"member_image"]?[currentOrder objectForKey:@"member_image"]:@"";
                                orderWorker.vehicleType =[currentOrder objectForKey:@"vehicle_type"]&&[NSNull null]!=[currentOrder objectForKey:@"vehicle_type"]?[[currentOrder objectForKey:@"vehicle_type"]intValue]:-1;
                                addedOrder.worker = orderWorker;
                                [ordersList addObject:addedOrder];
                            }
                            complession(ordersList);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}

-(void)getDriverRateForDriverId:(NSNumber *)DriverId Complession:(void (^)(id))complession{
    @try {
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@get_driver_rate.php",WEB_SERVICE_URL]];
        NSDictionary*parameters =@{@"reciever_id":[NSString stringWithFormat:@"%@",DriverId]
                                   
                                   };
        [WebService performPostRequestWithURL:url Parameters:parameters Delegate:delegate Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"] count]>0) {
                            
                            complession([response objectForKey:@"data"]);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}

-(void)acceptOrder:(Order *)order ForWorker:(Worker *)worker WithPrice:(double)price Complession:(void (^)(id))complession
{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@reply_search1.php?lang=1",WORKER_SERVICE_URL]];
        NSDictionary*parameters =@{@"sender_id":[NSString stringWithFormat:@"%i",worker.userID],@"reciever_id":[NSString stringWithFormat:@"%i",order.senderID] ,@"msg_id":[NSString stringWithFormat:@"%i",order.orderID],@"price_move":[NSString stringWithFormat:@"%f",price]};
        
        [WebService performPostRequestWithURL:url Parameters:parameters Delegate:delegate Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]isEqualToString:@"success"]) {
                            complession(response);
                        }
                        else{
                            complession(nil);
                        }
                        
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession( @{@"message":[response objectForKey:@"data"]});
                }
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}


//Follow Mandobi
-(void)finishSubmittingForOrder:(Order *)order Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@finish_process.php?msg_id=%i&lang=1",WEB_SERVICE_URL,order.orderID]];
        
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"success"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]isEqualToString:@"Successfully Sent Order"]) {
                            complession(response);
                        }
                        else{
                            complession(nil);
                        }
                        
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession( @{@"message":[response objectForKey:@"data"]});
                }
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}




-(void)getLocationForMandoob:(Worker*)user Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@get_user_lat_long.php?u_id=%i",WORKER_SERVICE_URL,user.userID]];
        
        
        [WebService performGetRequestWithURL:url Delegate:delegate  Complession:^(id response) {
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]count]>0) {
                            user.latitude =[response objectForKey:@"data"][0][@"latitude"]?[[response objectForKey:@"data"][0][@"latitude"]doubleValue]:0;
                            user.longitude =[response objectForKey:@"data"][0][@"longitude"]?[[response objectForKey:@"data"][0][@"longitude"]doubleValue]:0;
                            
                            complession(user);
                        }
                        else{
                            complession(nil);
                        }
                        
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession( @{@"message":[response objectForKey:@"data"]});
                }
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}

//Rate View Controller
-(void)rateandCommitWithType:(NSNumber *)Type WithOrder:(NSNumber *)OrderId WithSender:(NSNumber *)SenderId  WithRate:(NSNumber *)Rate Withcomment:(NSString *)note Complession:(void (^)(id))complession{
    @try {
        
        //http://mandobiapp.com/c_m_s/update_inbox.php?type=6&order_id=8&sender_id=365&rate=5&note=very+good
        //        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@update_inbox.php?type=%@&order_id=%@&sender_id=%@&rate=%@&note=%@",WEB_SERVICE_URL,Type,OrderId,SenderId,Rate,note]];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@update_inbox.php",WEB_SERVICE_URL]];
        
        
        NSDictionary*parameters =@{@"type":[NSString stringWithFormat:@"%@",Type],
                                   @"order_id":[NSString stringWithFormat:@"%@",OrderId],
                                   @"sender_id":[NSString stringWithFormat:@"%@",SenderId],
                                   @"rate":[NSString stringWithFormat:@"%@",Rate],
                                   @"note":[NSString stringWithFormat:@"%@",note]
                                   };
        [WebService performPostRequestWithURL:url Parameters:parameters Delegate:delegate Complession:^(id response) {
            
            
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"] count]>0) {
                            
                            complession([response objectForKey:@"data"]);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}



//Settings

-(void)logOutForUser:(User *)user Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@logout_update.php",WEB_SERVICE_URL]];
        if (user.userType == WORKER) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@logout_update.php",WORKER_SERVICE_URL]];
        }
        NSDictionary*parameters =@{@"member_id":[NSString stringWithFormat:@"%i",user.userID]};
        
        [WebService performPostRequestWithURL:url Parameters:parameters Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        complession([response objectForKey:@"data"]);
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }
        }];
        
        
    }
    @catch (NSException *exception) {
        
    }
}

-(void)updateProfileForUser:(User *)user Complession:(void (^)(id))complession{
    @try {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@update_profile.php",WEB_SERVICE_URL]];
        NSDictionary*parameters =@{@"username":user.userName,@"password":user.password,@"email":user.email,@"f_name":user.fullName,@"mobile":user.mobile,@"member_id":[NSString stringWithFormat:@"%i",user.userID]};
        if (user.userType == WORKER) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@update_profile.php",WORKER_SERVICE_URL]];
            parameters =@{@"username":user.userName,@"password":user.password,@"email":user.email,@"f_name":user.fullName,@"mobile":user.mobile,@"member_id":[NSString stringWithFormat:@"%i",user.userID],@"taxi_type":[NSString stringWithFormat:@"%i",((Worker*)user).vehicleType],@"plate_number":((Worker*)user).licenceNumber,@"color":((Worker*)user).vehicleColor,@"place":((Worker*)user).address};
        }
        
        [WebService performPostRequestWithURL:url   Parameters:parameters Delegate:delegate Complession:^(id response) {
            if (response) {
                
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    
                    complession(response);
                }
                else{
                    complession(@{@"message":[response objectForKey:@"data"]});
                }
                
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}


-(void)NewOrderAsyncTask:(NSDictionary *)order Complession :(void (^)(id))complession{
    @try {
        
        
        
        //,WEB_SERVICE_URL,user.longitude,user.latitude,user.userID,order.fromLocation.coordinate.longitude,order.fromLocation.coordinate.latitude,order.toLocation.coordinate.longitude,order.toLocation.coordinate.latitude,order.worker.vehicleType,order.distance,order.message,order.receipientMobile,order.receipientName]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@track_order_for_client.php",WEB_SERVICE_URL]];
        
        //        NSDictionary*parameters =@{
        //                                   @"from_longitude":[NSString stringWithFormat:@"%f",order.fromLocation.coordinate.longitude],
        //                                   @"from_latitude":[NSString stringWithFormat:@"%f",order.fromLocation.coordinate.latitude],
        //                                   @"from_address":[NSString stringWithFormat:@"%@",order.fromAddress],
        //                                   @"to_longitude":[NSString stringWithFormat:@"%f",order.toLocation.coordinate.longitude],
        //                                   @"to_latitude":[NSString stringWithFormat:@"%f",order.toLocation.coordinate.latitude],
        //                                   @"to_address":[NSString stringWithFormat:@"%@",order.toAddress],
        //                                   @"rate":[NSString stringWithFormat:@"%f",order.price],
        //                                   @"receiver_name":[NSString stringWithFormat:@"%@",order.receipientName],
        //                                   @"receiver_phone":[NSString stringWithFormat:@"%@",order.receipientMobile],
        //                                   @"client_id":[NSString stringWithFormat:@"%i",user.userID],
        //                                   @"client_longitude":[NSString stringWithFormat:@"%f",user.latitude],
        //                                   @"client_latitude":[NSString stringWithFormat:@"%f",user.longitude],
        //                                   @"vehicle_type":[NSString stringWithFormat:@"%i",1],
        //                                   @"description":@"m",
        //                                   @"radius":[NSString stringWithFormat:@"%i",10],
        //                                   };
        [WebService performPostJSONRequestWithURL:url Parameters:order Delegate:delegate Complession:^(id response) {
            
            if (response) {
                
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"] count]>0) {
                            complession([response objectForKey:@"data"]);
                        }
                        else{
                            complession(nil);
                        }
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }
        }];
        
    }
    @catch (NSException *exception) {
        
    }
}


-(void)sendNotificationToken:(NSString *)token ForWorker:(Worker *)user Complession:(void (^)(id))complession{
    @try {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@insert_ios_token.php",WEB_SERVICE_URL]];
        if (user.userType == WORKER) {
            url = [NSURL URLWithString:[NSString stringWithFormat:@"%@insert_ios_token.php",WORKER_SERVICE_URL]];
        }
        NSDictionary*parameters =@{@"ios_token":token,@"member_id":[NSString stringWithFormat:@"%i",user.userID]};
        [WebService performPostRequestWithURL:url Parameters:parameters Delegate:delegate  Complession:^(id response) {
            if (response) {
                if ([[response objectForKey:@"result"] isEqualToString:@"true"]) {
                    if ([response objectForKey:@"data"]) {
                        if ([[response objectForKey:@"data"]isEqualToString:@"success"]) {
                            complession(@{@"response":[response objectForKey:@"data"]});
                        }
                        else{
                            complession(nil);
                        }
                        
                    }
                    else{
                        complession(nil);
                    }
                }
                else{
                    complession( @{@"message":[response objectForKey:@"data"]});
                }
            }
            else{
                complession(nil);
            }
        }];
    }
    
    @catch (NSException *exception) {
        
    }
    
}




@end
