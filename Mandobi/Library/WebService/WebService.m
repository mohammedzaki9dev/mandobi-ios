//
//  WebService.m
//  Shauffeur Taxi
//
//  Created by Assem Imam on 12/2/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#import "WebService.h"
#import "PKMultipartInputStream.h"

@implementation WebService
+ (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
    }
    
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}

+(void)performGetRequestWithURL:(NSURL*)url Delegate:(id<NSURLSessionDelegate>)delegate Complession:(void(^)(id response) )complession{
    @try {
        NSURLRequest *serviceRequest = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_INTERVAL];
        NSURLSessionConfiguration * sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.timeoutIntervalForRequest = TIME_OUT_INTERVAL;
        sessionConfig.timeoutIntervalForResource = TIME_OUT_INTERVAL;

        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:delegate delegateQueue:nil];
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:serviceRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error && ((NSHTTPURLResponse*)response).statusCode == 200 ){
                if (data){
                    NSString*retString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                    NSData *_data = [retString dataUsingEncoding:NSUTF8StringEncoding];

                    id result= [NSJSONSerialization JSONObjectWithData:_data options:0 error:nil] ;
                    complession(result);
                }
            }
            else{
                complession(nil);
            }
            
        }];
        [task resume];
    }
    @catch (NSException *exception) {
        
    }
}
+(void)performPostRequestWithURL:(NSURL*)url Parameters:(NSDictionary*)parameters  Delegate:(id<NSURLSessionDelegate>)delegate Complession:(void(^)(id response) )complession{
//    @try {
        NSMutableURLRequest *serviceRequest = [[NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_INTERVAL]mutableCopy];
        [serviceRequest setHTTPMethod:@"POST"];
//        for (NSString*key in parameters.allKeys) {
//            [serviceRequest setValue:[parameters objectForKey:key] forHTTPHeaderField:key];
//        }
     
        NSData *requestData = [self encodeDictionary:parameters];
       
        [serviceRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [serviceRequest setHTTPBody: requestData];
        
        [serviceRequest setValue:REQUEST_CONTENT_TYPE forHTTPHeaderField:@"Content-Type"];
        NSURLSessionConfiguration * sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.timeoutIntervalForRequest = TIME_OUT_INTERVAL;
        sessionConfig.timeoutIntervalForResource = TIME_OUT_INTERVAL;

        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:delegate delegateQueue:nil];

        NSURLSessionDataTask *task = [session dataTaskWithRequest:serviceRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (!error && ((NSHTTPURLResponse*)response).statusCode == 200 ){
                if (data){
                    id result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
                    complession(result);
                }
            }
            else{
                complession(nil);
            }
            
        }];
        [task resume];
//    }
//    @catch (NSException *exception) {
//        
//    }

}




+(void)performPostJSONRequestWithURL:(NSURL*)url Parameters:(NSDictionary*)parameters  Delegate:(id<NSURLSessionDelegate>)delegate Complession:(void(^)(id response) )complession{
    //    @try {
    NSMutableURLRequest *serviceRequest = [[NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_INTERVAL]mutableCopy];
    [serviceRequest setHTTPMethod:@"POST"];
    //        for (NSString*key in parameters.allKeys) {
    //            [serviceRequest setValue:[parameters objectForKey:key] forHTTPHeaderField:key];
    //        }
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:parameters
                                                           options:NSJSONReadingAllowFragments
                                                             error:nil];
//    NSData *requestData = [self encodeDictionary:parameters];
    
    [serviceRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [serviceRequest setHTTPBody: requestData];
    
    [serviceRequest setValue:REQUEST_CONTENT_TYPE forHTTPHeaderField:@"Content-Type"];
    NSURLSessionConfiguration * sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
   // sessionConfig.timeoutIntervalForRequest = 90000;
//    sessionConfig.timeoutIntervalForResource = 90000;
    sessionConfig.timeoutIntervalForRequest = 60.0;
    sessionConfig.timeoutIntervalForResource = 60.0;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:delegate delegateQueue:nil];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:serviceRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error && ((NSHTTPURLResponse*)response).statusCode == 200 ){
            if (data){
                id result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
                complession(result);
            }
        }
        else{
            complession(nil);
        }
        
    }];
    [task resume];
    //    }
    //    @catch (NSException *exception) {
    //
    //    }
    
}



+(void)performPostRequestWithURL:(NSURL*)url Parameters:(NSDictionary*)parameters WithUploadFile:(NSString*)file_path Complession:(void(^)(id response) )complession{
    @try {
        PKMultipartInputStream *body = [[PKMultipartInputStream alloc] init];
        for (NSString*key in parameters.allKeys) {
            [body  addPartWithName: key string:[parameters objectForKey:key]];
         }
        [body addPartWithName:@"file" filename:[file_path lastPathComponent] path:file_path];
        
        NSMutableURLRequest *serviceRequest = [[NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy  timeoutInterval:TIME_OUT_INTERVAL]mutableCopy];
        [serviceRequest setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", [body boundary]] forHTTPHeaderField:@"Content-Type"];

        [serviceRequest setValue:[NSString stringWithFormat:@"%ld", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
        [serviceRequest setHTTPBodyStream:body];
        [serviceRequest setHTTPMethod:@"POST"];
        
        dispatch_queue_t someQueue = dispatch_queue_create("someQueue", nil);
        dispatch_async(someQueue, ^{
             NSHTTPURLResponse*response ;
            NSData *returnData = [NSURLConnection sendSynchronousRequest:serviceRequest returningResponse:&response error:nil];
            if ( response.statusCode == 200 ){
                
                if ([returnData length]>0){
                    id result= [NSJSONSerialization JSONObjectWithData:returnData options:0 error:nil] ;
                    complession(result);
                }
                else
                {
                    complession(nil);
                }
            }
            else{
                complession(nil);
            }

           
        });

        
        
        
        
//        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
//        
//        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
//    
//        NSURLSessionDataTask *task = [session dataTaskWithRequest:serviceRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//            if (!error && ((NSHTTPURLResponse*)response).statusCode == 200 ){
//                
//                if ([data length]>0){
//                    id result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
//                    complession(result);
//                }
//                else
//                {
//                    complession(nil);
//                }
//            }
//            else{
//                complession(nil);
//            }
//            
//        }];
//        
//        [task resume];

     
    }
    @catch (NSException *exception) {
        
    }
 
}
@end
