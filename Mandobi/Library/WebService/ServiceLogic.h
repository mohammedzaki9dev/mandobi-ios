//
//  ServiceLogic.h
//  Shauffeur Taxi
//
//  Created by Assem Imam on 12/2/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebService.h"
#import "User.h"
#import "Worker.h"
#import "Order.h"
#import "Place.h"

#define WEB_SERVICE_URL @"http://mandobiapp.com/c_m_s/"//"http://mandobiapp.com/c_m_s/"

//#define WEB_SERVICE_URL @"http://132.148.27.114/~mandobiapp2/c_m_s/"//"http://mandobiapp.com/c_m_s/"
#define WORKER_SERVICE_URL @"http://mandobiapp.com/w_m_s/"

@interface ServiceLogic : NSObject
@property(assign)id<NSURLSessionDelegate>delegate;
+ (ServiceLogic*) sharedInstance ;
-(void)autoCompleteForPlace:(NSString*)place  Complession:(void (^)(id))complession;
-(void)getLocationForMandoob:(Worker*)user Complession:(void (^)(id))complession;
-(void)getFollowesForUser:(User*)user Complession:(void (^)(id))complession;
-(void)finishSubmittingForOrder:(Order*) order Complession:(void (^)(id))complession;
-(void)getMandoobResponsesForUser:(User*)user Complession:(void (^)(id))complession;
-(void)getUsePolicyWithComplession:(void(^)(id response) )complession;
-(void)getHowToRegisterWithComplession:(void(^)(id response) )complession;
-(void)sendOrder:(Order*)order ForUser:(User*)user Complession:(void(^)(id response) )complession;
-(void)reverseGeocodeWithLatitude:(double)latitude Longitude:(double)longitude APIKEY:(NSString*)api_key Complession:(void(^)(id response) )complession;
-(void)acceptOrder:(Order*)order ForWorker:(Worker*)worker WithPrice:(double)price Complession:(void(^)(id response) )complession;
-(void)getDetailsForOrder:(Order*) order Complession:(void(^)(id response) )complession;
-(void)getOrdersForUser:(User*)user WithOrderType:(OrderType)orderType Complession:(void(^)(id response) )complession;
-(void)getProfitForUser:(User*)user ForDate:(NSDate*)date Complession:(void(^)(id response) )complession;
-(void)setAvailabilityForUser:(User*)user  Complession:(void(^)(id response) )complession;
-(void)loginWithUser:(User*)user  Complession:(void(^)(id response) )complession;
-(void)registerUser:(User*)user Complession:(void(^)(id response) ) complession;
-(void)getAboutApplicationWithComplession:(void(^)(id response) ) complession;
-(void)logOutForUser:(User*)user Complession:(void(^)(id response) ) complession;
-(void)sendNotificationToken:(NSString*)token ForWorker:(User*)user Complession:(void(^)(id response) ) complession;
-(void)getInformationForUserID:(int)userID Complession:(void(^)(id response) ) complession;
-(void)updateLocationForUser:(User*)user Complession:(void(^)(id response) ) complession;
-(void)getOnlineDriversForLatitude:(double)latitude Longitude:(double)longitude  Complession:(void(^)(id response) ) complession;
-(void)sendRequestForomUser:(User*)user Complession:(void(^)(id response) ) complession;
-(void)getInformationForDriverByID:(int)driverID Complession:(void(^)(id response) ) complession;
-(void)confirmRequestFromUser:(User*)user ForOrder:(Order*)order Complession:(void(^)(id response) ) complession;
-(void)updateProfileForUser:(User*)user  Complession:(void(^)(id response) ) complession;
-(void)updateImageForUser:(User *)user Complession:(void (^)(id))complession;
-(void)getInboxForUser:(User*)user Complession:(void (^)(id))complession;
-(void)getFavouriteDriversForUser:(User*)user Complession:(void(^)(id response) ) complession;
-(void)deleteFromFavouriteDrivers:(Worker*)driver Complession:(void(^)(id response) ) complession;
-(void)addToFavouriteDrivers:(Worker*)driver ForUser:(User*)user Complession:(void(^)(id response) ) complession;
-(void)cancelUserRequest:(User *)user ForOrder:(Order *)order WithCancelReasonID:(int)cancelReason Complession:(void (^)(id))complession;
-(void)getPublicNotificationDetails:(int)message_id Complession:(void(^)(id response) ) complession;
-(void)getCancellingReasonsWithComplession:(void (^)(id))complession;
-(void)resetPasswordForUserType:(UserType )user_type WithEmail:(NSString *)email Complession:(void (^)(id))complession;
-(void)NewOrderAsyncTask:(NSDictionary *)order Complession :(void (^)(id))complession;

-(void)confirmRequestFromUserUpdates:(NSInteger)userID ForOrder:(NSNumber*)orderID forWorker:(NSInteger)WorkerID Complession:(void(^)(id response) ) complession;
-(void)getOrdersResponsesFor:(User *)user  With:(NSInteger)parentId Complession:(void (^)(id))complession;
-(void)ActionForInboxMail:(NSNumber*)orderId ForSender:(NSNumber*)senderId forReciver:(NSNumber*)recieverId withTye:(NSNumber*)type Complession:(void(^)(id response) ) complession;
-(void)getDriverRateForDriverId:(NSNumber *)DriverId Complession:(void (^)(id))complession;
-(void)rateandCommitWithType:(NSNumber *)Type WithOrder:(NSNumber *)OrderId WithSender:(NSNumber *)SenderId  WithRate:(NSNumber *)Rate Withcomment:(NSString *)note Complession:(void (^)(id))complession;

-(void)getOrderById:(NSNumber *)orderID Complession:(void (^)(id))complession;
-(void)getAvalibleDriverinRangeForUserID:(NSNumber*)userID ForLocation:(CLLocation *)currentLocation forCarType:(NSNumber *)type Complession:(void (^)(id))complession;
 @end
