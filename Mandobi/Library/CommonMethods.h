//
//  CommonMethods.h
//
//
//  Created by Assem Imam on 1/19/14.
//  Copyright (c) 2014 tawasol. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


#define valueOrEmptyString(X) [CommonMethods valueOrEmptyString:X]

#define valueOrZeroNSNumber(X) [CommonMethods valueOrZeroNSNumber:X]

@interface CommonMethods : NSObject
+(BOOL)checkForEnglishLanguage:(NSString*)word;
+(NSData*)compressImage:(UIImage*) image WithSize:(CGSize)size;
+(CGFloat) getHeightOfString:(NSString*)string withSize:(CGFloat)size andWidth:(CGFloat)width;
+(UIColor *)colorFromHexString:(NSString *)hexString;
+(BOOL) ValidateMail:(NSString *)mail;
+(void)FormatView:(UIView*)view WithShadowRadius:(float)shadow_radius CornerRadius:(float)Corner_radius ShadowOpacity:(float)opcaity BorderWidth:(int)border_width BorderColor:(UIColor*)border_color ShadowColor:(UIColor*)shadow_color andXPosition:(float)xPosition;
+(void)FormatView:(UIView*)view;
+(NSString *)GetHTML_Text:(NSString *)html;
+(int)GetViewController:(id)viewController FromNavigationController:(UINavigationController*)nav_controller;
+(UIColor *)GetColorFromHexaValue:(int)value;
+(NSString*)encodeUrlWithString:(NSString*)urlString;
+(BOOL)CheckPhoneNumber:(NSString*)number;
+ (NSString *)urlencode:(NSString*)url ;
+(NSString*) convertDateToString:(NSDate*) date withFormat:(NSString *)format;
+(void)roundImgView:(UIImageView *)imageView withCornerRadius:(float)cornerRadius;
+(bool)isValidString:(NSString *)string;
+(NSString *)trimString:(NSString *)myString;
+(NSString *)convertStringDateFormat:(NSString *)date;
+(NSString *)convertDateStringFormat:(NSString *)date;
+(NSDate*) convertStringToDate:(NSString*)dateString withFormat:(NSString *)format;
+(NSString *)valueOrEmptyString:(id)value;
+(NSNumber *)valueOrZeroNSNumber:(id)value;
+ (UIImage *)imageWithColor:(UIColor *)color;
+(void)setBacgroundImageForButton:(UIButton*)button;
+(void)geocodeLocationWithLatitude:(double)latitude AndLongitude:(double)longitude Complession:(void(^)(id response) ) complession;
;
@end
