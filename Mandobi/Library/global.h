//
//  global.h
//  EasyHome
//
//  Created by Assem Imam on 12/6/15.
//  Copyright (c) 2015 Assem Imam. All rights reserved.
//

#ifndef EasyHome_global_h
#define EasyHome_global_h

#endif
#import "User.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <GoogleMaps/GoogleMaps.h>
#import "BundleLocalization.h"
#import "ServiceLogic.h"
#import "XHAmazingLoadingView.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

//#import <UIActivityIndicator-for-SDWebImage/UIImageView+UIActivityIndicatorForSDWebImage.h>

#define IMAGES_BASE_URL @"http://mandobiapp.com/all_images/"
#define WORKER_REQUEST_REMOTE_NOTIFICATION @"didReceiveWorkerRemoteNotification"
#define CLIENT_REQUEST_REMOTE_NOTIFICATION @"didReceiveClientRemoteNotification"
#define PUBLIC_REMOTE_NOTIFICATION @"didReceivePublicRemoteNotification"
#define CHANGE_LANGUAGE_NOTIFICATION @"didChangeLanguage"
#define LOGIN_NOTIFICATION @"didFinishLogin"
#define LOGOUT_NOTIFICATION @"didFinishLogout"
#define APP_DELEGATE ((AppDelegate *)[[UIApplication sharedApplication] delegate])


//#define GOOGLE_MAPS_API_KEY @"AIzaSyDOzBuFo5ZxCjlW-4k7NaxkOE-1YdD64_o"
#define GOOGLE_MAPS_API_KEY @"AIzaSyAuTTB0oI_JISgh_bKvnX951YM_oTf42RQ"
extern UserType CurrentUserType;
extern User *CurrentLogedUserInfo;
extern id remoteNotificationInfo;
extern  UIStoryboard *applicationStroryBoard;
extern BOOL internetConnection;
