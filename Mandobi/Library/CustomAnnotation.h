//
//  CustomAnnotation.h
//  SAIB
//
//  Created by user on 6/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@interface CustomAnnotation : NSObject <MKAnnotation> {
    NSString *_Name;
    NSString *_Profession;
    int  _WorkerID;
    CLLocationCoordinate2D _Coordinate;
}
@property (nonatomic) int WorkerID;
@property (copy) NSString *Name;
@property (copy) NSString *Profession;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

- (id)initWithName:(NSString*)name Profession:(NSString*)profession WorkerID:(int)worker_id coordinate:(CLLocationCoordinate2D)coordinate;


@end
