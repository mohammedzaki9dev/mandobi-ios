//
//  CustomAnnotation.m
//  SAIB
//
//  Created by user on 6/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomAnnotation.h"

@implementation CustomAnnotation
@synthesize Name = _Name;
@synthesize Profession = _Profession;
@synthesize coordinate = _Coordinate;
@synthesize WorkerID = _WorkerID;

- (id)initWithName:(NSString*)name Profession:(NSString*)profession WorkerID:(int)worker_id coordinate:(CLLocationCoordinate2D)coordinate {
    if ((self = [super init])) {
        _Name = [name copy];
        _Profession = [profession copy];
        _Coordinate = coordinate;
        _WorkerID = worker_id;
    }
    return self;
}

- (NSString *)title {
    if ([_Name isKindOfClass:[NSNull class]]) 
        return @"Unknown";
    else
        return _Name;
}

- (NSString *)subtitle {
    return _Profession;
}


@end
