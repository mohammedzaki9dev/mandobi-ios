//
//  ProfitPickerView.h
//  Mandobi
//
//  Created by Assem Imam on 2/27/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfitPickerView : UIView
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerView;

@end
