//
//  SendOrderView.m
//  Mandobi
//
//  Created by Assem Imam on 3/8/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import "SendOrderView.h"

@implementation SendOrderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib{
    @try {
        for (UIView*view in containerScrollView.subviews) {
            if (view.tag == -1) {
                view.layer.borderWidth = 1;
                view.layer.borderColor = [[UIColor colorWithRed:237/255.0f green:237/255.0f blue:237/255.0f alpha:1]CGColor];
                
            }
        }
    }
    @catch (NSException *exception) {
        
    }
}
@end
