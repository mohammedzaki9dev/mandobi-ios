//
//  SendOrderView.h
//  Mandobi
//
//  Created by Assem Imam on 3/8/16.
//  Copyright © 2016 Assem Imam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendOrderView : UIView
{
    
    __weak IBOutlet UIScrollView *containerScrollView;
}
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIButton *smallVehicleButton;
@property (weak, nonatomic) IBOutlet UIButton *mediumVehicleButton;
@property (weak, nonatomic) IBOutlet UIButton *largeVehicleButton;
@property (weak, nonatomic) IBOutlet UIButton *smallRadioButton;
@property (weak, nonatomic) IBOutlet UIButton *mediumRadioButton;
@property (weak, nonatomic) IBOutlet UIButton *largeRadioButton;
@property (weak, nonatomic) IBOutlet UITextField *shipmentDetailsTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@end
