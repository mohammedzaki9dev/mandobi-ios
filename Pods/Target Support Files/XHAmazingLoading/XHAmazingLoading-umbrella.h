#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "XHAmazingLoadingMusicsAnimation.h"
#import "XHAmazingLoadingSkypeAnimation.h"
#import "XHAmazingLoadingStarAnimation.h"
#import "XHLayerHelper.h"
#import "XHAmazingLoadingAnimationProtocol.h"
#import "XHAmazingLoadingView.h"

FOUNDATION_EXPORT double XHAmazingLoadingVersionNumber;
FOUNDATION_EXPORT const unsigned char XHAmazingLoadingVersionString[];

