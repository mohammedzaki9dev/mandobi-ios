#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "LMAddress.h"
#import "LMGeocoder.h"

FOUNDATION_EXPORT double LMGeocoderVersionNumber;
FOUNDATION_EXPORT const unsigned char LMGeocoderVersionString[];

